/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __inputNome = __modal.querySelector('input#nome')
  const __inputDescricao = __modal.querySelector('input#descricao')
  const __pagamento = __modal.querySelector('app-pagamento')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonAdicionar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonAdicionar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputNome.value = ''
    __inputDescricao.value = ''
    __pagamento.limpar()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputNome.value && !__pagamento.vazio()) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarValorRestante = () => {
    if (!__pagamento.verificarSeExisteValorRestante()) return true

    __componenteAlerta.alertar('Preencha as parcelas de forma que não sobre nem falte dinheiro.')
    return false
  }

  // Data

  methods.exportarDados = () => {
    const despesa = {}

    // obrigatorio
    despesa.nome = __inputNome.value
    despesa.valorTotal = __pagamento.exportarValorTotal()
    despesa.parcelas = __pagamento.exportarParcelas()

    // opcional
    if (__inputDescricao.value) despesa.descricao = __inputDescricao.value

    return despesa
  }

  return methods
}

module.exports = Module
