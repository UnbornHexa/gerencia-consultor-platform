/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __inputId = __modal.querySelector('input#id')
  const __inputNome = __modal.querySelector('input#nome')
  const __inputDescricao = __modal.querySelector('input#descricao')
  const __pagamento = __modal.querySelector('app-pagamento')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.mostrarParcialmente = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.ocultarParcialmente = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonEditar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEditar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __inputNome.value = ''
    __inputDescricao.value = ''
    __pagamento.limpar()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputNome.value && !__pagamento.vazio()) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarValorRestante = () => {
    if (!__pagamento.verificarSeExisteValorRestante()) return true

    __componenteAlerta.alertar('Preencha as parcelas de forma que não sobre nem falte dinheiro.')
    return false
  }

  // Data

  methods.importarDados = (dados, dataParcelaAtual) => {
    __inputId.value = dados._id
    __inputNome.value = dados.nome || ''
    __pagamento.importarDataParcelaAtual(dataParcelaAtual)
    __pagamento.importarParcelas(dados.parcelas)
    __pagamento.importarValorTotal(dados.valorTotal)

    // opcional
    __inputDescricao.value = dados.descricao || ''
  }

  methods.exportarDados = () => {
    const despesa = {}

    // obrigatorio
    despesa.nome = __inputNome.value
    despesa.valorTotal = __pagamento.exportarValorTotal()
    despesa.parcelas = __pagamento.exportarParcelas()

    // opcional
    if (__inputDescricao.value) despesa.descricao = __inputDescricao.value

    return despesa
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  return methods
}

module.exports = Module
