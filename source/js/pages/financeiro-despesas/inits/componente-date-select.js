/* Requires */

const COMPONENT_DATE_SELECT = require('../components/date-select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-date-select', COMPONENT_DATE_SELECT)
  }

  return methods
}

module.exports = Module
