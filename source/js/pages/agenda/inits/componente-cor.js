/* Requires */

const COMPONENT_COR = require('../../../global/components/cor')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-cor', COMPONENT_COR)
  }

  return methods
}

module.exports = Module
