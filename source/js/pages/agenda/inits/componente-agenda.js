/* Requires */

const REQUEST_HORARIOS = require('../../../global/requests/horarios')
const COMPONENT_AGENDA = require('../components/agenda')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-agenda', COMPONENT_AGENDA)
  }

  methods.atualizar = () => {
    const __componenteAgenda = document.querySelector('app-agenda')
    const __dateSelect = document.querySelector('app-date-select')

    const ano = __dateSelect.exportarDataSelecionada().ano
    const mes = __dateSelect.exportarDataSelecionada().mes

    __componenteAgenda.definirData(ano, mes)

    REQUEST_HORARIOS().receberTodos(ano, mes)
      .then(horarios => {
        __componenteAgenda.importarHorarios(horarios)
      })
  }

  return methods
}

module.exports = Module
