/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_AGENDA = require('./componente-agenda')
const COMPONENTE_COR = require('./componente-cor')
const COMPONENTE_DATE_SELECT = require('./componente-date-select')
const COMPONENTE_DATE_PICKER = require('./componente-date-picker')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_AGENDA().iniciar()
    COMPONENTE_COR().iniciar()
    COMPONENTE_DATE_SELECT().iniciar()
    COMPONENTE_DATE_PICKER().iniciar()
  }

  return methods
}

module.exports = Module
