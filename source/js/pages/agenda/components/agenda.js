/* Requires */

const DATE = require('../../../global/helpers/date')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const REQUEST_HORARIOS = require('../../../global/requests/horarios')

/* Module */

class Agenda extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      horarios: [],
      data: {
        ano: 0,
        mes: 0,
        quantidadeDiasNoMes: 0
      },
      diasDaSemana: [
        'Domingo',
        'Segunda',
        'Terça',
        'Quarta',
        'Quinta',
        'Sexta',
        'Sábado'
      ]
    }

    const dataHoje = this._receberDataHoje()
    this.definirData(dataHoje.ano, dataHoje.mes)
    this._renderizar()
    this.habilitarDragAndDrop()
    this.habilitarCliqueHorario()
  }

  // Inicializacao

  definirData (ano, mes) {
    if (!ano && !mes) return

    this.__state.data.ano = Number(ano)
    this.__state.data.mes = Number(mes)
    this.__state.data.quantidadeDiasNoMes = new Date(ano, mes, 0).getDate()
  }

  // Renderizacao

  _renderizar () {
    const quantidadeDiasDoMes = this.__state.data.quantidadeDiasNoMes
    let html = ''
    let diaHoje = 1

    for (const indice of [...Array(quantidadeDiasDoMes).keys()]) {
      const diaNumero = indice + 1
      const diaNome = this._receberNomeDiaDaSemana(diaNumero)

      const classeAtual = (this._verificarDataHoje(diaNumero)) ? 'atual' : ''

      html += `
      <div class="dia ${classeAtual}" data-dia="${diaNumero}">
        <div class="texto">
          <p class="numero">${diaNumero}</p>
          <p class="texto">${diaNome}</p>
        </div>
        <div class="horarios"></div>
      </div>
      `

      if (this._verificarDataHoje(diaNumero)) diaHoje = diaNumero
    }

    this.innerHTML = html
    this.rolarPara(diaHoje)
  }

  limpar () {
    this.innerHTML = ''
  }

  rolarPara (dia) {
    const posicaoX = this.offsetLeft
    const coordenadaX = this.querySelector(`.dia[data-dia="${dia}"]`).offsetLeft - posicaoX
    this.scrollTo({ top: 0, left: coordenadaX, behavior: 'smooth' })
  }

  // Horarios

  importarHorarios (horarios) {
    if (!horarios) return

    this.limpar()
    this._renderizar()

    for (const horario of horarios) {
      const dataBR = DATE().convertMongoDateToBR(horario.data)
      const dia = Number(dataBR.split('/')[0])
      this._adicionarHorario(dia, horario)
    }
  }

  _adicionarHorario (dia, horario) {
    const divDia = this.querySelector(`.dia[data-dia="${Number(dia)}"] .horarios`)

    divDia.innerHTML += `
    <div class="horario ${horario.cor}">
      <input type="hidden" value="${horario._id}">
      <p class="titulo">${horario.titulo}</p>
      <p class="hora">${horario.horaInicial} até ${horario.horaFinal}</p>
    </div>
    `
  }

  // Calculos de Data

  _receberDataHoje () {
    const hoje = new Date()
    const dataObj = DATE().dateToObj(hoje)

    return {
      ano: dataObj.year,
      mes: dataObj.month,
      dia: dataObj.day
    }
  }

  _verificarDataHoje (dia) {
    const ano = Number(this.__state.data.ano)
    const mes = Number(this.__state.data.mes)
    const dataObjHoje = this._receberDataHoje()

    if (Number(dataObjHoje.ano) !== ano) return false
    if (Number(dataObjHoje.mes) !== mes) return false
    if (Number(dataObjHoje.dia) !== Number(dia)) return false
    return true
  }

  _receberNomeDiaDaSemana (dia) {
    const ano = this.__state.data.ano
    const mes = this.__state.data.mes
    const indiceDiaDaSemana = new Date(ano, mes - 1, dia).getDay()

    return this.__state.diasDaSemana[indiceDiaDaSemana]
  }

  // Eventos

  habilitarDragAndDrop () {
    let ativado = false
    let coordenadaXInicial
    let rolagemParaEsquerda

    this.addEventListener('mousedown', (evento) => {
      ativado = true

      coordenadaXInicial = evento.pageX - this.offsetLeft
      rolagemParaEsquerda = this.scrollLeft
    })

    this.addEventListener('mouseleave', () => {
      ativado = false
      this.classList.remove('drag')
    })

    this.addEventListener('mouseup', () => {
      ativado = false
      this.classList.remove('drag')
    })

    this.addEventListener('mousemove', (evento) => {
      if (!ativado) return
      evento.preventDefault()

      const coordenadaX = evento.pageX - this.offsetLeft
      const distancia = (coordenadaX - coordenadaXInicial) * 2
      this.scrollLeft = rolagemParaEsquerda - distancia
      this.classList.add('drag')
    })
  }

  habilitarCliqueHorario () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('.horario')) return

      MODAL_UNIVERSAL().mostrar()

      const idItem = evento.target.querySelector('input[type="hidden"]').value
      REQUEST_HORARIOS().receberPorId(idItem)
        .then((retorno) => {
          MODAL_UNIVERSAL().importarDados(retorno)
        })
    })
  }
}

module.exports = Agenda
