/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __inputId = __modal.querySelector('input#id')
  const __inputTitulo = __modal.querySelector('input#titulo')
  const __inputHoraInicial = __modal.querySelector('input#hora_inicial')
  const __inputHoraFinal = __modal.querySelector('input#hora_final')
  const __textareaDescricao = __modal.querySelector('textarea#descricao')
  const __datepicker = __modal.querySelector('app-date-picker')
  const __cor = __modal.querySelector('app-cor')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.mostrarParcialmente = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.ocultarParcialmente = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonEditar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEditar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __inputTitulo.value = ''
    __inputHoraInicial.value = ''
    __inputHoraFinal.value = ''
    __textareaDescricao.value = ''
    __cor.limpar()
    __datepicker.limpar()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputTitulo.value && !__datepicker.vazio() && __inputHoraInicial.value && __inputHoraFinal.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  // Data

  methods.importarDados = (dados) => {
    const data = DATE().convertMongoDateToBR(dados.data) || ''

    __inputId.value = dados._id
    __inputTitulo.value = dados.titulo || ''
    __datepicker.selecionarData(data)
    __inputHoraInicial.value = dados.horaInicial || ''
    __inputHoraFinal.value = dados.horaFinal || ''

    // opcional
    __textareaDescricao.value = dados.descricao || ''
    __cor.selecionarCor(dados.cor)
  }

  methods.exportarDados = () => {
    const horario = {}

    // obrigatorio
    horario.titulo = __inputTitulo.value
    horario.data = DATE().convertBRToTimestamp(__datepicker.exportarData())
    horario.horaInicial = __inputHoraInicial.value
    horario.horaFinal = __inputHoraFinal.value

    // opcional
    if (__textareaDescricao.value) horario.descricao = __textareaDescricao.value
    if (__cor.exportarCor()) horario.cor = __cor.exportarCor()

    return horario
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  return methods
}

module.exports = Module
