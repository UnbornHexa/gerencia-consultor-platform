/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __inputTitulo = __modal.querySelector('input#titulo')
  const __inputHoraInicial = __modal.querySelector('input#hora_inicial')
  const __inputHoraFinal = __modal.querySelector('input#hora_final')
  const __textareaDescricao = __modal.querySelector('textarea#descricao')
  const __datepicker = __modal.querySelector('app-date-picker')
  const __cor = __modal.querySelector('app-cor')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonAdicionar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonAdicionar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputTitulo.value = ''
    __inputHoraInicial.value = ''
    __inputHoraFinal.value = ''
    __textareaDescricao.value = ''
    __cor.limpar()
    __datepicker.resetar()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputTitulo.value && !__datepicker.vazio() && __inputHoraInicial.value && __inputHoraFinal.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  // Data

  methods.exportarDados = () => {
    const horario = {}

    // obrigatorio
    horario.titulo = __inputTitulo.value
    horario.data = DATE().convertBRToTimestamp(__datepicker.exportarData())
    horario.horaInicial = __inputHoraInicial.value
    horario.horaFinal = __inputHoraFinal.value

    // opcional
    if (__textareaDescricao.value) horario.descricao = __textareaDescricao.value
    if (__cor.exportarCor()) horario.cor = __cor.exportarCor()

    return horario
  }

  return methods
}

module.exports = Module
