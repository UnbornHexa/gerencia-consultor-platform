/* Requires */

const INIT_AGENDA = require('../inits/componente-agenda')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarAgendaAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_AGENDA().atualizar()
    })
  }

  return methods
}

module.exports = Module
