/* Requires */

const REQUEST_HORARIOS = require('../../../global/requests/horarios')
const INIT_AGENDA = require('../inits/componente-agenda')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_DELETAR = require('../modals/modal-deletar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonOpcoesDeletar = __modal.querySelector('.opcoes button#deletar')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()

      const camposObrigatoriosOk = MODAL_UNIVERSAL().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      MODAL_UNIVERSAL().bloquearBotao()
      REQUEST_HORARIOS().editar(dados, id)
        .then(() => {
          MODAL_UNIVERSAL().ocultar()
          INIT_AGENDA().atualizar()
        })
        .finally(() => {
          MODAL_UNIVERSAL().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_UNIVERSAL().ocultar()
    })
  }

  // Opcoes

  methods.habilitarCliqueBotaoOpcoesDeletar = () => {
    __buttonOpcoesDeletar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { titulo: dados.titulo, id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_DELETAR().mostrar()
      MODAL_DELETAR().importarDados(obj)
    })
  }

  return methods
}

module.exports = Module
