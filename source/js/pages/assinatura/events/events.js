/* Requires */

const EVENTS = require('../../../global/events/events')
const WINDOW = require('./window')
const MAIN = require('./main')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    WINDOW().sincronizarTelaAssinatura()

    MAIN().habilitarCliqueAssinar()
    MAIN().habilitarCliqueSair()
    MAIN().habilitarCliqueSuporte()
  }

  return methods
}

module.exports = Module
