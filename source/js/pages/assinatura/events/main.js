/* Requires */

const CHAT = require('../../../global/helpers/chat')
const LOGOUT = require('../../../global/helpers/logout')
const HELPER_PAGSEGURO = require('../helpers/formulario-pagseguro')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __chatInicial = document.querySelector('section.base.inicial .suporte')
  const __chatAssinante = document.querySelector('section.base.assinante .suporte')
  const __chatExpirado = document.querySelector('section.base.expirado .suporte')
  const __buttonSairInicial = document.querySelector('section.base.inicial button.sair')
  const __buttonSairExpirado = document.querySelector('section.base.expirado button.sair')
  const __buttonAssinarInicial = document.querySelector('section.base.inicial button.assinar')
  const __buttonAssinarExpirado = document.querySelector('section.base.expirado button.assinar')

  // Methods

  methods.habilitarCliqueSuporte = () => {
    CHAT().construirChat()

    __chatInicial.addEventListener('click', () => {
      CHAT().abrirChat()
    })
    __chatAssinante.addEventListener('click', () => {
      CHAT().abrirChat()
    })
    __chatExpirado.addEventListener('click', () => {
      CHAT().abrirChat()
    })
  }

  methods.habilitarCliqueSair = () => {
    __buttonSairInicial.addEventListener('click', () => {
      LOGOUT().logout()
    })
    __buttonSairExpirado.addEventListener('click', () => {
      LOGOUT().logout()
    })
  }

  methods.habilitarCliqueAssinar = () => {
    __buttonAssinarInicial.addEventListener('click', () => { pagar() })
    __buttonAssinarExpirado.addEventListener('click', () => { pagar() })

    function pagar () {
      HELPER_PAGSEGURO().adicionarIdUsuario()
      HELPER_PAGSEGURO().redirecionarPagamento()
    }
  }

  return methods
}

module.exports = Module
