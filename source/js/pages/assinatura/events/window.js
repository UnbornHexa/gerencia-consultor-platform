/* Requires */

const HELPER_ASSINATURA = require('../helpers/atualizar-assinatura')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.sincronizarTelaAssinatura = () => {
    window.addEventListener('load', () => {
      HELPER_ASSINATURA().atualizar()
    })
  }

  return methods
}

module.exports = Module
