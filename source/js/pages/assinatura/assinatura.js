/* Requires */

const HELPER_LOGOUT = require('../../global/helpers/logout')
const EVENTOS = require('./events/events')
const INICIADORES = require('./inits/init')

/* Start */

HELPER_LOGOUT().checkTokenOk()
INICIADORES().iniciarTodosOsComponentes()
EVENTOS().habilitarTodosOsEventos()
