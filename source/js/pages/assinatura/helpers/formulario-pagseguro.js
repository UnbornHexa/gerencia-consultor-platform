/* Requires */

const TOKEN = require('../../../global/helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __formularioPagseguro = document.querySelector('form#pagseguro')
  const __inputReference = __formularioPagseguro.querySelector('input[name="reference"]')

  // Methods

  methods.adicionarIdUsuario = () => {
    const idUsuario = TOKEN().readUsuarioId()
    __inputReference.value = idUsuario
  }

  methods.redirecionarPagamento = () => {
    __formularioPagseguro.submit()
  }

  return methods
}

module.exports = Module
