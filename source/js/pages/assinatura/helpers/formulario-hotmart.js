/* Requires */

const TOKEN = require('../../../global/helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  methods.redirecionarPagamento = () => {
    const idUsuario = TOKEN().readUsuarioId()
    const url = `https://pay.hotmart.com/A37161737P?checkoutMode=10&xcod=${idUsuario}`
    window.location.assign(url)
  }

  return methods
}

module.exports = Module
