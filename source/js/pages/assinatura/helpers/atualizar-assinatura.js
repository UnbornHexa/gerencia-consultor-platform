/* Requires */

const REQUEST_ASSINATURAS = require('../../../global/requests/assinaturas')
const SECTION_TELAS = require('../sections/section-telas')

/* Module */

const Module = () => {
  const methods = {}

  methods.atualizar = () => {
    REQUEST_ASSINATURAS().receberPorUsuario()
      .then((assinatura) => {
        // assinante
        if (assinatura.expirado === false) {
          SECTION_TELAS().mostrarTelaAssinante()
          return
        }

        // expirado
        if (assinatura.expirado === true && assinatura.tipo !== 'gratis') {
          SECTION_TELAS().mostrarTelaExpirado()
          return
        }

        SECTION_TELAS().mostrarTelaInicial()
      })
  }

  return methods
}

module.exports = Module
