const Module = () => {
  const methods = {}

  // Internal Variables

  const __telaInicial = document.querySelector('section.base.inicial')
  const __telaAssinante = document.querySelector('section.base.assinante')
  const __telaExpirado = document.querySelector('section.base.expirado')

  // Modal

  methods.ocultarTelas = () => {
    __telaInicial.classList.remove('mostrar')
    __telaAssinante.classList.remove('mostrar')
    __telaExpirado.classList.remove('mostrar')
  }

  methods.mostrarTelaInicial = () => {
    methods.ocultarTelas()
    __telaInicial.classList.add('mostrar')
  }

  methods.mostrarTelaAssinante = () => {
    methods.ocultarTelas()
    __telaAssinante.classList.add('mostrar')
  }

  methods.mostrarTelaExpirado = () => {
    methods.ocultarTelas()
    __telaExpirado.classList.add('mostrar')
  }

  return methods
}

module.exports = Module
