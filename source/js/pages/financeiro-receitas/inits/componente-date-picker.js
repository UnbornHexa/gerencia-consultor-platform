/* Requires */

const COMPONENT_DATE_PICKER = require('../../../global/components/date-picker')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-date-picker', COMPONENT_DATE_PICKER)
  }

  return methods
}

module.exports = Module
