/* Requires */

const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_DELETAR = require('../modals/modal-deletar')
const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_deletar')
  const __buttonDeletar = __modal.querySelector('button#deletar')
  const __buttonVoltar = __modal.querySelector('button#voltar')

  // Methods

  methods.habilitarCliqueBotaoDeletar = () => {
    __buttonDeletar.addEventListener('click', () => {
      const id = MODAL_DELETAR().exportarId()

      MODAL_DELETAR().bloquearBotao()
      REQUEST_PRODUTOS().deletar(id)
        .then(() => {
          MODAL_DELETAR().ocultar()
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_DELETAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_DELETAR().ocultar()
      MODAL_UNIVERSAL().mostrarParcialmente()
    })
  }

  return methods
}

module.exports = Module
