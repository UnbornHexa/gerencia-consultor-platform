/* Requires */

const REQUEST_RECEITAS = require('../../../global/requests/receitas')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_DELETAR = require('../modals/modal-deletar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonOpcoesDeletar = __modal.querySelector('.opcoes button#deletar')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()

      const camposObrigatoriosOk = MODAL_UNIVERSAL().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const valorRestanteOk = MODAL_UNIVERSAL().verificarValorRestante()
      if (!valorRestanteOk) return

      MODAL_UNIVERSAL().bloquearBotao()
      REQUEST_RECEITAS().editar(dados, id)
        .then(() => {
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_UNIVERSAL().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_UNIVERSAL().ocultar()
    })
  }

  // Opcoes

  methods.habilitarCliqueBotaoOpcoesDeletar = () => {
    __buttonOpcoesDeletar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { nome: dados.nome, id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_DELETAR().mostrar()
      MODAL_DELETAR().importarDados(obj)
    })
  }

  return methods
}

module.exports = Module
