/* Requires */

const REQUEST_RECEITAS = require('../../../global/requests/receitas')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __buttonFechar = __modal.querySelector('button#fechar')

  // Methods

  methods.habilitarCliqueBotaoAdicionar = () => {
    __buttonAdicionar.addEventListener('click', () => {
      const dados = MODAL_ADICIONAR().exportarDados()

      const camposObrigatoriosOk = MODAL_ADICIONAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const valorRestanteOk = MODAL_ADICIONAR().verificarValorRestante()
      if (!valorRestanteOk) return

      MODAL_ADICIONAR().bloquearBotao()
      REQUEST_RECEITAS().adicionar(dados)
        .then(() => {
          MODAL_ADICIONAR().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_ADICIONAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_ADICIONAR().ocultar()
    })
  }

  return methods
}

module.exports = Module
