/* Requires */

const INIT_LISTA = require('../inits/componente-lista')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarListaAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_LISTA().atualizar()
    })
  }

  return methods
}

module.exports = Module
