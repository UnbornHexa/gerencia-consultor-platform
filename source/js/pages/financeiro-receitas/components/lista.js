/* Requires */

const FORMAT = require('../../../global/helpers/format')
const DATE = require('../../../global/helpers/date')
const COMPONENT_LISTA = require('../../../global/components/lista')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const REQUEST_RECEITAS = require('../../../global/requests/receitas')

/* Module */

class Lista extends COMPONENT_LISTA {
  constructor () {
    super()
    this.habilitarCliqueItem()
  }

  _renderizarNovoItem (item) {
    const data = DATE().convertMongoDateToBR(item.parcelas.dataPagamento)
    const valor = FORMAT().money(item.parcelas.valor)
    const itensUl = this.querySelector('ul.itens')

    const li =
    `
    <li class="item">
      <div class="info">
        <input type="hidden" value="${item._id}">
        <div class="icone-tela receitas"></div>
        <div>
          <p class="principal">${item.nome}</p>
          <p class="secundario">${valor} - ${data}</p>
        </div>
      </div>
      <div class="opcoes">
        <div class="icone"></div>
      </div>
    </li>
    `

    itensUl.innerHTML += li
  }

  habilitarCliqueItem () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('ul li')) return

      MODAL_UNIVERSAL().mostrar()

      const idItem = evento.target.querySelector('.info input').value
      const dataItem = evento.target.querySelector('.info .secundario').innerText.split(' - ')[1]

      REQUEST_RECEITAS().receberPorId(idItem)
        .then((retorno) => {
          MODAL_UNIVERSAL().importarDados(retorno, dataItem)
        })
    })
  }
}

module.exports = Lista
