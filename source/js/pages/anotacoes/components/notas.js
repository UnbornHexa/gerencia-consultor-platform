/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const DATE = require('../../../global/helpers/date')
const REQUEST_ANOTACOES = require('../../../global/requests/anotacoes')
const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

class Notas extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      notas: [],
      notasFiltradas: []
    }

    this.habilitarClique()
  }

  // Renderizar

  _renderizarNovaNota (nota) {
    const classeLembrete = (nota.dataLembrete) ? 'notificar' : ''

    this.innerHTML += `
    <div class="nota ${nota.cor} ${classeLembrete}">
      <input type="hidden" value="${nota._id}">
      <div class="cor"></div>
      <p class="titulo">${nota.titulo}</p>
      <p class="data">Criado em ${DATE().convertMongoDateToBR(nota.dataRegistro)}</p>
    </div>
    `
  }

  // Utils

  filtrar (texto) {
    this.__state.notasFiltradas = this.__state.notas.filter(nota => {
      const termoPesquisado = ACCENT().remove(texto.toString().toUpperCase())
      const notaAVerificar = ACCENT().remove(nota.titulo.toUpperCase())
      return (notaAVerificar.includes(termoPesquisado))
    })

    this.limpar()
    for (const nota of this.__state.notasFiltradas) {
      this._renderizarNovaNota(nota)
    }
  }

  limpar () {
    this.innerHTML = ''
  }

  importarNotas (notas) {
    this.limpar()
    this.__state.notas = notas
    this.__state.notasFiltradas = notas

    for (const nota of this.__state.notasFiltradas) {
      this._renderizarNovaNota(nota)
    }
  }

  // Eventos

  habilitarClique () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('.nota')) return

      MODAL_UNIVERSAL().mostrar()

      const idNota = evento.target.querySelector('input').value
      REQUEST_ANOTACOES().receberPorId(idNota)
        .then((retorno) => {
          MODAL_UNIVERSAL().importarDados(retorno)
        })
    })
  }
}

module.exports = Notas
