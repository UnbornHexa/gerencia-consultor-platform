/* Requires */

const COMPONENT_DATE_SELECT = require('../../../global/components/date-select')
const INIT_NOTAS = require('../inits/componente-notas')

/* Module */

class DateSelect extends COMPONENT_DATE_SELECT {
  habilitarCliqueBotaoAvancar () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('button.avancar')
      if (evento.target !== elemento) return

      this._avancarMes()
      INIT_NOTAS().atualizar()
    })
  }

  habilitarCliqueBotaoVoltar () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('button.voltar')
      if (evento.target !== elemento) return

      this._retrocederMes()
      INIT_NOTAS().atualizar()
    })
  }
}

module.exports = DateSelect
