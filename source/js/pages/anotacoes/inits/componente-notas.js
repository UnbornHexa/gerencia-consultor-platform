/* Requires */

const REQUEST_ANOTACOES = require('../../../global/requests/anotacoes')
const COMPONENT_NOTAS = require('../components/notas')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-notas', COMPONENT_NOTAS)
  }

  methods.atualizar = () => {
    const __componenteNotas = document.querySelector('app-notas')
    const __dateSelect = document.querySelector('app-date-select')

    const ano = __dateSelect.exportarDataSelecionada().ano
    const mes = __dateSelect.exportarDataSelecionada().mes

    REQUEST_ANOTACOES().receberTodos(ano, mes)
      .then(anotacoes => {
        __componenteNotas.importarNotas(anotacoes)
      })
  }

  return methods
}

module.exports = Module
