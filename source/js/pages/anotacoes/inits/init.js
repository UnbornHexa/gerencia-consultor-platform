/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_DATE_SELECT = require('./componente-date-select')
const COMPONENTE_DATE_PICKER = require('./componente-date-picker')
const COMPONENTE_COR = require('./componente-cor')
const COMPONENTE_NOTAS = require('./componente-notas')
const COMPONENTE_CHECKBOX = require('./componente-checkbox')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_DATE_SELECT().iniciar()
    COMPONENTE_COR().iniciar()
    COMPONENTE_NOTAS().iniciar()
    COMPONENTE_DATE_PICKER().iniciar()
    COMPONENTE_CHECKBOX().iniciar()
  }

  return methods
}

module.exports = Module
