const Module = () => {
  const methods = {}

  // Internal Variables

  const __componenteNotas = document.querySelector('app-notas')
  const __inputPesquisa = document.querySelector('section.menu .pesquisa input')
  let __cancelarPesquisa = null

  // Methods

  methods.habilitarBarraPesquisa = () => {
    __inputPesquisa.addEventListener('keyup', (evento) => {
      const texto = evento.target.value
      clearTimeout(__cancelarPesquisa)

      __cancelarPesquisa = setTimeout(() => {
        __componenteNotas.filtrar(texto)
      }, 300)
    })
  }

  return methods
}

module.exports = Module
