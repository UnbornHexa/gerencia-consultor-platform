/* Requires */

const EVENTS = require('../../../global/events/events')
const CHAT = require('../../../global/events/chat')
const TOGGLE = require('../../../structures/navbar/helper-toggle')
const WINDOW = require('./window')
const MAIN = require('./main')
const MODAL_ADICIONAR = require('./modal-adicionar')
const MODAL_UNIVERSAL = require('./modal-universal')
const MODAL_DELETAR = require('./modal-deletar')
const PESQUISA = require('./pesquisa')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    TOGGLE().habilitarCliqueToggle()
    MAIN().habilitarCliqueBotaoAdicionar()
    PESQUISA().habilitarBarraPesquisa()
    WINDOW().povoarNotasAoIniciar()

    MODAL_ADICIONAR().habilitarCliqueBotaoAdicionar()
    MODAL_ADICIONAR().habilitarCliqueBotaoFechar()
    MODAL_ADICIONAR().habilitarCliqueCheckbox()

    MODAL_UNIVERSAL().habilitarCliqueBotaoEditar()
    MODAL_UNIVERSAL().habilitarCliqueBotaoFechar()
    MODAL_UNIVERSAL().habilitarCliqueBotaoOpcoesDeletar()
    MODAL_UNIVERSAL().habilitarCliqueCheckbox()

    MODAL_DELETAR().habilitarCliqueBotaoDeletar()
    MODAL_DELETAR().habilitarCliqueBotaoVoltar()

    CHAT().habilitarChat()
  }

  return methods
}

module.exports = Module
