/* Requires */

const REQUEST_ANOTACOES = require('../../../global/requests/anotacoes')
const INIT_NOTAS = require('../inits/componente-notas')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __checkbox = __modal.querySelector('app-checkbox')

  // Methods

  methods.habilitarCliqueBotaoAdicionar = () => {
    __buttonAdicionar.addEventListener('click', () => {
      const dados = MODAL_ADICIONAR().exportarDados()

      const camposObrigatoriosOk = MODAL_ADICIONAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const dataLembreteOk = MODAL_ADICIONAR().verificarDataLembrete()
      if (!dataLembreteOk) return

      MODAL_ADICIONAR().bloquearBotao()
      REQUEST_ANOTACOES().adicionar(dados)
        .then(() => {
          MODAL_ADICIONAR().ocultar()
          INIT_NOTAS().atualizar()
        })
        .finally(() => {
          MODAL_ADICIONAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_ADICIONAR().ocultar()
    })
  }

  methods.habilitarCliqueCheckbox = () => {
    __checkbox.addEventListener('click', () => {
      (__checkbox.verificar())
        ? MODAL_ADICIONAR().mostrarLembrete()
        : MODAL_ADICIONAR().ocultarLembrete()
    })
  }

  return methods
}

module.exports = Module
