/* Requires */

const INIT_NOTAS = require('../inits/componente-notas')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarNotasAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_NOTAS().atualizar()
    })
  }

  return methods
}

module.exports = Module
