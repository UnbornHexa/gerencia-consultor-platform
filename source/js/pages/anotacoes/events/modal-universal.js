/* Requires */

const REQUEST_ANOTACOES = require('../../../global/requests/anotacoes')
const INIT_NOTAS = require('../inits/componente-notas')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_DELETAR = require('../modals/modal-deletar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonOpcoesDeletar = __modal.querySelector('.opcoes button#deletar')
  const __checkbox = __modal.querySelector('app-checkbox')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()

      const camposObrigatoriosOk = MODAL_UNIVERSAL().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const dataLembreteOk = MODAL_UNIVERSAL().verificarDataLembrete()
      if (!dataLembreteOk) return

      MODAL_UNIVERSAL().bloquearBotao()
      REQUEST_ANOTACOES().editar(dados, id)
        .then(() => {
          MODAL_UNIVERSAL().ocultar()
          INIT_NOTAS().atualizar()
        })
        .finally(() => {
          MODAL_UNIVERSAL().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_UNIVERSAL().ocultar()
    })
  }

  methods.habilitarCliqueCheckbox = () => {
    __checkbox.addEventListener('click', () => {
      (__checkbox.verificar())
        ? MODAL_UNIVERSAL().mostrarLembrete()
        : MODAL_UNIVERSAL().ocultarLembrete()
    })
  }

  // Opcoes

  methods.habilitarCliqueBotaoOpcoesDeletar = () => {
    __buttonOpcoesDeletar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { titulo: dados.titulo, id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_DELETAR().mostrar()
      MODAL_DELETAR().importarDados(obj)
    })
  }

  return methods
}

module.exports = Module
