/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __inputTitulo = __modal.querySelector('input#titulo')
  const __datepicker = __modal.querySelector('app-date-picker')
  const __textareaConteudo = __modal.querySelector('textarea#conteudo')
  const __cor = __modal.querySelector('app-cor')
  const __checkbox = __modal.querySelector('app-checkbox')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.mostrarLembrete = () => {
    __datepicker.style.display = 'flex'
  }

  methods.ocultarLembrete = () => {
    __datepicker.style.display = 'none'
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonAdicionar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonAdicionar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputTitulo.value = ''
    __textareaConteudo.value = ''
    __cor.limpar()
    __checkbox.limpar()
    __datepicker.resetar()

    methods.ocultarLembrete()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputTitulo.value && __textareaConteudo.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarDataLembrete = () => {
    if (!__checkbox.verificar()) return true
    if (__checkbox.verificar() && !__datepicker.vazio()) return true

    __componenteAlerta.alertar('Preencha a data do lembrete')
    return false
  }

  // Data

  methods.exportarDados = () => {
    const anotacao = {}

    // obrigatorio
    anotacao.titulo = __inputTitulo.value
    anotacao.conteudo = __textareaConteudo.value

    // opional
    if (__cor.exportarCor()) anotacao.cor = __cor.exportarCor()
    if (__checkbox.verificar()) anotacao.dataLembrete = DATE().convertBRToTimestamp(__datepicker.exportarData())

    return anotacao
  }

  return methods
}

module.exports = Module
