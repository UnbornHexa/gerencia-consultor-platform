const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_deletar')
  const __inputId = __modal.querySelector('input#id')
  const __pTexto = __modal.querySelector('p.texto')
  const __buttonDeletar = __modal.querySelector('button#deletar')

  // Modal

  methods.mostrar = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonDeletar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonDeletar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __pTexto.innerText = ''
  }

  // Data

  methods.importarDados = (dados) => {
    __inputId.value = dados.id
    __pTexto.innerText = dados.titulo
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  return methods
}

module.exports = Module
