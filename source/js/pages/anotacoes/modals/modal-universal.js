/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __inputId = __modal.querySelector('input#id')
  const __inputTitulo = __modal.querySelector('input#titulo')
  const __datepicker = __modal.querySelector('app-date-picker')
  const __textareaConteudo = __modal.querySelector('textarea#conteudo')
  const __cor = __modal.querySelector('app-cor')
  const __checkbox = __modal.querySelector('app-checkbox')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.mostrarParcialmente = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.ocultarParcialmente = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
  }

  methods.mostrarLembrete = () => {
    __datepicker.style.display = 'flex'
  }

  methods.ocultarLembrete = () => {
    __datepicker.style.display = 'none'
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonEditar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEditar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __inputTitulo.value = ''
    __textareaConteudo.value = ''
    __cor.limpar()
    __checkbox.limpar()
    __datepicker.resetar()

    methods.ocultarLembrete()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputTitulo.value && __textareaConteudo.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarDataLembrete = () => {
    if (!__checkbox.verificar()) return true
    if (__checkbox.verificar() && !__datepicker.vazio()) return true

    __componenteAlerta.alertar('Preencha a data do lembrete')
    return false
  }

  // Data

  methods.importarDados = (dados) => {
    const data = DATE().convertMongoDateToBR(dados.dataLembrete) || ''

    __inputId.value = dados._id
    __inputTitulo.value = dados.titulo || ''
    __textareaConteudo.value = dados.conteudo || ''

    if (data) {
      methods.mostrarLembrete()
      __datepicker.selecionarData(data)
      __checkbox.ativar()
    }

    // opcional
    __cor.selecionarCor(dados.cor)
  }

  methods.exportarDados = () => {
    const anotacao = {}

    // obrigatorio
    anotacao.titulo = __inputTitulo.value
    anotacao.conteudo = __textareaConteudo.value

    // opcional
    if (__cor.exportarCor()) anotacao.cor = __cor.exportarCor()
    if (__checkbox.verificar()) anotacao.dataLembrete = DATE().convertBRToTimestamp(__datepicker.exportarData())

    return anotacao
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  return methods
}

module.exports = Module
