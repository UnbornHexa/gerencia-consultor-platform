/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_senha')
  const __inputSenhaAtual = __modal.querySelector('input#senha_atual')
  const __inputSenhaNova = __modal.querySelector('input#senha_nova')
  const __inputConfirmarSenha = __modal.querySelector('input#confirmar_senha')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonEditar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEditar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputSenhaAtual.value = ''
    __inputSenhaNova.value = ''
    __inputConfirmarSenha.value = ''
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputSenhaAtual.value && __inputSenhaNova.value && __inputConfirmarSenha.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarSenhasIguais = () => {
    if (__inputSenhaNova.value === __inputConfirmarSenha.value) return true

    __componenteAlerta.alertar('As senhas devem ser iguais.')
    return false
  }

  methods.verificarTamanhoSenha = () => {
    if (__inputSenhaNova.value.length >= 6) return true

    __componenteAlerta.alertar('A senha deve ter no mínimo 6 caracteres.')
    return false
  }

  // Data

  methods.exportarDados = () => {
    const usuario = {}

    usuario.senhaAtual = __inputSenhaAtual.value || ''
    usuario.senhaNova = __inputSenhaNova.value || ''

    return usuario
  }

  return methods
}

module.exports = Module
