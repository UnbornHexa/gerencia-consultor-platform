/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_CHECKBOX = require('./componente-checkbox')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_CHECKBOX().iniciar()
  }

  return methods
}

module.exports = Module
