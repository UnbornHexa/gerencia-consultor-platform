const Module = () => {
  const methods = {}

  // Internal Variables

  const __checkboxModoNoturno = document.querySelector('section.conteudo .noturno app-checkbox')

  // Methods

  methods.verificarToggleModoNoturno = () => {
    if (window.localStorage.getItem('dark-theme')) __checkboxModoNoturno.ativar()
  }

  return methods
}

module.exports = Module
