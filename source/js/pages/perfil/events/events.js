/* Requires */

const EVENTS = require('../../../global/events/events')
const CHAT = require('../../../global/events/chat')
const TOGGLE = require('../../../structures/navbar/helper-toggle')
const WINDOW = require('./window')
const MAIN = require('./main')
const MODAL_ALTERAR_SENHA = require('./modal-alterar-senha')
const SECTION_EDITAR = require('./section-editar')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    TOGGLE().habilitarCliqueToggle()
    WINDOW().povoarSecaoEditar()

    MAIN().habilitarCliqueBotaoAlterarSenha()
    MAIN().habilitarCliqueBotaoSuporte()
    MAIN().habilitarCliqueBotaoSair()
    MAIN().habilitarCliqueCheckboxModoNoturno()

    MODAL_ALTERAR_SENHA().habilitarCliqueBotaoEditar()
    MODAL_ALTERAR_SENHA().habilitarCliqueBotaoFechar()

    SECTION_EDITAR().habilitarCliqueBotaoEditar()
    CHAT().habilitarChat()
  }

  return methods
}

module.exports = Module
