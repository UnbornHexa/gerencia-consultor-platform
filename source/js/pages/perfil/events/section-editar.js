/* Requires */

const REQUEST_USUARIOS = require('../../../global/requests/usuarios')
const HELPER_HEADER_PERFIL = require('../../../structures/header/helper-perfil')
const SECTION_EDITAR = require('../sections/section-editar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.conteudo .dados')
  const __buttonEditar = __section.querySelector(' button.salvar')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = SECTION_EDITAR().exportarDados()

      const camposObrigatoriosOk = SECTION_EDITAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      SECTION_EDITAR().bloquearBotao()
      REQUEST_USUARIOS().editar(dados)
        .then(() => {
          window.localStorage.setItem('nome-usuario', dados.nome)
          HELPER_HEADER_PERFIL().exibirNomeUsuario()
        })
        .finally(() => {
          SECTION_EDITAR().desbloquearBotao()
        })
    })
  }

  return methods
}

module.exports = Module
