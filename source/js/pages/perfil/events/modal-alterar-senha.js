/* Requires */

const REQUEST_USUARIOS = require('../../../global/requests/usuarios')
const MODAL_ALTERAR_SENHA = require('../modals/modal-alterar-senha')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_senha')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __buttonFechar = __modal.querySelector('button#fechar')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = MODAL_ALTERAR_SENHA().exportarDados()

      const camposObrigatoriosOk = MODAL_ALTERAR_SENHA().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const senhasIguaisOk = MODAL_ALTERAR_SENHA().verificarSenhasIguais()
      if (!senhasIguaisOk) return

      const tamanhoSenhaOk = MODAL_ALTERAR_SENHA().verificarTamanhoSenha()
      if (!tamanhoSenhaOk) return

      MODAL_ALTERAR_SENHA().bloquearBotao()
      REQUEST_USUARIOS().editarSenha(dados)
        .then(() => {
          MODAL_ALTERAR_SENHA().ocultar()
        })
        .finally(() => {
          MODAL_ALTERAR_SENHA().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_ALTERAR_SENHA().ocultar()
    })
  }

  return methods
}

module.exports = Module
