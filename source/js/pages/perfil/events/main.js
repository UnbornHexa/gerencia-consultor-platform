/* Requires */

const LOGOUT = require('../../../global/helpers/logout')
const CHAT = require('../../../global/events/chat')
const THEME = require('../../../global/helpers/theme')
const MODAL_ALTERAR_SENHA = require('../modals/modal-alterar-senha')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __buttonAlterarSenha = document.querySelector('section.conteudo button.senha')
  const __buttonSuporte = document.querySelector('section.conteudo .suporte button')
  const __buttonSair = document.querySelector('section.sair button')
  const __checkboxModoNoturno = document.querySelector('section.conteudo .noturno app-checkbox')

  // Methods

  methods.habilitarCliqueBotaoAlterarSenha = () => {
    __buttonAlterarSenha.addEventListener('click', () => {
      MODAL_ALTERAR_SENHA().mostrar()
    })
  }

  methods.habilitarCliqueBotaoSuporte = () => {
    __buttonSuporte.addEventListener('click', () => {
      CHAT().abrirChat()
    })
  }

  methods.habilitarCliqueBotaoSair = () => {
    __buttonSair.addEventListener('click', () => {
      LOGOUT().logout()
    })
  }

  methods.habilitarCliqueCheckboxModoNoturno = () => {
    __checkboxModoNoturno.addEventListener('click', () => {
      if (__checkboxModoNoturno.verificar()) {
        THEME().habilitarModoNoturno()
        return
      }

      THEME().desabilitarModoNoturno()
    })
  }

  return methods
}

module.exports = Module
