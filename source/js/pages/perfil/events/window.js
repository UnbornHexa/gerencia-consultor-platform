/* Requires */

const REQUEST_USUARIOS = require('../../../global/requests/usuarios')
const SECTION_EDITAR = require('../sections/section-editar')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarSecaoEditar = () => {
    window.addEventListener('load', () => {
      REQUEST_USUARIOS().receberPorId()
        .then((usuario) => {
          SECTION_EDITAR().importarDados(usuario)
        })
    })
  }

  return methods
}

module.exports = Module
