/* Requires */

const HELPER_LOGOUT = require('../../global/helpers/logout')
const HELPER_ASSINATURA = require('../../global/helpers/assinatura-expirada')
const TOGGLE_NOTURNO = require('./helpers/noturno')
const HELPER_HEADER_PERFIL = require('../../structures/header/helper-perfil')
const HELPER_MULTI_NOTIFICACOES = require('../../structures/multi/helper-notificacoes')
const EVENTOS = require('./events/events')
const INICIADORES = require('./inits/init')

/* Start */

HELPER_LOGOUT().checkTokenOk()
HELPER_ASSINATURA().checkAssinaturaOk()
INICIADORES().iniciarTodosOsComponentes()
EVENTOS().habilitarTodosOsEventos()
TOGGLE_NOTURNO().verificarToggleModoNoturno()
HELPER_HEADER_PERFIL().exibirNomeUsuario()
HELPER_MULTI_NOTIFICACOES().verificarNovasNotificacoes()
