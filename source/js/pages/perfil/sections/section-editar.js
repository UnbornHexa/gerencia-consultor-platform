/* Requires */

const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.conteudo .dados')
  const __inputNome = __section.querySelector('input#nome')
  const __inputDataNascimento = __section.querySelector('input#data_nascimento')
  const __inputTelefone = __section.querySelector('input#telefone')
  const __inputEmail = __section.querySelector('input#email')
  const __buttonEditar = __section.querySelector('button#editar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Form

  methods.bloquearBotao = () => {
    __buttonEditar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEditar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputNome.value = ''
    __inputDataNascimento.value = ''
    __inputTelefone.value = ''
    __inputEmail.value = ''
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputNome.value && __inputTelefone.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  // Data

  methods.importarDados = (dados) => {
    __inputNome.value = dados.nome || ''
    __inputDataNascimento.value = DATE().convertMongoDateToBR(dados.dataNascimento) || ''
    __inputTelefone.value = dados.contato.telefone1 || ''
    __inputEmail.value = dados.email || ''
  }

  methods.exportarDados = () => {
    const usuario = {}
    usuario.contato = {}

    // required
    usuario.nome = __inputNome.value || ''
    usuario.contato.telefone1 = __inputTelefone.value || ''

    // opcional
    if (__inputDataNascimento.value) usuario.dataNascimento = DATE().convertBRToTimestamp(__inputDataNascimento.value)

    return usuario
  }

  return methods
}

module.exports = Module
