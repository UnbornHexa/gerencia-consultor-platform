/* Requires */

const EVENTS = require('../../../global/events/events')
const MAIN = require('./main')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    MAIN().habilitarCliqueBotaoSair()
  }

  return methods
}

module.exports = Module
