/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_LISTA = require('./componente-lista')
const COMPONENTE_SELECT = require('./componente-select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_LISTA().iniciar()
    COMPONENTE_SELECT().iniciar()
  }

  return methods
}

module.exports = Module
