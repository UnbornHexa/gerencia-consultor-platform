/* Requires */

const COMPONENT_SELECT = require('../../../global/components/select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-select', COMPONENT_SELECT)

    const selectModalAdicionar = document.querySelector('section#modal_adicionar app-select')
    const selectModalUniversal = document.querySelector('section#modal_universal app-select')

    selectModalAdicionar.importarEstados()
    selectModalUniversal.importarEstados()
  }

  return methods
}

module.exports = Module
