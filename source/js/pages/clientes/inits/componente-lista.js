/* Requires */

const REQUEST_CLIENTES = require('../../../global/requests/clientes')
const COMPONENT_LISTA = require('../components/lista')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-lista', COMPONENT_LISTA)
  }

  methods.atualizar = () => {
    const __componenteLista = document.querySelector('app-lista')

    REQUEST_CLIENTES().receberTodos()
      .then(clientes => {
        __componenteLista.importarItens(clientes)
      })
  }

  return methods
}

module.exports = Module
