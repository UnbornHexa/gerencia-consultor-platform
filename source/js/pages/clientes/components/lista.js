/* Requires */

const COMPONENT_LISTA = require('../../../global/components/lista')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const REQUEST_CLIENTES = require('../../../global/requests/clientes')

/* Module */

class Lista extends COMPONENT_LISTA {
  constructor () {
    super()
    this.habilitarCliqueItem()
  }

  _renderizarNovoItem (item) {
    const itensUl = this.querySelector('ul.itens')

    const li =
    `
    <li class="item">
      <div class="info">
        <input type="hidden" value="${item._id}">
        <div class="icone-tela clientes"></div>
        <div>
          <p class="principal">${item.nome}</p>
          <p class="secundario">${item.contato.telefone1}</p>
        </div>
      </div>
      <div class="opcoes">
        <div class="icone"></div>
      </div>
    </li>
    `

    itensUl.innerHTML += li
  }

  habilitarCliqueItem () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('ul li')) return

      MODAL_UNIVERSAL().mostrar()

      const idItem = evento.target.querySelector('.info input').value
      REQUEST_CLIENTES().receberPorId(idItem)
        .then((retorno) => {
          MODAL_UNIVERSAL().importarDados(retorno)
        })
    })
  }
}

module.exports = Lista
