/* Requires */

const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __buttonAdicionar = document.querySelector('main header .adicionar')

  // Methods

  methods.habilitarCliqueBotaoAdicionar = () => {
    __buttonAdicionar.addEventListener('click', () => {
      MODAL_ADICIONAR().mostrar()
    })
  }

  return methods
}

module.exports = Module
