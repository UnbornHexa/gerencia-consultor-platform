/* Requires */

const REQUEST_CLIENTES = require('../../../global/requests/clientes')
const REQUEST_CEP = require('../../../global/requests/cep')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_DELETAR = require('../modals/modal-deletar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __buttonAvancar = __modal.querySelector('button#avancar')
  const __buttonVoltar = __modal.querySelector('button#voltar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonOpcoesDeletar = __modal.querySelector('.opcoes button#deletar')
  const __inputCep = __modal.querySelector('input#cep')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()

      const camposObrigatoriosOk = MODAL_UNIVERSAL().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      MODAL_UNIVERSAL().bloquearBotao()
      REQUEST_CLIENTES().editar(dados, id)
        .then(() => {
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_UNIVERSAL().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoAvancar = () => {
    __buttonAvancar.addEventListener('click', () => {
      MODAL_UNIVERSAL().mostrarTelaEndereco()
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_UNIVERSAL().mostrarTelaInformacoes()
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_UNIVERSAL().ocultar()
    })
  }

  // Opcoes

  methods.habilitarCliqueBotaoOpcoesDeletar = () => {
    __buttonOpcoesDeletar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { nome: dados.nome, id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_DELETAR().mostrar()
      MODAL_DELETAR().importarDados(obj)
    })
  }

  // Cep

  methods.habilitarBuscaPorCep = () => {
    __inputCep.addEventListener('keyup', (evento) => {
      const cep = evento.target.value.replace(/[-.]/g, '')
      if (cep.length !== 8) return

      REQUEST_CEP().receber(cep)
        .then((endereco) => {
          MODAL_UNIVERSAL().importarEndereco(endereco)
        })
    })
  }

  return methods
}

module.exports = Module
