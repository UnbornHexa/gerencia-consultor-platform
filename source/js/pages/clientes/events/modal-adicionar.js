/* Requires */

const REQUEST_CLIENTES = require('../../../global/requests/clientes')
const REQUEST_CEP = require('../../../global/requests/cep')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __buttonAvancar = __modal.querySelector('button#avancar')
  const __buttonVoltar = __modal.querySelector('button#voltar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __inputCep = __modal.querySelector('input#cep')

  // Methods

  methods.habilitarCliqueBotaoAdicionar = () => {
    __buttonAdicionar.addEventListener('click', () => {
      const dados = MODAL_ADICIONAR().exportarDados()

      const camposObrigatoriosOk = MODAL_ADICIONAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      MODAL_ADICIONAR().bloquearBotao()
      REQUEST_CLIENTES().adicionar(dados)
        .then(() => {
          MODAL_ADICIONAR().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_ADICIONAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoAvancar = () => {
    __buttonAvancar.addEventListener('click', () => {
      MODAL_ADICIONAR().mostrarTelaEndereco()
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_ADICIONAR().mostrarTelaInformacoes()
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_ADICIONAR().ocultar()
    })
  }

  // Cep

  methods.habilitarBuscaPorCep = () => {
    __inputCep.addEventListener('keyup', (evento) => {
      const cep = evento.target.value.replace(/[-.]/g, '')
      if (cep.length !== 8) return

      REQUEST_CEP().receber(cep)
        .then((endereco) => {
          MODAL_ADICIONAR().importarEndereco(endereco)
        })
    })
  }

  return methods
}

module.exports = Module
