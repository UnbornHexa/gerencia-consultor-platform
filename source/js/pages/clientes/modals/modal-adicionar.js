/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __inputNome = __modal.querySelector('input#nome')
  const __inputTelefone = __modal.querySelector('input#telefone')
  const __inputDataNascimento = __modal.querySelector('input#data_nascimento')
  const __inputObservacao = __modal.querySelector('input#observacao')
  const __inputCep = __modal.querySelector('input#cep')
  const __inputCidade = __modal.querySelector('input#cidade')
  const __inputBairro = __modal.querySelector('input#bairro')
  const __inputRua = __modal.querySelector('input#rua')
  const __inputNumero = __modal.querySelector('input#numero')
  const __selectEstado = __modal.querySelector('app-select')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __componenteAlerta = document.querySelector('app-alerta')
  const __telaInformacoes = __modal.querySelector('.telas .tela[data-id="1"]')
  const __telaEndereco = __modal.querySelector('.telas .tela[data-id="2"]')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.add('ocultar')
    __modal.classList.remove('mostrar-block')
    methods.limparCampos()
  }

  methods.mostrarTelaInformacoes = () => {
    __telaInformacoes.classList.add('mostrar-flex')
    __telaInformacoes.classList.remove('ocultar')
    __telaEndereco.classList.add('ocultar')
    __telaEndereco.classList.remove('mostrar-flex')
  }

  methods.mostrarTelaEndereco = () => {
    __telaInformacoes.classList.add('ocultar')
    __telaInformacoes.classList.remove('mostrar-flex')
    __telaEndereco.classList.add('mostrar-flex')
    __telaEndereco.classList.remove('ocultar')
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonAdicionar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonAdicionar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputNome.value = ''
    __inputTelefone.value = ''
    __inputDataNascimento.value = ''
    __inputObservacao.value = ''
    __inputCep.value = ''
    __inputCidade.value = ''
    __inputBairro.value = ''
    __inputRua.value = ''
    __inputNumero.value = ''
    __selectEstado.limparSelecionado()

    methods.mostrarTelaInformacoes()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputNome.value && __inputTelefone.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  // Data

  methods.exportarDados = () => {
    const cliente = {}
    cliente.endereco = {}
    cliente.contato = {}

    // obrigatorio
    cliente.nome = __inputNome.value
    cliente.contato.telefone1 = __inputTelefone.value

    // opcional
    if (__inputDataNascimento.value) cliente.dataNascimento = DATE().convertBRToTimestamp(__inputDataNascimento.value)
    if (__inputObservacao.value) cliente.observacao = __inputObservacao.value

    if (__inputCep.value) cliente.endereco.cep = __inputCep.value
    if (!__selectEstado.vazio()) cliente.endereco.estado = __selectEstado.exportarValorSelecionado()
    if (__inputCidade.value) cliente.endereco.cidade = __inputCidade.value
    if (__inputBairro.value) cliente.endereco.bairro = __inputBairro.value
    if (__inputRua.value) cliente.endereco.rua = __inputRua.value
    if (__inputNumero.value) cliente.endereco.numero = __inputNumero.value

    return cliente
  }

  methods.importarEndereco = (endereco) => {
    __inputCidade.value = endereco.localidade || ''
    __inputBairro.value = endereco.bairro || ''
    __inputRua.value = endereco.logradouro || ''
    __selectEstado.selecionarOpcao(endereco.uf)
  }

  return methods
}

module.exports = Module
