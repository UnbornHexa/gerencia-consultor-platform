/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const DATE = require('../../../global/helpers/date')
const REQUEST_NOTIFICACOES = require('../../../global/requests/notificacoes')
const COMPONENT_LISTA = require('../../../global/components/lista')
const MODAL_VISUALIZAR = require('../modals/modal-visualizar')

/* Module */

class Lista extends COMPONENT_LISTA {
  constructor () {
    super()
    this.habilitarCliqueItem()
  }

  _renderizarNovoItem (item) {
    const itensUl = this.querySelector('ul.itens')
    const mensagem = (item.mensagem.length === 60) ? item.mensagem + '...' : item.mensagem
    const classeVisualizado = (!item.visualizado) ? 'novo' : ''
    const data = DATE().convertMongoDateToBR(item.dataRegistro)

    const li =
    `
    <li class="item ${classeVisualizado}">
      <div class="info">
        <input type="hidden" value="${item._id}">
        <div class="icone-tela notificacoes"></div>
        <div>
          <p class="principal">${item.titulo}</p>
          <p class="secundario">${data} - ${mensagem}</p>
        </div>
      </div>
      <div class="opcoes">
        <div class="icone"></div>
      </div>
    </li>
    `

    itensUl.innerHTML += li
  }

  filtrar (texto) {
    this.__state.itensFiltrados = this.__state.itens.filter(item => {
      const termoPesquisado = ACCENT().remove(texto.toString().toUpperCase())
      const itemAVerificar = ACCENT().remove(item.titulo.toUpperCase())
      return (itemAVerificar.includes(termoPesquisado))
    })

    this._exibirItensDaPagina(1)
  }

  habilitarCliqueItem () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('ul li')) return

      MODAL_VISUALIZAR().mostrar()

      const idItem = evento.target.querySelector('.info input').value
      REQUEST_NOTIFICACOES().receberPorId(idItem)
        .then((retorno) => {
          MODAL_VISUALIZAR().importarDados(retorno)

          REQUEST_NOTIFICACOES().receberTodos()
            .then(notificacoes => { this.importarItens(notificacoes) })
        })
    })
  }
}

module.exports = Lista
