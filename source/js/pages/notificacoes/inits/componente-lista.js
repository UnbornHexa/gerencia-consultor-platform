/* Requires */

const REQUEST_NOTIFICACOES = require('../../../global/requests/notificacoes')
const COMPONENT_LISTA = require('../components/lista')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-lista', COMPONENT_LISTA)
  }

  methods.atualizar = () => {
    const __componenteLista = document.querySelector('app-lista')

    REQUEST_NOTIFICACOES().receberTodos()
      .then(notificacoes => {
        __componenteLista.importarItens(notificacoes)
      })
  }

  return methods
}

module.exports = Module
