/* Requires */

const EVENTS = require('../../../global/events/events')
const CHAT = require('../../../global/events/chat')
const TOGGLE = require('../../../structures/navbar/helper-toggle')
const WINDOW = require('./window')
const PESQUISA = require('./pesquisa')
const MODAL_VISUALIZAR = require('./modal-visualizar')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    TOGGLE().habilitarCliqueToggle()
    WINDOW().povoarListaAoIniciar()
    PESQUISA().habilitarBarraPesquisa()
    MODAL_VISUALIZAR().habilitarCliqueBotaoVoltar()
    CHAT().habilitarChat()
  }

  return methods
}

module.exports = Module
