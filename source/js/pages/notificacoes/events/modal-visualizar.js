/* Requires */

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar')
  const __buttonVoltar = __modal.querySelector('button#voltar')

  // Methods
  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_VISUALIZAR().ocultar()
    })
  }

  return methods
}

module.exports = Module
