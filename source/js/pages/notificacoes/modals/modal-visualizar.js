/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar')
  const __inputId = __modal.querySelector('input#id')
  const __pTitulo = __modal.querySelector('p#titulo')
  const __pMensagem = __modal.querySelector('p#mensagem')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.limparCampos = () => {
    __inputId.value = ''
    __pTitulo.innerText = ''
    __pMensagem.innerText = ''
  }

  // Data

  methods.importarDados = (dados) => {
    __inputId.value = dados._id
    __pTitulo.innerText = dados.titulo || ''
    __pMensagem.innerText = dados.mensagem || ''
  }

  return methods
}

module.exports = Module
