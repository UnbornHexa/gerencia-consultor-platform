/* Requires */

const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_CANCELAR = require('../modals/modal-cancelar')
const MODAL_CONFIRMAR = require('../modals/modal-confirmar')
const MODAL_ALTERAR = require('../modals/modal-alterar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonCancelar = __modal.querySelector('button#cancelar')
  const __buttonConfirmar = __modal.querySelector('button#confirmar')
  const __buttonAlterar = __modal.querySelector('button#alterar')

  // Methods

  methods.habilitarCliqueBotaoCancelar = () => {
    __buttonCancelar.addEventListener('click', () => {
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_CANCELAR().mostrar()
      MODAL_CANCELAR().importarDados(obj)
    })
  }

  methods.habilitarCliqueBotaoConfirmar = () => {
    __buttonConfirmar.addEventListener('click', () => {
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_CONFIRMAR().mostrar()
      MODAL_CONFIRMAR().importarDados(obj)
    })
  }

  methods.habilitarCliqueBotaoAlterar = () => {
    __buttonAlterar.addEventListener('click', () => {
      const id = MODAL_UNIVERSAL().exportarId()
      const data = MODAL_UNIVERSAL().exportarData()
      const obj = { id: id, data: data }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_ALTERAR().mostrar()
      MODAL_ALTERAR().importarDados(obj)
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_UNIVERSAL().ocultar()
    })
  }

  return methods
}

module.exports = Module
