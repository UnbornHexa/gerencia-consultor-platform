const Module = () => {
  const methods = {}

  // Internal Variables

  const __componenteLista = document.querySelector('app-lista')
  const __inputPesquisa = document.querySelector('section.menu .filtros .pesquisa input')
  let __cancelarPesquisa = null

  // Methods

  methods.habilitarBarraPesquisa = () => {
    __inputPesquisa.addEventListener('keyup', (evento) => {
      const texto = evento.target.value
      clearTimeout(__cancelarPesquisa)

      __cancelarPesquisa = setTimeout(() => {
        __componenteLista.filtrar(texto)
      }, 300)
    })
  }

  return methods
}

module.exports = Module
