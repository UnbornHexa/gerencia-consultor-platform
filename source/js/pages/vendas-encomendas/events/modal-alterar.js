/* Requires */

const REQUEST_ENCOMENDAS = require('../../../global/requests/encomendas')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_ALTERAR = require('../modals/modal-alterar')
const INIT_LISTA = require('../inits/componente-lista')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_alterar')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __buttonVoltar = __modal.querySelector('button#voltar')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = MODAL_ALTERAR().exportarDados()
      const id = MODAL_ALTERAR().exportarId()

      MODAL_ALTERAR().bloquearBotao()
      REQUEST_ENCOMENDAS().editar(dados, id)
        .then(() => {
          MODAL_ALTERAR().ocultar()
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_ALTERAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_ALTERAR().ocultar()
      MODAL_UNIVERSAL().mostrarParcialmente()
    })
  }

  return methods
}

module.exports = Module
