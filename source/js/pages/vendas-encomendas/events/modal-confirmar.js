/* Requires */

const REQUEST_ENCOMENDAS = require('../../../global/requests/encomendas')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_CONFIRMAR = require('../modals/modal-confirmar')
const INIT_LISTA = require('../inits/componente-lista')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_confirmar')
  const __buttonConfirmar = __modal.querySelector('button#confirmar')
  const __buttonVoltar = __modal.querySelector('button#voltar')

  // Methods

  methods.habilitarCliqueBotaoConfirmar = () => {
    __buttonConfirmar.addEventListener('click', () => {
      const dados = { entregue: true }
      const id = MODAL_CONFIRMAR().exportarId()

      MODAL_CONFIRMAR().bloquearBotao()
      REQUEST_ENCOMENDAS().editar(dados, id)
        .then(() => {
          MODAL_CONFIRMAR().ocultar()
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_CONFIRMAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_CONFIRMAR().ocultar()
      MODAL_UNIVERSAL().mostrarParcialmente()
    })
  }

  return methods
}

module.exports = Module
