/* Requires */

const INIT_LISTA = require('../inits/componente-lista')
const INIT_SELECT = require('../inits/componente-select')
const INIT_ITENS_VENDA = require('../inits/componente-itens-venda')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarListaAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_LISTA().atualizar()
    })
  }

  methods.povoarSelectAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_SELECT().atualizarNomesClientes()
    })
  }

  methods.povoarItensVendaAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_ITENS_VENDA().atualizar()
    })
  }

  return methods
}

module.exports = Module
