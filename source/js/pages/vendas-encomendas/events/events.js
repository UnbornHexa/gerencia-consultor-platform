/* Requires */

const EVENTS = require('../../../global/events/events')
const CHAT = require('../../../global/events/chat')
const TOGGLE = require('../../../structures/navbar/helper-toggle')
const WINDOW = require('./window')
const MODAL_UNIVERSAL = require('./modal-universal')
const MODAL_CANCELAR = require('./modal-cancelar')
const MODAL_CONFIRMAR = require('./modal-confirmar')
const MODAL_ALTERAR = require('./modal-alterar')
const PESQUISA = require('./pesquisa')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    TOGGLE().habilitarCliqueToggle()

    WINDOW().povoarListaAoIniciar()
    WINDOW().povoarSelectAoIniciar()
    WINDOW().povoarItensVendaAoIniciar()

    MODAL_UNIVERSAL().habilitarCliqueBotaoAlterar()
    MODAL_UNIVERSAL().habilitarCliqueBotaoConfirmar()
    MODAL_UNIVERSAL().habilitarCliqueBotaoCancelar()
    MODAL_UNIVERSAL().habilitarCliqueBotaoFechar()

    MODAL_CANCELAR().habilitarCliqueBotaoCancelar()
    MODAL_CANCELAR().habilitarCliqueBotaoVoltar()

    MODAL_CONFIRMAR().habilitarCliqueBotaoConfirmar()
    MODAL_CONFIRMAR().habilitarCliqueBotaoVoltar()

    MODAL_ALTERAR().habilitarCliqueBotaoEditar()
    MODAL_ALTERAR().habilitarCliqueBotaoVoltar()

    PESQUISA().habilitarBarraPesquisa()
    CHAT().habilitarChat()
  }

  return methods
}

module.exports = Module
