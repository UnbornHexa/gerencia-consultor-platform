/* Requires */

const REQUEST_ENCOMENDAS = require('../../../global/requests/encomendas')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_CANCELAR = require('../modals/modal-cancelar')
const INIT_LISTA = require('../inits/componente-lista')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_cancelar')
  const __buttonCancelar = __modal.querySelector('button#cancelar')
  const __buttonVoltar = __modal.querySelector('button#voltar')

  // Methods

  methods.habilitarCliqueBotaoCancelar = () => {
    __buttonCancelar.addEventListener('click', () => {
      const dados = { entregue: false }
      const id = MODAL_CANCELAR().exportarId()

      MODAL_CANCELAR().bloquearBotao()
      REQUEST_ENCOMENDAS().editar(dados, id)
        .then(() => {
          MODAL_CANCELAR().ocultar()
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_CANCELAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_CANCELAR().ocultar()
      MODAL_UNIVERSAL().mostrarParcialmente()
    })
  }

  return methods
}

module.exports = Module
