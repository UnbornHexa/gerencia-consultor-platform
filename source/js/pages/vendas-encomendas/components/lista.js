/* Requires */

const REQUEST_ENCOMENDAS = require('../../../global/requests/encomendas')
const COMPONENT_LISTA = require('../../../global/components/lista')
const DATE = require('../../../global/helpers/date')
const ACCENT = require('../../../global/helpers/accent')
const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

class Lista extends COMPONENT_LISTA {
  constructor () {
    super()
    this.habilitarCliqueItem()
  }

  _renderizarNovoItem (item) {
    const itensUl = this.querySelector('ul.itens')

    const data = DATE().convertMongoDateToBR(item.data)
    const quantidadeProdutos = item.produtos.length
    const nomeCliente = item.idCliente.nome
    const numeroVenda = item.idVenda.numero

    const frasePluralSingular = (quantidadeProdutos > 1) ? 'produtos para serem entregues' : 'produto para ser entregue'
    const classeEntregue = (item.entregue) ? 'entregue' : ''

    const li =
    `
    <li class="item ${classeEntregue}">
      <div class="info">
        <input type="hidden" value="${item._id}">
        <div class="icone-tela encomendas"></div>
        <div>
          <p class="principal">${nomeCliente} <span class="data">entregar dia ${data}</span></p>
          ${(!item.entregue)
              ? `<p class="secundario">Há ${quantidadeProdutos} ${frasePluralSingular} na Venda <span id="venda">#${numeroVenda}</span></p>`
              : `<p class="secundario">Os produtos da <span id="venda">Venda #${numeroVenda}</span> já foram entregues.</p>`
          }
        </div>
      </div>
      <div class="opcoes">
        <div class="icone"></div>
      </div>
    </li>
    `

    itensUl.innerHTML += li
  }

  filtrar (texto) {
    this.__state.itensFiltrados = this.__state.itens.filter(item => {
      const nomeCliente = item.idCliente.nome || ''
      const termoPesquisado = ACCENT().remove(texto.toString().toUpperCase())
      const itemAVerificar = ACCENT().remove(nomeCliente.toUpperCase())
      return (itemAVerificar.includes(termoPesquisado))
    })

    this._exibirItensDaPagina(1)
  }

  habilitarCliqueItem () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('ul li')) return

      MODAL_UNIVERSAL().mostrar()

      const idItem = evento.target.querySelector('.info input').value
      REQUEST_ENCOMENDAS().receberPorId(idItem)
        .then((retorno) => {
          MODAL_UNIVERSAL().importarDados(retorno)
        })
    })
  }
}

module.exports = Lista
