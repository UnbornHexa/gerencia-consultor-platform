const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_cancelar')
  const __inputId = __modal.querySelector('input#id')
  const __buttonCancelar = __modal.querySelector('button#cancelar')

  // Modal

  methods.mostrar = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonCancelar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonCancelar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
  }

  // Data

  methods.importarDados = (dados) => {
    __inputId.value = dados.id
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  return methods
}

module.exports = Module
