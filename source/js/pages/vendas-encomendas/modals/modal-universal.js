/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __inputId = __modal.querySelector('input#id')
  const __selectCliente = __modal.querySelector('app-select')
  const __datepickerData = __modal.querySelector('app-date-picker')
  const __itensVenda = __modal.querySelector('app-itens-venda')
  const __buttonConfirmar = __modal.querySelector('button#confirmar')
  const __buttonCancelar = __modal.querySelector('button#cancelar')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.mostrarParcialmente = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.ocultarParcialmente = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
  }

  // Form

  methods.mostrarBotaoCancelar = () => {
    __buttonConfirmar.classList.add('ocultar')
    __buttonCancelar.classList.remove('ocultar')
  }

  methods.mostrarBotaoConfirmar = () => {
    __buttonCancelar.classList.add('ocultar')
    __buttonConfirmar.classList.remove('ocultar')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __selectCliente.limparSelecionado()
    __selectCliente.desativar()
    __datepickerData.limpar()
    __datepickerData.desativar()
    __itensVenda.limpar()
    __itensVenda.desativar()

    methods.mostrarBotaoConfirmar()
  }

  // Data

  methods.importarDados = (dados) => {
    const data = DATE().convertMongoDateToBR(dados.data)

    __inputId.value = dados._id
    __selectCliente.selecionarOpcao(dados.idCliente)
    __datepickerData.selecionarData(data)
    __itensVenda.importarProdutos(dados.produtos)

    if (dados.entregue) methods.mostrarBotaoCancelar()
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  methods.exportarData = () => {
    return __datepickerData.exportarData()
  }

  return methods
}

module.exports = Module
