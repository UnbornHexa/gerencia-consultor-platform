/* Requires */

const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_alterar')
  const __inputId = __modal.querySelector('input#id')
  const __datepickerData = __modal.querySelector('app-date-picker#data')
  const __datepickerNovaData = __modal.querySelector('app-date-picker#data_nova')
  const __buttonEditar = __modal.querySelector('button#editar')

  // Modal

  methods.mostrar = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonEditar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEditar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __datepickerData.desativar()
    __datepickerNovaData.resetar()
  }

  // Data

  methods.importarDados = (dados) => {
    __inputId.value = dados.id
    __datepickerData.selecionarData(dados.data)
  }

  methods.exportarDados = () => {
    const data = DATE().convertBRToTimestamp(__datepickerNovaData.exportarData())

    const encomenda = {}
    encomenda.data = data

    return encomenda
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  return methods
}

module.exports = Module
