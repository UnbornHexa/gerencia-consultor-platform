/* Requires */

const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const COMPONENT_ITENS_VENDA = require('../../../global/components/itens-venda')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-itens-venda', COMPONENT_ITENS_VENDA)
  }

  methods.atualizar = () => {
    const __componenteItensVendaModalUniversal = document.querySelector('section#modal_universal app-itens-venda')

    REQUEST_PRODUTOS().receberTodos()
      .then(produtos => {
        __componenteItensVendaModalUniversal.povoarSelect(produtos)
      })
  }

  return methods
}

module.exports = Module
