/* Requires */

const REQUEST_ENCOMENDAS = require('../../../global/requests/encomendas')
const COMPONENT_LISTA = require('../components/lista')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-lista', COMPONENT_LISTA)
  }

  methods.atualizar = () => {
    const __componenteLista = document.querySelector('app-lista')
    const __dateSelect = document.querySelector('app-date-select')

    const ano = __dateSelect.exportarDataSelecionada().ano
    const mes = __dateSelect.exportarDataSelecionada().mes

    REQUEST_ENCOMENDAS().receberTodos(ano, mes)
      .then(encomendas => {
        __componenteLista.importarItens(encomendas)
      })
  }

  return methods
}

module.exports = Module
