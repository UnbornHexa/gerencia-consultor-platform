/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_LISTA = require('./componente-lista')
const COMPONENTE_DATE_SELECT = require('./componente-date-select')
const COMPONENTE_DATE_PICKER = require('./componente-date-picker')
const COMPONENTE_ITENS_VENDA = require('./componente-itens-venda')
const COMPONENTE_SELECT = require('./componente-select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_DATE_SELECT().iniciar()
    COMPONENTE_LISTA().iniciar()
    COMPONENTE_SELECT().iniciar()
    COMPONENTE_DATE_PICKER().iniciar()
    COMPONENTE_ITENS_VENDA().iniciar()
  }

  return methods
}

module.exports = Module
