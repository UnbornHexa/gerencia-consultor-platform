/* Requires */

const COMPONENT_ALERTA = require('../../../global/components/alerta')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-alerta', COMPONENT_ALERTA)
  }

  return methods
}

module.exports = Module
