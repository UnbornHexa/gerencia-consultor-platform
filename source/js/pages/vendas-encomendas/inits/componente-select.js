/* Requires */

const REQUEST_CLIENTES = require('../../../global/requests/clientes')
const COMPONENT_SELECT = require('../../../global/components/select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-select', COMPONENT_SELECT)
  }

  methods.atualizarNomesClientes = () => {
    const __componenteSelect = document.querySelector('section#modal_universal app-select')

    REQUEST_CLIENTES().receberNomes()
      .then(clientes => {
        const opcoes = clientes.map(cliente => {
          return {
            texto: cliente.nome,
            valor: cliente._id
          }
        })
        __componenteSelect.importarOpcoes(opcoes)
      })
  }

  return methods
}

module.exports = Module
