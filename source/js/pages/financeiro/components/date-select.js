/* Requires */

const COMPONENT_DATE_SELECT = require('../../../global/components/date-select')
const HELPER_ATUALIZAR_GRAFICOS = require('../helpers/atualizar-graficos')

/* Module */

class DateSelect extends COMPONENT_DATE_SELECT {
  habilitarCliqueBotaoAvancar () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('button.avancar')
      if (evento.target !== elemento) return

      this._avancarMes()
      HELPER_ATUALIZAR_GRAFICOS().atualizar()
    })
  }

  habilitarCliqueBotaoVoltar () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('button.voltar')
      if (evento.target !== elemento) return

      this._retrocederMes()
      HELPER_ATUALIZAR_GRAFICOS().atualizar()
    })
  }
}

module.exports = DateSelect
