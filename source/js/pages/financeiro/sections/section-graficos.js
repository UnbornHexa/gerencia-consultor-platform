/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __sectionGraficoReceitas = document.querySelector('section.resumo .receitas')
  const __sectionGraficoDespesas = document.querySelector('section.resumo .despesas')
  const __spanTotalReceitas = document.querySelector('section.resumo .lucro span#total_receitas')
  const __spanTotalDespesas = document.querySelector('section.resumo .lucro span#total_despesas')
  const __spanTotalLucro = document.querySelector('section.resumo .lucro span#total_lucro')
  const __svgReceitasDinheiro = __sectionGraficoReceitas.querySelector('svg circle#receitas_grafico_dinheiro')
  const __svgReceitasCredito = __sectionGraficoReceitas.querySelector('svg circle#receitas_grafico_credito')
  const __svgReceitasDebito = __sectionGraficoReceitas.querySelector('svg circle#receitas_grafico_debito')
  const __svgReceitasBoleto = __sectionGraficoReceitas.querySelector('svg circle#receitas_grafico_boleto')
  const __svgDespesasDinheiro = __sectionGraficoDespesas.querySelector('svg circle#despesas_grafico_dinheiro')
  const __svgDespesasCredito = __sectionGraficoDespesas.querySelector('svg circle#despesas_grafico_credito')
  const __svgDespesasDebito = __sectionGraficoDespesas.querySelector('svg circle#despesas_grafico_debito')
  const __svgDespesasBoleto = __sectionGraficoDespesas.querySelector('svg circle#despesas_grafico_boleto')
  const __pReceitasDinheiro = __sectionGraficoReceitas.querySelector('.tabela p#receitas_valor_dinheiro')
  const __pReceitasCredito = __sectionGraficoReceitas.querySelector('.tabela p#receitas_valor_credito')
  const __pReceitasDebito = __sectionGraficoReceitas.querySelector('.tabela p#receitas_valor_debito')
  const __pReceitasBoleto = __sectionGraficoReceitas.querySelector('.tabela p#receitas_valor_boleto')
  const __pDespesasDinheiro = __sectionGraficoDespesas.querySelector('.tabela p#despesas_valor_dinheiro')
  const __pDespesasCredito = __sectionGraficoDespesas.querySelector('.tabela p#despesas_valor_credito')
  const __pDespesasDebito = __sectionGraficoDespesas.querySelector('.tabela p#despesas_valor_debito')
  const __pDespesasBoleto = __sectionGraficoDespesas.querySelector('.tabela p#despesas_valor_boleto')

  // Methods

  methods.construirGraficos = (receitas, despesas) => {
    const resumoReceitas = methods._calcularResumo(receitas)
    const resumoDespesas = methods._calcularResumo(despesas)
    const totalReceitas = methods._calcularTotal(resumoReceitas)
    const totalDespesas = methods._calcularTotal(resumoDespesas)

    methods.exibirResumoReceitas(resumoReceitas)
    methods.exibirResumoDespesas(resumoDespesas)
    methods.exibirTotal(totalReceitas, totalDespesas)

    methods.exibirGraficoReceitas(resumoReceitas, totalReceitas)
    methods.exibirGraficoDespesas(resumoDespesas, totalDespesas)
  }

  // Exibir

  methods.exibirResumoReceitas = (receitas) => {
    __pReceitasDinheiro.innerText = FORMAT().money(receitas.dinheiro || 0)
    __pReceitasCredito.innerText = FORMAT().money(receitas.credito || 0)
    __pReceitasDebito.innerText = FORMAT().money(receitas.debito || 0)
    __pReceitasBoleto.innerText = FORMAT().money(receitas.boleto || 0)
  }

  methods.exibirResumoDespesas = (despesas) => {
    __pDespesasDinheiro.innerText = FORMAT().money(despesas.dinheiro || 0)
    __pDespesasCredito.innerText = FORMAT().money(despesas.credito || 0)
    __pDespesasDebito.innerText = FORMAT().money(despesas.debito || 0)
    __pDespesasBoleto.innerText = FORMAT().money(despesas.boleto || 0)
  }

  methods.exibirTotal = (receitas, despesas) => {
    __spanTotalReceitas.innerText = FORMAT().money(receitas || 0)
    __spanTotalDespesas.innerText = FORMAT().money(despesas || 0)
    __spanTotalLucro.innerText = FORMAT().money((receitas - despesas) || 0)
  }

  // Grafico

  methods.exibirGraficoReceitas = (resumoReceitas, totalReceitas) => {
    const porcentagemDinheiro = methods._calcularPorcentagemGrafico(resumoReceitas.dinheiro, totalReceitas)
    const porcentagemCredito = methods._calcularPorcentagemGrafico(resumoReceitas.credito, totalReceitas)
    const porcentagemDebito = methods._calcularPorcentagemGrafico(resumoReceitas.debito, totalReceitas)
    const porcentagemBoleto = methods._calcularPorcentagemGrafico(resumoReceitas.boleto, totalReceitas)

    const circulo4 = methods._arredondar(porcentagemBoleto)
    const circulo3 = methods._arredondar((circulo4 + porcentagemDebito))
    const circulo2 = methods._arredondar((circulo3 + porcentagemCredito))
    const circulo1 = methods._arredondar((circulo2 + porcentagemDinheiro))

    __svgReceitasBoleto.setAttribute('stroke-dasharray', `${circulo4} 100`)
    __svgReceitasDebito.setAttribute('stroke-dasharray', `${circulo3} 100`)
    __svgReceitasCredito.setAttribute('stroke-dasharray', `${circulo2} 100`)
    __svgReceitasDinheiro.setAttribute('stroke-dasharray', `${circulo1} 100`)
  }

  methods.exibirGraficoDespesas = (resumoDespesas, totalDespesas) => {
    const porcentagemDinheiro = methods._calcularPorcentagemGrafico(resumoDespesas.dinheiro, totalDespesas)
    const porcentagemCredito = methods._calcularPorcentagemGrafico(resumoDespesas.credito, totalDespesas)
    const porcentagemDebito = methods._calcularPorcentagemGrafico(resumoDespesas.debito, totalDespesas)
    const porcentagemBoleto = methods._calcularPorcentagemGrafico(resumoDespesas.boleto, totalDespesas)

    const circulo4 = methods._arredondar(porcentagemBoleto)
    const circulo3 = methods._arredondar((circulo4 + porcentagemDebito))
    const circulo2 = methods._arredondar((circulo3 + porcentagemCredito))
    const circulo1 = methods._arredondar((circulo2 + porcentagemDinheiro))

    __svgDespesasBoleto.setAttribute('stroke-dasharray', `${circulo4} 100`)
    __svgDespesasDebito.setAttribute('stroke-dasharray', `${circulo3} 100`)
    __svgDespesasCredito.setAttribute('stroke-dasharray', `${circulo2} 100`)
    __svgDespesasDinheiro.setAttribute('stroke-dasharray', `${circulo1} 100`)
  }

  // Calculos

  methods._calcularResumo = (lancamentos) => {
    const resumo = {}
    resumo.dinheiro = 0
    resumo.debito = 0
    resumo.credito = 0
    resumo.boleto = 0

    lancamentos.map(lancamento => {
      const metodoPagamento = lancamento.parcelas.metodoPagamento
      const valor = lancamento.parcelas.valor

      resumo[metodoPagamento] += Number(Math.round(valor * 100) / 100)
    })

    return resumo
  }

  methods._calcularTotal = (lancamentos) => {
    const reducer = (acumulador, valor) => acumulador + valor
    return Object.values(lancamentos).reduce(reducer, 0)
  }

  methods._calcularPorcentagemGrafico = (valor, total) => {
    if (!valor) return 0

    const porcentagem = (Number(valor) * 100) / Number(total)
    return methods._arredondar(porcentagem)
  }

  methods._arredondar = (numero) => {
    return Number(Math.round(numero * 100) / 100)
  }

  return methods
}

module.exports = Module
