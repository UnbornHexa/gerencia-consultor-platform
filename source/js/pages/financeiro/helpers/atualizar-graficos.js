/* Requires */

const REQUEST_DESPESAS = require('../../../global/requests/despesas')
const REQUEST_RECEITAS = require('../../../global/requests/receitas')
const SECTION_GRAFICOS = require('../sections/section-graficos')

/* Module */

const Module = () => {
  const methods = {}

  methods.atualizar = () => {
    const __dateSelect = document.querySelector('app-date-select')

    const ano = __dateSelect.exportarDataSelecionada().ano
    const mes = __dateSelect.exportarDataSelecionada().mes

    const promiseDespesas = REQUEST_DESPESAS().receberTodos(ano, mes)
    const promiseReceitas = REQUEST_RECEITAS().receberTodos(ano, mes)

    Promise.all([promiseReceitas, promiseDespesas])
      .then((promises) => {
        const receitas = promises[0]
        const despesas = promises[1]

        SECTION_GRAFICOS().construirGraficos(receitas, despesas)
      })
  }

  return methods
}

module.exports = Module
