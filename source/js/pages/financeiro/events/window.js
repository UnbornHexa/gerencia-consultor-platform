/* Requires */

const HELPER_ATUALIZAR_GRAFICOS = require('../helpers/atualizar-graficos')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarGraficosAoIniciar = () => {
    window.addEventListener('load', () => {
      HELPER_ATUALIZAR_GRAFICOS().atualizar()
    })
  }

  return methods
}

module.exports = Module
