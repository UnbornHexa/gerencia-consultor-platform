/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_DATE_SELECT = require('./componente-date-select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_DATE_SELECT().iniciar()
  }

  return methods
}

module.exports = Module
