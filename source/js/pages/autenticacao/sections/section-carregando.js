const Module = () => {
  const methods = {}

  // Internal Variables

  const __sectionCarregando = document.querySelector('section.carregando')
  const __sectionEntrar = document.querySelector('section.autenticacao .entrar')
  const __sectionRegistrar = document.querySelector('section.autenticacao .registrar')
  const __inputEmailRegistrar = __sectionRegistrar.querySelector('input#email')
  const __inputSenhaRegistrar = __sectionRegistrar.querySelector('input#senha')
  const __inputEmailEntrar = __sectionEntrar.querySelector('input#email')
  const __inputSenhaEntrar = __sectionEntrar.querySelector('input#senha')
  const __buttonEntrar = __sectionEntrar.querySelector('.botoes button.autenticar')

  // Methods

  methods.mostrarSection = () => {
    __sectionCarregando.style.display = 'flex'
    __sectionEntrar.classList.add('ativo')
    __sectionRegistrar.classList.remove('ativo')
  }

  methods.preencherDadosEAutenticar = () => {
    __inputEmailEntrar.value = __inputEmailRegistrar.value
    __inputSenhaEntrar.value = __inputSenhaRegistrar.value
    __buttonEntrar.click()
  }

  return methods
}

module.exports = Module
