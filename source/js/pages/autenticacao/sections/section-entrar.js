const Module = () => {
  const methods = {}

  // Internal Variables

  const __sectionEntrar = document.querySelector('section.autenticacao .entrar')
  const __sectionRegistrar = document.querySelector('section.autenticacao .registrar')
  const __inputEmail = __sectionEntrar.querySelector('input#email')
  const __inputSenha = __sectionEntrar.querySelector('input#senha')
  const __buttonEntrar = __sectionEntrar.querySelector('.botoes button.autenticar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Methods

  methods.mostrarSection = () => {
    __sectionEntrar.classList.add('ativo')
    __sectionRegistrar.classList.remove('ativo')
  }

  methods.bloquearBotao = () => {
    __buttonEntrar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEntrar.removeAttribute('disabled')
  }

  /* Form */

  methods.verificarCamposObrigatorios = () => {
    if (__inputEmail.value && __inputSenha.value) return true

    __componenteAlerta.alertar('Preencha todos os campos para se autenticar.')
    return false
  }

  methods.limparCampos = () => {
    __inputEmail.value = ''
    __inputSenha.value = ''
  }

  /* Data */

  methods.exportarDados = () => {
    const usuario = {}

    usuario.email = __inputEmail.value
    usuario.senha = __inputSenha.value

    return usuario
  }

  return methods
}

module.exports = Module
