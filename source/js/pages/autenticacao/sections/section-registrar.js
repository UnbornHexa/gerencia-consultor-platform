const Module = () => {
  const methods = {}

  // Internal Variables

  const __sectionRegistrar = document.querySelector('section.autenticacao .registrar')
  const __sectionEntrar = document.querySelector('section.autenticacao .entrar')

  const __inputNome = __sectionRegistrar.querySelector('input#nome')
  const __inputTelefone = __sectionRegistrar.querySelector('input#telefone')
  const __inputEmail = __sectionRegistrar.querySelector('input#email')
  const __inputSenha = __sectionRegistrar.querySelector('input#senha')
  const __inputConfirmarSenha = __sectionRegistrar.querySelector('input#confirmar_senha')

  const __buttonRegistrar = __sectionEntrar.querySelector('.botoes button.criar')

  const __componenteAlerta = document.querySelector('app-alerta')

  // Methods

  methods.mostrarSection = () => {
    __sectionRegistrar.classList.add('ativo')
    __sectionEntrar.classList.remove('ativo')
  }

  methods.bloquearBotao = () => {
    __buttonRegistrar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonRegistrar.removeAttribute('disabled')
  }

  /* Form */

  methods.verificarCamposObrigatorios = () => {
    if (__inputNome.value && __inputTelefone.value && __inputEmail.value &&
      __inputSenha.value && __inputConfirmarSenha.value) return true

    __componenteAlerta.alertar('Preencha todos os campos para criar sua conta.')
    return false
  }

  methods.verificarSenhaMinimo6Digitos = () => {
    if (__inputSenha.value.length >= 6) return true

    __componenteAlerta.alertar('A senha deve ter no mínimo 6 caractéres.')
    return false
  }

  methods.verificarSenhasIguais = () => {
    if (__inputSenha.value === __inputConfirmarSenha.value) return true

    __componenteAlerta.alertar('As senhas devem ser iguais.')
    return false
  }

  methods.limparCampos = () => {
    __inputNome.value = ''
    __inputTelefone.value = ''
    __inputEmail.value = ''
    __inputSenha.value = ''
    __inputConfirmarSenha.value = ''
  }

  /* Data */

  methods.exportarDados = () => {
    const usuario = {}
    usuario.contato = {}

    usuario.nome = __inputNome.value
    usuario.email = __inputEmail.value
    usuario.senha = __inputSenha.value
    usuario.contato.telefone1 = __inputTelefone.value

    return usuario
  }

  return methods
}

module.exports = Module
