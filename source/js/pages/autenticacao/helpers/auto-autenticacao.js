/* Requires */

const TOKEN = require('../../../global/helpers/token')
const LOGOUT = require('../../../global/helpers/logout')
const HELPER_REDIRECIONADOR = require('./redirecionador')

/* Module */

const Module = () => {
  const methods = {}

  methods.redirecionaUsuarioAutenticado = () => {
    if (!TOKEN().getToken()) return
    if (LOGOUT().tokenExpired()) return

    HELPER_REDIRECIONADOR().redirecionar()
  }

  return methods
}

module.exports = Module
