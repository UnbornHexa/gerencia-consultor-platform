/* Requires */

const HOSTS = require('../../../global/data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  methods.redirecionar = () => {
    window.location.assign(`${HOSTS().url.appPlataforma}/painel`)
  }

  return methods
}

module.exports = Module
