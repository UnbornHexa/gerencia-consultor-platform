/* Requires */

const REQUEST_AUTENTICACAO = require('../../../global/requests/autenticacao')
const MODAL_ESQUECEU_SENHA = require('../modals/modal-esqueceu-senha')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_esqueceu_senha')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonEnviar = __modal.querySelector('button#enviar')

  // Methods

  methods.habilitarCliqueBotaoEnviar = () => {
    __buttonEnviar.addEventListener('click', () => {
      const email = MODAL_ESQUECEU_SENHA().exportarEmail()

      const camposObrigatoriosOk = MODAL_ESQUECEU_SENHA().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      MODAL_ESQUECEU_SENHA().bloquearBotao()
      REQUEST_AUTENTICACAO().recuperarSenha(email)
        .then(() => {
          MODAL_ESQUECEU_SENHA().ocultar()
        })
        .finally(() => {
          MODAL_ESQUECEU_SENHA().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_ESQUECEU_SENHA().ocultar()
    })
  }

  return methods
}

module.exports = Module
