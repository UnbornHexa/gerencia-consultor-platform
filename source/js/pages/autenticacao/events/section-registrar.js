/* Requires */

const REQUEST_AUTENTICACAO = require('../../../global/requests/autenticacao')
const SECTION_ENTRAR = require('../sections/section-entrar')
const SECTION_REGISTRAR = require('../sections/section-registrar')
const SECTION_CARREGANDO = require('../sections/section-carregando')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.autenticacao .registrar')
  const __buttonEntrar = __section.querySelector('.botoes button.autenticar')
  const __buttonRegistrar = __section.querySelector('.botoes button.criar')

  // Methods

  methods.habilitarCliqueButtonRegistrar = () => {
    __buttonRegistrar.addEventListener('click', () => {
      const dados = SECTION_REGISTRAR().exportarDados()

      const camposObrigatoriosOk = SECTION_REGISTRAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const senhaSeguraOk = SECTION_REGISTRAR().verificarSenhaMinimo6Digitos()
      if (!senhaSeguraOk) return

      const senhasIguais = SECTION_REGISTRAR().verificarSenhasIguais()
      if (!senhasIguais) return

      SECTION_REGISTRAR().bloquearBotao()
      REQUEST_AUTENTICACAO().registrar(dados)
        .then(() => {
          SECTION_CARREGANDO().mostrarSection()
          setTimeout(() => {
            SECTION_CARREGANDO().preencherDadosEAutenticar()
            SECTION_REGISTRAR().limparCampos()
          }, 3000)
        })
        .finally(() => {
          SECTION_REGISTRAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueButtonEntrar = () => {
    __buttonEntrar.addEventListener('click', () => {
      SECTION_ENTRAR().mostrarSection()
    })
  }

  return methods
}

module.exports = Module
