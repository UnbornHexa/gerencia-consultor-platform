/* Requires */

const EVENTS = require('../../../global/events/events')
const SECTION_ENTRAR = require('./section-entrar')
const SECTION_REGISTRAR = require('./section-registrar')
const MODAL_ESQUECEU_SENHA = require('./modal-esqueceu-senha')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()

    SECTION_ENTRAR().habilitarCliqueButtonEntrar()
    SECTION_ENTRAR().habilitarCliqueButtonRegistrar()
    SECTION_ENTRAR().habilitarAutenticacaoComTeclaEnter()
    SECTION_ENTRAR().habilitarCliqueBotaoEsqueceuSenha()

    SECTION_REGISTRAR().habilitarCliqueButtonEntrar()
    SECTION_REGISTRAR().habilitarCliqueButtonRegistrar()

    MODAL_ESQUECEU_SENHA().habilitarCliqueBotaoEnviar()
    MODAL_ESQUECEU_SENHA().habilitarCliqueBotaoFechar()
  }

  return methods
}

module.exports = Module
