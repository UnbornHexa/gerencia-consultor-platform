/* Requires */

const REQUEST_AUTENTICACAO = require('../../../global/requests/autenticacao')
const TOKEN = require('../../../global/helpers/token')
const SECTION_ENTRAR = require('../sections/section-entrar')
const SECTION_REGISTRAR = require('../sections/section-registrar')
const HELPER_REDIRECIONADOR = require('../helpers/redirecionador')
const MODAL_ESQUECEU_SENHA = require('../modals/modal-esqueceu-senha')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.autenticacao .entrar')
  const __buttonEntrar = __section.querySelector('.botoes button.autenticar')
  const __buttonRegistrar = __section.querySelector('.botoes button.criar')
  const __inputSenha = __section.querySelector('input#senha')
  const __buttonEsqueceuSenha = __section.querySelector('button.esqueceu')

  // Methods

  methods.habilitarCliqueButtonEntrar = () => {
    __buttonEntrar.addEventListener('click', () => {
      const dados = SECTION_ENTRAR().exportarDados()

      const camposObrigatoriosOk = SECTION_ENTRAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      SECTION_ENTRAR().bloquearBotao()
      REQUEST_AUTENTICACAO().entrar(dados)
        .then((retorno) => {
          TOKEN().setToken(retorno)
          SECTION_ENTRAR().limparCampos()
          HELPER_REDIRECIONADOR().redirecionar()
        })
        .finally(() => {
          SECTION_ENTRAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoEsqueceuSenha = () => {
    __buttonEsqueceuSenha.addEventListener('click', () => {
      MODAL_ESQUECEU_SENHA().mostrar()
    })
  }

  methods.habilitarCliqueButtonRegistrar = () => {
    __buttonRegistrar.addEventListener('click', () => {
      SECTION_REGISTRAR().mostrarSection()
    })
  }

  methods.habilitarAutenticacaoComTeclaEnter = () => {
    __inputSenha.addEventListener('keypress', (evento) => {
      const keyCode = evento.keyCode || evento.which
      if (keyCode !== 13) return false

      __buttonEntrar.click()
    })
  }

  return methods
}

module.exports = Module
