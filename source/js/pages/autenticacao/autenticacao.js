/* Requires */

const HELPER_AUTENTICACAO = require('./helpers/auto-autenticacao')
const EVENTOS = require('./events/events')
const INICIADORES = require('./inits/init')

/* Start */

HELPER_AUTENTICACAO().redirecionaUsuarioAutenticado()
INICIADORES().iniciarTodosOsComponentes()
EVENTOS().habilitarTodosOsEventos()
