/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_esqueceu_senha')
  const __inputEmail = __modal.querySelector('input#email')
  const __buttonEnviar = __modal.querySelector('button#enviar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputEmail.value) return true

    __componenteAlerta.alertar('Preencha o email de sua conta')
    return false
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonEnviar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEnviar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputEmail.value = ''
  }

  // Data

  methods.exportarEmail = () => {
    return __inputEmail.value
  }

  return methods
}

module.exports = Module
