/* Requires */

const LOGOUT_HELPER = require('../../../global/helpers/logout')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __buttonSair = document.querySelector('section.header button')

  // Methods

  methods.habilitarCliqueBotaoSair = () => {
    __buttonSair.addEventListener('click', () => {
      LOGOUT_HELPER().logout()
    })
  }

  return methods
}

module.exports = Module
