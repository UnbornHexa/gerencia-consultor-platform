/* Requires */

const HELPER_LOGOUT = require('../../global/helpers/logout')
const EVENTOS = require('./events/events')

/* Start */

HELPER_LOGOUT().checkTokenOk()
EVENTOS().habilitarTodosOsEventos()
