/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __telaInformacoes = __modal.querySelector('.telas .tela[data-id="1"]')
  const __telaProdutos = __modal.querySelector('.telas .tela[data-id="2"]')
  const __telaPagamento = __modal.querySelector('.telas .tela[data-id="3"]')
  const __selectCliente = __modal.querySelector('app-select')
  const __datepickerData = __modal.querySelector('app-date-picker#data')
  const __itensVenda = __modal.querySelector('app-itens-venda')
  const __pagamento = __modal.querySelector('app-pagamento')
  const __checkbox = __modal.querySelector('app-checkbox')
  const __divEncomenda = __modal.querySelector('.encomenda')
  const __datepickerDataEntrega = __modal.querySelector('app-date-picker#dataEntrega')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.mostrarTelaInformacoes = () => {
    __telaInformacoes.classList.add('mostrar-flex')
    __telaInformacoes.classList.remove('ocultar')
    __telaProdutos.classList.add('ocultar')
    __telaProdutos.classList.remove('mostrar-flex')
    __telaPagamento.classList.add('ocultar')
    __telaPagamento.classList.add('mostrar-flex')
  }

  methods.mostrarTelaProdutos = () => {
    __telaProdutos.classList.add('mostrar-flex')
    __telaProdutos.classList.remove('ocultar')
    __telaInformacoes.classList.add('ocultar')
    __telaInformacoes.classList.remove('mostrar-flex')
    __telaPagamento.classList.add('ocultar')
    __telaPagamento.classList.add('mostrar-flex')
  }

  methods.mostrarTelaPagamento = () => {
    __telaPagamento.classList.add('mostrar-flex')
    __telaPagamento.classList.remove('ocultar')
    __telaProdutos.classList.add('ocultar')
    __telaProdutos.classList.remove('mostrar-flex')
    __telaInformacoes.classList.add('ocultar')
    __telaInformacoes.classList.remove('mostrar-flex')
  }

  methods.mostrarEncomenda = () => {
    __divEncomenda.style.display = 'block'
  }

  methods.ocultarEncomenda = () => {
    __divEncomenda.style.display = 'none'
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonAdicionar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonAdicionar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __selectCliente.limparSelecionado()
    __datepickerData.resetar()
    __itensVenda.limpar()
    __pagamento.limpar()
    __checkbox.limpar()
    __datepickerDataEntrega.limpar()

    methods.mostrarTelaInformacoes()
    methods.ocultarEncomenda()
  }

  methods.verificarCamposObrigatorios = () => {
    if (!__selectCliente.vazio() && !__datepickerData.vazio() &&
      !__pagamento.vazio() && !__itensVenda.vazio()) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarProdutosAdicionados = () => {
    if (!__itensVenda.vazio()) return true

    __componenteAlerta.alertar('Adicione um produto primeiro antes de ir para a próxima tela.')
    return false
  }

  methods.verificarValorRestante = () => {
    if (!__pagamento.verificarSeExisteValorRestante()) return true

    __componenteAlerta.alertar('Preencha as parcelas de forma que não sobre nem falte dinheiro.')
    return false
  }

  methods.verificarDataEncomenda = () => {
    if (!__checkbox.verificar()) return true
    if (__checkbox.verificar() && !__datepickerDataEntrega.vazio()) return true

    __componenteAlerta.alertar('Preencha a data de entrega')
    return false
  }

  // Data

  methods.exportarDados = () => {
    const venda = {}
    const encomenda = {}
    const pagamento = {}
    venda.produtos = {}

    venda.data = DATE().convertBRToTimestamp(__datepickerData.exportarData())
    venda.idCliente = __selectCliente.exportarValorSelecionado()
    venda.produtos = __itensVenda.exportarProdutos()

    pagamento.parcelas = __pagamento.exportarParcelas()
    pagamento.valorTotal = __pagamento.exportarValorTotal()

    if (__checkbox.verificar()) encomenda.data = DATE().convertBRToTimestamp(__datepickerDataEntrega.exportarData())

    return { venda, encomenda, pagamento }
  }

  methods.preencherValorTotal = () => {
    const produtos = __itensVenda.exportarProdutos()
    const reducer = (acumulador, produto) => acumulador + produto.valorSubtotal
    const valorTotal = produtos.reduce(reducer, 0)

    __pagamento.importarValorTotal(valorTotal)
  }

  return methods
}

module.exports = Module
