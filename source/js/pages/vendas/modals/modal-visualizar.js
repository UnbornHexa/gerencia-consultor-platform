/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const DATE = require('../../../global/helpers/date')
const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar')
  const __buttonWhatsapp = __modal.querySelector('.opcoes button#whatsapp')
  const __telaInformacoes = __modal.querySelector('.telas .tela[data-id="1"]')
  const __telaProdutos = __modal.querySelector('.telas .tela[data-id="2"]')
  const __telaPagamento = __modal.querySelector('.telas .tela[data-id="3"]')
  const __inputId = __modal.querySelector('input#id')
  const __selectCliente = __modal.querySelector('app-select')
  const __datepickerData = __modal.querySelector('app-date-picker#data')
  const __itensVenda = __modal.querySelector('app-itens-venda')
  const __pagamento = __modal.querySelector('app-pagamento')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.mostrarTelaInformacoes = () => {
    __telaInformacoes.classList.add('mostrar-flex')
    __telaInformacoes.classList.remove('ocultar')
    __telaProdutos.classList.add('ocultar')
    __telaProdutos.classList.remove('mostrar-flex')
    __telaPagamento.classList.add('ocultar')
    __telaPagamento.classList.add('mostrar-flex')
  }

  methods.mostrarTelaProdutos = () => {
    __telaProdutos.classList.add('mostrar-flex')
    __telaProdutos.classList.remove('ocultar')
    __telaInformacoes.classList.add('ocultar')
    __telaInformacoes.classList.remove('mostrar-flex')
    __telaPagamento.classList.add('ocultar')
    __telaPagamento.classList.add('mostrar-flex')
  }

  methods.mostrarTelaPagamento = () => {
    __telaPagamento.classList.add('mostrar-flex')
    __telaPagamento.classList.remove('ocultar')
    __telaProdutos.classList.add('ocultar')
    __telaProdutos.classList.remove('mostrar-flex')
    __telaInformacoes.classList.add('ocultar')
    __telaInformacoes.classList.remove('mostrar-flex')
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonWhatsapp.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonWhatsapp.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __selectCliente.limparSelecionado()
    __selectCliente.desativar()
    __datepickerData.limpar()
    __datepickerData.desativar()
    __itensVenda.limpar()
    __itensVenda.desativar()
    __pagamento.limpar()
    __pagamento.desativar()

    methods.mostrarTelaInformacoes()
  }

  methods.verificarCamposObrigatorios = () => {
    if (!__selectCliente.vazio() && !__datepickerData.vazio() &&
      !__pagamento.vazio() && !__itensVenda.vazio()) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarValorRestante = () => {
    if (!__pagamento.verificarSeExisteValorRestante()) return true

    __componenteAlerta.alertar('Preencha as parcelas de forma que não sobre nem falte dinheiro.')
    return false
  }

  // Data

  methods.importarDados = (dados) => {
    const dataVenda = DATE().convertMongoDateToBR(dados.data)
    const reducer = (acumulador, parcela) => acumulador + parcela.valor
    const valorTotal = dados.idReceita.parcelas.reduce(reducer, 0) || 0

    __inputId.value = dados._id
    __selectCliente.selecionarOpcao(dados.idCliente._id)
    __datepickerData.selecionarData(dataVenda)
    __itensVenda.importarProdutos(dados.produtos)
    __pagamento.importarValorTotal(valorTotal)
    __pagamento.importarParcelas(dados.idReceita.parcelas)
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  methods.construirMensagemWhatsapp = (venda) => {
    const data = DATE().convertMongoDateToBR(venda.data)
    const nomeCliente = venda.idCliente.nome
    const telefoneCliente = venda.idCliente.contato.telefone1

    const listaProdutos = venda.produtos.map(produto => {
      const nome = produto.idProduto.nome
      const unitario = FORMAT().money(produto.valorUnitario)
      const subtotal = FORMAT().money(produto.valorSubtotal)
      const quantidade = produto.quantidade
      return `${quantidade}x ${nome} (${unitario}) - Total: ${subtotal}`
    }).join('\n\n')

    const reducer = (acumulador, produto) => { return acumulador + produto.valorSubtotal }
    const valorTotal = FORMAT().money(venda.produtos.reduce(reducer, 0))

    let texto = `Olá, ${nomeCliente}!\n`
    texto += `Segue a lista de produtos que você comprou em ${data}\n`
    texto += '---'
    texto += '\n\n'
    texto += `${listaProdutos}`
    texto += '\n\n'
    texto += '---\n'
    texto += `Total: ${valorTotal}`
    const textoFormatado = window.encodeURIComponent(texto)
    const pagina = 'https://api.whatsapp.com/send'
    const url = `${pagina}?phone=55${telefoneCliente}&text=${textoFormatado}`

    return url
  }

  return methods
}

module.exports = Module
