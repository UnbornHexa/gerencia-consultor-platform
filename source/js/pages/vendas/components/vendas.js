/* Requires */

const DATE = require('../../../global/helpers/date')
const FORMAT = require('../../../global/helpers/format')
const REQUEST_VENDAS = require('../../../global/requests/vendas')
const MODAL_VISUALIZAR = require('../modals/modal-visualizar')

/* Module */

class Vendas extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      vendas: [],
      data: {
        ano: 0,
        mes: 0,
        quantidadeDiasNoMes: 0
      },
      diasDaSemana: [
        'Domingo',
        'Segunda',
        'Terça',
        'Quarta',
        'Quinta',
        'Sexta',
        'Sábado'
      ]
    }

    const dataHoje = this._receberDataHoje()
    this.definirData(dataHoje.ano, dataHoje.mes)
    this._renderizar()
    this.habilitarDragAndDrop()
    this.habilitarCliqueVenda()
  }

  // Inicializacao

  definirData (ano, mes) {
    if (!ano && !mes) return

    this.__state.data.ano = Number(ano)
    this.__state.data.mes = Number(mes)
    this.__state.data.quantidadeDiasNoMes = new Date(ano, mes, 0).getDate()
  }

  // Renderizacao

  _renderizar () {
    const quantidadeDiasDoMes = this.__state.data.quantidadeDiasNoMes
    let html = ''
    let diaHoje = 1

    for (const indice of [...Array(quantidadeDiasDoMes).keys()]) {
      const diaNumero = indice + 1
      const diaNome = this._receberNomeDiaDaSemana(diaNumero)

      const classeAtual = (this._verificarDataHoje(diaNumero)) ? 'atual' : ''

      html += `
      <div class="dia ${classeAtual}" data-dia="${diaNumero}">
        <div class="texto">
          <p class="numero">${diaNumero}</p>
          <p class="texto">${diaNome}</p>
        </div>
        <div class="vendas"></div>
      </div>
      `

      if (this._verificarDataHoje(diaNumero)) diaHoje = diaNumero
    }

    this.innerHTML = html
    this.rolarPara(diaHoje)
  }

  limpar () {
    this.innerHTML = ''
  }

  rolarPara (dia) {
    const posicaoX = this.offsetLeft
    const coordenadaX = this.querySelector(`.dia[data-dia="${dia}"]`).offsetLeft - posicaoX
    this.scrollTo({ top: 0, left: coordenadaX, behavior: 'smooth' })
  }

  // Vendas

  importarVendas (vendas) {
    if (!vendas) return

    this.limpar()
    this._renderizar()

    for (const venda of vendas) {
      const dataBR = DATE().convertMongoDateToBR(venda.data)
      const dia = Number(dataBR.split('/')[0])
      this._adicionarVenda(dia, venda)
    }
  }

  _adicionarVenda (dia, venda) {
    const divDia = this.querySelector(`.dia[data-dia="${Number(dia)}"] .vendas`)
    const nomeCliente = venda.cliente[0].nome || ''
    const totalVenda = FORMAT().money(venda.valorTotal) || 'R$ 0,00'

    divDia.innerHTML += `
    <div class="venda">
      <input type="hidden" value="${venda._id}">
      <p class="titulo">Venda #${venda.numero} <span class="total">${totalVenda}</span></p>
      <p class="cliente">Cliente - ${nomeCliente}</p>
    </div>
    `
  }

  // Calculos de Data

  _receberDataHoje () {
    const hoje = new Date()
    const dataObj = DATE().dateToObj(hoje)

    return {
      ano: dataObj.year,
      mes: dataObj.month,
      dia: dataObj.day
    }
  }

  _verificarDataHoje (dia) {
    const ano = Number(this.__state.data.ano)
    const mes = Number(this.__state.data.mes)
    const dataObjHoje = this._receberDataHoje()

    if (Number(dataObjHoje.ano) !== ano) return false
    if (Number(dataObjHoje.mes) !== mes) return false
    if (Number(dataObjHoje.dia) !== Number(dia)) return false
    return true
  }

  _receberNomeDiaDaSemana (dia) {
    const ano = this.__state.data.ano
    const mes = this.__state.data.mes
    const indiceDiaDaSemana = new Date(ano, mes - 1, dia).getDay()

    return this.__state.diasDaSemana[indiceDiaDaSemana]
  }

  // Eventos

  habilitarDragAndDrop () {
    let ativado = false
    let coordenadaXInicial
    let rolagemParaEsquerda

    this.addEventListener('mousedown', (evento) => {
      ativado = true

      coordenadaXInicial = evento.pageX - this.offsetLeft
      rolagemParaEsquerda = this.scrollLeft
    })

    this.addEventListener('mouseleave', () => {
      ativado = false
      this.classList.remove('drag')
    })

    this.addEventListener('mouseup', () => {
      ativado = false
      this.classList.remove('drag')
    })

    this.addEventListener('mousemove', (evento) => {
      if (!ativado) return
      evento.preventDefault()

      const coordenadaX = evento.pageX - this.offsetLeft
      const distancia = (coordenadaX - coordenadaXInicial) * 2
      this.scrollLeft = rolagemParaEsquerda - distancia
      this.classList.add('drag')
    })
  }

  habilitarCliqueVenda () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('.venda')) return

      MODAL_VISUALIZAR().mostrar()

      const idItem = evento.target.querySelector('input[type="hidden"]').value
      REQUEST_VENDAS().receberPorId(idItem)
        .then((retorno) => {
          MODAL_VISUALIZAR().importarDados(retorno)
        })
    })
  }
}

module.exports = Vendas
