/* Requires */

const REQUEST_VENDAS = require('../../../global/requests/vendas')
const MODAL_VISUALIZAR = require('../modals/modal-visualizar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar')
  const __buttonAvancar = __modal.querySelector('button#avancar')
  const __buttonAvancar2 = __modal.querySelector('button#avancar_2')
  const __buttonVoltar = __modal.querySelector('button#voltar')
  const __buttonVoltar2 = __modal.querySelector('button#voltar_2')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonOpcoesWhatsapp = __modal.querySelector('.opcoes button#whatsapp')

  // Methods

  methods.habilitarCliqueBotaoOpcoesWhatsapp = () => {
    __buttonOpcoesWhatsapp.addEventListener('click', () => {
      const id = MODAL_VISUALIZAR().exportarId()

      MODAL_VISUALIZAR().bloquearBotao()
      REQUEST_VENDAS().receberPorId(id)
        .then((venda) => {
          const link = MODAL_VISUALIZAR().construirMensagemWhatsapp(venda)
          window.open(link, '_blank')
        })
        .finally(() => {
          MODAL_VISUALIZAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoAvancar = () => {
    __buttonAvancar.addEventListener('click', () => {
      MODAL_VISUALIZAR().mostrarTelaProdutos()
    })
  }

  methods.habilitarCliqueBotaoAvancar2 = () => {
    __buttonAvancar2.addEventListener('click', () => {
      MODAL_VISUALIZAR().mostrarTelaPagamento()
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_VISUALIZAR().mostrarTelaInformacoes()
    })
  }

  methods.habilitarCliqueBotaoVoltar2 = () => {
    __buttonVoltar2.addEventListener('click', () => {
      MODAL_VISUALIZAR().mostrarTelaProdutos()
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_VISUALIZAR().ocultar()
    })
  }

  return methods
}

module.exports = Module
