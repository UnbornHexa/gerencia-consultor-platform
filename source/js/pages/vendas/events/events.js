/* Requires */

const EVENTS = require('../../../global/events/events')
const CHAT = require('../../../global/events/chat')
const TOGGLE = require('../../../structures/navbar/helper-toggle')
const MAIN = require('./main')
const MODAL_ADICIONAR = require('./modal-adicionar')
const MODAL_VISUALIZAR = require('./modal-visualizar')
const WINDOW = require('./window')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    TOGGLE().habilitarCliqueToggle()
    MAIN().habilitarCliqueBotaoAdicionar()

    WINDOW().povoarVendasAoIniciar()
    WINDOW().povoarSelectAoIniciar()
    WINDOW().povoarItensVendaAoIniciar()

    MODAL_ADICIONAR().habilitarCliqueBotaoAdicionar()
    MODAL_ADICIONAR().habilitarCliqueBotaoAvancar()
    MODAL_ADICIONAR().habilitarCliqueBotaoAvancar2()
    MODAL_ADICIONAR().habilitarCliqueBotaoVoltar()
    MODAL_ADICIONAR().habilitarCliqueBotaoVoltar2()
    MODAL_ADICIONAR().habilitarCliqueBotaoFechar()
    MODAL_ADICIONAR().habilitarCliqueCheckbox()

    MODAL_VISUALIZAR().habilitarCliqueBotaoOpcoesWhatsapp()
    MODAL_VISUALIZAR().habilitarCliqueBotaoAvancar()
    MODAL_VISUALIZAR().habilitarCliqueBotaoAvancar2()
    MODAL_VISUALIZAR().habilitarCliqueBotaoVoltar()
    MODAL_VISUALIZAR().habilitarCliqueBotaoVoltar2()
    MODAL_VISUALIZAR().habilitarCliqueBotaoFechar()

    CHAT().habilitarChat()
  }

  return methods
}

module.exports = Module
