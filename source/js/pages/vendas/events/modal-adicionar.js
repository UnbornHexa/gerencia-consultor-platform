/* Requires */

const REQUEST_VENDAS = require('../../../global/requests/vendas')
const INIT_VENDAS = require('../inits/componente-vendas')
const INIT_ITENS_VENDA = require('../inits/componente-itens-venda')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __buttonAvancar = __modal.querySelector('button#avancar')
  const __buttonAvancar2 = __modal.querySelector('button#avancar_2')
  const __buttonVoltar = __modal.querySelector('button#voltar')
  const __buttonVoltar2 = __modal.querySelector('button#voltar_2')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __checkbox = __modal.querySelector('app-checkbox')

  // Methods

  methods.habilitarCliqueBotaoAdicionar = () => {
    __buttonAdicionar.addEventListener('click', () => {
      const dados = MODAL_ADICIONAR().exportarDados()

      const camposObrigatoriosOk = MODAL_ADICIONAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const valorRestanteOk = MODAL_ADICIONAR().verificarValorRestante()
      if (!valorRestanteOk) return

      const dataEncomendaOk = MODAL_ADICIONAR().verificarDataEncomenda()
      if (!dataEncomendaOk) return

      MODAL_ADICIONAR().bloquearBotao()
      REQUEST_VENDAS().adicionar(dados)
        .then(() => {
          MODAL_ADICIONAR().ocultar()
          INIT_VENDAS().atualizar()
          INIT_ITENS_VENDA().atualizar()
        })
        .finally(() => {
          MODAL_ADICIONAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoAvancar = () => {
    __buttonAvancar.addEventListener('click', () => {
      MODAL_ADICIONAR().mostrarTelaProdutos()
    })
  }

  methods.habilitarCliqueBotaoAvancar2 = () => {
    __buttonAvancar2.addEventListener('click', () => {
      const produtosAdicionadosOk = MODAL_ADICIONAR().verificarProdutosAdicionados()
      if (!produtosAdicionadosOk) return

      MODAL_ADICIONAR().mostrarTelaPagamento()
      MODAL_ADICIONAR().preencherValorTotal()
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_ADICIONAR().mostrarTelaInformacoes()
    })
  }

  methods.habilitarCliqueBotaoVoltar2 = () => {
    __buttonVoltar2.addEventListener('click', () => {
      MODAL_ADICIONAR().mostrarTelaProdutos()
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_ADICIONAR().ocultar()
    })
  }

  methods.habilitarCliqueCheckbox = () => {
    __checkbox.addEventListener('click', () => {
      (__checkbox.verificar())
        ? MODAL_ADICIONAR().mostrarEncomenda()
        : MODAL_ADICIONAR().ocultarEncomenda()
    })
  }

  return methods
}

module.exports = Module
