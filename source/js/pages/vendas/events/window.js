/* Requires */

const INIT_VENDAS = require('../inits/componente-vendas')
const INIT_SELECT = require('../inits/componente-select')
const INIT_ITENS_VENDA = require('../inits/componente-itens-venda')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarVendasAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_VENDAS().atualizar()
    })
  }

  methods.povoarSelectAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_SELECT().atualizarNomesClientes()
    })
  }

  methods.povoarItensVendaAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_ITENS_VENDA().atualizar()
    })
  }

  return methods
}

module.exports = Module
