/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_DATE_SELECT = require('./componente-date-select')
const COMPONENTE_SELECT = require('./componente-select')
const COMPONENTE_VENDAS = require('./componente-vendas')
const COMPONENTE_ITENS_VENDA = require('./componente-itens-venda')
const COMPONENTE_PAGAMENTO = require('./componente-pagamento')
const COMPONENTE_CHECKBOX = require('./componente-checkbox')
const COMPONENTE_DATE_PICKER = require('./componente-date-picker')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_DATE_SELECT().iniciar()
    COMPONENTE_VENDAS().iniciar()
    COMPONENTE_SELECT().iniciar()
    COMPONENTE_ITENS_VENDA().iniciar()
    COMPONENTE_PAGAMENTO().iniciar()
    COMPONENTE_CHECKBOX().iniciar()
    COMPONENTE_DATE_PICKER().iniciar()
  }

  return methods
}

module.exports = Module
