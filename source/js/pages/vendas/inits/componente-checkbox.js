/* Requires */

const COMPONENT_CHECKBOX = require('../../../global/components/checkbox')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-checkbox', COMPONENT_CHECKBOX)
  }

  return methods
}

module.exports = Module
