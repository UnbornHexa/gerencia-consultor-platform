/* Requires */

const REQUEST_VENDAS = require('../../../global/requests/vendas')
const COMPONENT_VENDAS = require('../components/vendas')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-vendas', COMPONENT_VENDAS)
  }

  methods.atualizar = () => {
    const __componenteVendas = document.querySelector('app-vendas')
    const __dateSelect = document.querySelector('app-date-select')

    const ano = __dateSelect.exportarDataSelecionada().ano
    const mes = __dateSelect.exportarDataSelecionada().mes

    __componenteVendas.definirData(ano, mes)

    REQUEST_VENDAS().receberTodos(ano, mes)
      .then(vendas => {
        __componenteVendas.importarVendas(vendas)
      })
  }

  return methods
}

module.exports = Module
