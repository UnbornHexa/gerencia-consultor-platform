/* Requires */

const REQUEST_CLIENTES = require('../../../global/requests/clientes')
const COMPONENT_SELECT = require('../../../global/components/select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-select', COMPONENT_SELECT)
  }

  methods.atualizarNomesClientes = () => {
    const __componenteSelectModalAdicionar = document.querySelector('section#modal_adicionar app-select')
    const __componenteSelectModalVisualizar = document.querySelector('section#modal_visualizar app-select')

    REQUEST_CLIENTES().receberNomes()
      .then(clientes => {
        const opcoes = clientes.map(cliente => {
          return {
            texto: cliente.nome,
            valor: cliente._id
          }
        })
        __componenteSelectModalAdicionar.importarOpcoes(opcoes)
        __componenteSelectModalVisualizar.importarOpcoes(opcoes)
      })
  }

  return methods
}

module.exports = Module
