/* Requires */

const COMPONENT_PAGAMENTO = require('../../../global/components/pagamento')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-pagamento', COMPONENT_PAGAMENTO)
  }

  return methods
}

module.exports = Module
