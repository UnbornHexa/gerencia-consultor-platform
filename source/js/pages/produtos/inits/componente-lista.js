/* Requires */

const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const COMPONENT_LISTA = require('../components/lista')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-lista', COMPONENT_LISTA)
  }

  methods.atualizar = () => {
    const __componenteLista = document.querySelector('app-lista')

    REQUEST_PRODUTOS().receberTodos()
      .then(produtos => {
        __componenteLista.importarItens(produtos)
      })
  }

  return methods
}

module.exports = Module
