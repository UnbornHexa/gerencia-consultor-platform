/* Requires */

const REQUEST_PRODUTOS_FORNECEDORES = require('../../../global/requests/produtos-fornecedores')
const COMPONENT_SELECT_PRODUTOS_FORNECEDORES = require('../components/select-produtos-fornecedores')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-select-produtos-fornecedores', COMPONENT_SELECT_PRODUTOS_FORNECEDORES)
  }

  methods.atualizarProdutosFornecedores = () => {
    const __componenteSelectProdutosFornecedores = document.querySelector('section#modal_adicionar app-select-produtos-fornecedores')

    REQUEST_PRODUTOS_FORNECEDORES().receberTodosHinode()
      .then(produtos => {
        const opcoes = produtos.map(produto => {
          return {
            texto: produto.nome,
            valor: produto._id,
            conteudo: produto
          }
        })
        __componenteSelectProdutosFornecedores.importarOpcoes(opcoes)
      })
  }

  return methods
}

module.exports = Module
