/* Requires */

const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const COMPONENT_SELECT = require('../../../global/components/select')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-select', COMPONENT_SELECT)
  }

  methods.atualizarNomesProdutos = () => {
    const __componenteSelect = document.querySelector('section#modal_repor app-select')

    REQUEST_PRODUTOS().receberNomes()
      .then(produtos => {
        const opcoes = produtos.map(produto => {
          return {
            texto: produto.nome,
            valor: produto._id
          }
        })
        __componenteSelect.importarOpcoes(opcoes)
      })
  }

  return methods
}

module.exports = Module
