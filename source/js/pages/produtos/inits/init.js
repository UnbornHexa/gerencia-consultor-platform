/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_LISTA = require('./componente-lista')
const COMPONENTE_SELECT = require('./componente-select')
const COMPONENTE_SELECT_PRODUTOS_FORNECEDORES = require('./componente-select-produtos-fornecedores')
const COMPONENTE_PAGAMENTO = require('./componente-pagamento')
const COMPONENTE_DATE_PICKER = require('./componente-date-picker')
const COMPONENTE_CHECKBOX = require('./componente-checkbox')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_LISTA().iniciar()
    COMPONENTE_SELECT().iniciar()
    COMPONENTE_SELECT_PRODUTOS_FORNECEDORES().iniciar()
    COMPONENTE_DATE_PICKER().iniciar()
    COMPONENTE_PAGAMENTO().iniciar()
    COMPONENTE_CHECKBOX().iniciar()
  }

  return methods
}

module.exports = Module
