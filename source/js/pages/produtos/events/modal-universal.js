/* Requires */

const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_UNIVERSAL = require('../modals/modal-universal')
const MODAL_DELETAR = require('../modals/modal-deletar')
const MODAL_REPOR = require('../modals/modal-repor')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonOpcoesDeletar = __modal.querySelector('.opcoes button#deletar')
  const __buttonOpcoesRepor = __modal.querySelector('.opcoes button#repor')
  const __inputPrecoCusto = __modal.querySelector('input#preco_custo')
  const __inputPrecoVenda = __modal.querySelector('input#preco_venda')
  const __checkbox = __modal.querySelector('app-checkbox')

  // Methods

  methods.habilitarCliqueBotaoEditar = () => {
    __buttonEditar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()

      const camposObrigatoriosOk = MODAL_UNIVERSAL().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const estoqueGerenciadoOk = MODAL_UNIVERSAL().verificarEstoqueGerenciado()
      if (!estoqueGerenciadoOk) return

      MODAL_UNIVERSAL().bloquearBotao()
      REQUEST_PRODUTOS().editar(dados, id)
        .then(() => {
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_UNIVERSAL().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_UNIVERSAL().ocultar()
    })
  }

  // Opcoes

  methods.habilitarCliqueBotaoOpcoesDeletar = () => {
    __buttonOpcoesDeletar.addEventListener('click', () => {
      const dados = MODAL_UNIVERSAL().exportarDados()
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { nome: dados.nome, id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_DELETAR().mostrar()
      MODAL_DELETAR().importarDados(obj)
    })
  }

  methods.habilitarCliqueBotaoOpcoesRepor = () => {
    __buttonOpcoesRepor.addEventListener('click', () => {
      const id = MODAL_UNIVERSAL().exportarId()
      const obj = { id: id }

      MODAL_UNIVERSAL().ocultarParcialmente()
      MODAL_REPOR().mostrar()
      MODAL_REPOR().importarDados(obj)
    })
  }

  // ---

  methods.habilitarCliqueSelecionandoInputPreco = () => {
    __inputPrecoCusto.addEventListener('click', () => {
      __inputPrecoCusto.select()
    })

    __inputPrecoVenda.addEventListener('click', () => {
      __inputPrecoVenda.select()
    })
  }

  methods.habilitarCliqueCheckbox = () => {
    __checkbox.addEventListener('click', () => {
      (__checkbox.verificar())
        ? MODAL_UNIVERSAL().mostrarEstoque()
        : MODAL_UNIVERSAL().ocultarEstoque()
    })
  }

  return methods
}

module.exports = Module
