/* Requires */

const INIT_LISTA = require('../inits/componente-lista')
const INIT_SELECT = require('../inits/componente-select')
const INIT_SELECT_PRODUTOS_FORNECEDORES = require('../inits/componente-select-produtos-fornecedores')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarListaAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_LISTA().atualizar()
    })
  }

  methods.povoarSelectAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_SELECT().atualizarNomesProdutos()
    })
  }

  methods.povoarSelectProdutosFornecedoresAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_SELECT_PRODUTOS_FORNECEDORES().atualizarProdutosFornecedores()
    })
  }

  return methods
}

module.exports = Module
