/* Requires */

const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __inputPrecoCusto = __modal.querySelector('input#preco_custo')
  const __inputPrecoVenda = __modal.querySelector('input#preco_venda')
  const __checkbox = __modal.querySelector('app-checkbox')

  // Methods

  methods.habilitarCliqueBotaoAdicionar = () => {
    __buttonAdicionar.addEventListener('click', () => {
      const dados = MODAL_ADICIONAR().exportarDados()

      const camposObrigatoriosOk = MODAL_ADICIONAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const estoqueGerenciadoOk = MODAL_ADICIONAR().verificarEstoqueGerenciado()
      if (!estoqueGerenciadoOk) return

      MODAL_ADICIONAR().bloquearBotao()
      REQUEST_PRODUTOS().adicionar(dados)
        .then(() => {
          MODAL_ADICIONAR().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_ADICIONAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_ADICIONAR().ocultar()
    })
  }

  methods.habilitarCliqueSelecionandoInputPreco = () => {
    __inputPrecoCusto.addEventListener('click', () => {
      __inputPrecoCusto.select()
    })

    __inputPrecoVenda.addEventListener('click', () => {
      __inputPrecoVenda.select()
    })
  }

  methods.habilitarCliqueCheckbox = () => {
    __checkbox.addEventListener('click', () => {
      (__checkbox.verificar())
        ? MODAL_ADICIONAR().mostrarEstoque()
        : MODAL_ADICIONAR().ocultarEstoque()
    })
  }

  return methods
}

module.exports = Module
