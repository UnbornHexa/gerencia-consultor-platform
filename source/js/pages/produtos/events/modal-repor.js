/* Requires */

const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_REPOR = require('../modals/modal-repor')
const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_repor')
  const __buttonRepor = __modal.querySelector('button#repor')
  const __buttonVoltar = __modal.querySelector('button#voltar')

  // Methods

  methods.habilitarCliqueBotaoRepor = () => {
    __buttonRepor.addEventListener('click', () => {
      const dados = MODAL_REPOR().exportarDados()
      const id = MODAL_REPOR().exportarId()

      const camposObrigatoriosOk = MODAL_REPOR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const valorRestanteOk = MODAL_REPOR().verificarValorRestante()
      if (!valorRestanteOk) return

      MODAL_REPOR().bloquearBotao()
      REQUEST_PRODUTOS().repor(dados, id)
        .then(() => {
          MODAL_REPOR().ocultar()
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
        .finally(() => {
          MODAL_REPOR().desbloquearBotao()
        })
    })
  }

  methods.habilitarCliqueBotaoVoltar = () => {
    __buttonVoltar.addEventListener('click', () => {
      MODAL_REPOR().ocultar()
      MODAL_UNIVERSAL().mostrarParcialmente()
    })
  }

  return methods
}

module.exports = Module
