/* Requires */

const COMPONENT_SELECT = require('../../../global/components/select')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

class SelectProdutosFornecedores extends COMPONENT_SELECT {
  constructor () {
    super()
    this.habilitarCliqueOpcao()
  }

  exportarConteudoSelecionado () {
    const idProdutoFornecedor = this.__state.selecionado.valor

    const produtoFiltrado = this.__state.opcoes.filter((produto) => {
      return (produto.valor === idProdutoFornecedor)
    })

    return produtoFiltrado[0].conteudo
  }

  habilitarCliqueOpcao () {
    this.addEventListener('click', (evento) => {
      const elemento = '.opcoes div'
      if (!evento.target.matches(elemento)) return

      const valor = evento.target.getAttribute('data-value')

      this.selecionarOpcao(valor)
      this._limparBusca()
      this._exibirPrimeirosResultados()
      this._ocultarDropdown()

      const produto = this.exportarConteudoSelecionado()
      MODAL_ADICIONAR().importarDados(produto)
    })
  }
}

module.exports = SelectProdutosFornecedores
