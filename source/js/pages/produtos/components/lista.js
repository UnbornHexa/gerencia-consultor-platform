/* Requires */

const COMPONENT_LISTA = require('../../../global/components/lista')
const REQUEST_PRODUTOS = require('../../../global/requests/produtos')
const FORMAT = require('../../../global/helpers/format')
const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

class Lista extends COMPONENT_LISTA {
  constructor () {
    super()
    this.habilitarCliqueItem()
  }

  _renderizarNovoItem (item) {
    const itensUl = this.querySelector('ul.itens')
    const produtoEstacado = item.estocado

    const li =
    `
    <li class="item">
      <div class="info">
        <input type="hidden" value="${item._id}">
        <div class="icone-tela produtos"></div>
        <div>
          <p class="principal">${item.nome}
            ${(produtoEstacado) ? `<span class="estoque">${item.quantidade} no estoque</span>` : ''}
          </p>
          <p class="secundario">${FORMAT().money(item.precoVenda)}</p>
        </div>
      </div>
      <div class="opcoes">
        <div class="icone"></div>
      </div>
    </li>
    `

    itensUl.innerHTML += li
  }

  habilitarCliqueItem () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('ul li')) return

      MODAL_UNIVERSAL().mostrar()

      const idItem = evento.target.querySelector('.info input').value
      REQUEST_PRODUTOS().receberPorId(idItem)
        .then((retorno) => {
          MODAL_UNIVERSAL().importarDados(retorno)
        })
    })
  }
}

module.exports = Lista
