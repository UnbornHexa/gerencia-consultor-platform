const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_repor')
  const __inputQuantidade = __modal.querySelector('input#quantidade')
  const __selectProduto = __modal.querySelector('app-select')
  const __pagamento = __modal.querySelector('app-pagamento')
  const __buttonRepor = __modal.querySelector('button#repor')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonRepor.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonRepor.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputQuantidade.value = ''
    __selectProduto.limparSelecionado()
    __pagamento.limpar()
  }

  methods.verificarCamposObrigatorios = () => {
    if (!__selectProduto.vazio() && __inputQuantidade.value && !__pagamento.vazio()) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarValorRestante = () => {
    if (!__pagamento.verificarSeExisteValorRestante()) return true

    __componenteAlerta.alertar('Preencha as parcelas de forma que não sobre nem falte dinheiro.')
    return false
  }

  // Data

  methods.importarDados = (dados) => {
    __selectProduto.selecionarOpcao(dados.id)
  }

  methods.exportarDados = () => {
    const produto = {}
    const despesa = {}

    produto.quantidade = __inputQuantidade.value || 0

    despesa.nome = `+${__inputQuantidade.value} ${__selectProduto.exportarTextoSelecionado()}` || ''
    despesa.valorTotal = __pagamento.exportarValorTotal()
    despesa.parcelas = __pagamento.exportarParcelas()

    return { produto, despesa }
  }

  methods.exportarId = () => {
    return __selectProduto.exportarValorSelecionado()
  }

  return methods
}

module.exports = Module
