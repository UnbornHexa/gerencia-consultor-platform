/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar')
  const __inputNome = __modal.querySelector('input#nome')
  const __inputCategoria = __modal.querySelector('input#categoria')
  const __inputQuantidade = __modal.querySelector('input#quantidade')
  const __inputQuantidadeMinima = __modal.querySelector('input#quantidade_minima')
  const __inputDuracao = __modal.querySelector('input#duracao')
  const __inputPrecoCusto = __modal.querySelector('input#preco_custo')
  const __inputPrecoVenda = __modal.querySelector('input#preco_venda')
  const __checkbox = __modal.querySelector('app-checkbox')
  const __selectProdutosFornecedores = __modal.querySelector('app-select-produtos-fornecedores')
  const __divEstoque = __modal.querySelector('.estoque_wrapper')
  const __buttonAdicionar = __modal.querySelector('button#adicionar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.mostrarEstoque = () => {
    __divEstoque.style.display = 'block'
  }

  methods.ocultarEstoque = () => {
    __divEstoque.style.display = 'none'
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonAdicionar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonAdicionar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputNome.value = ''
    __inputCategoria.value = ''
    __inputQuantidade.value = ''
    __inputQuantidadeMinima.value = ''
    __inputDuracao.value = ''
    __inputPrecoCusto.value = ''
    __inputPrecoVenda.value = ''
    __checkbox.limpar()
    __selectProdutosFornecedores.limparSelecionado()

    methods.ocultarEstoque()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputNome.value && __inputDuracao.value && __inputPrecoCusto.value && __inputPrecoVenda.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarEstoqueGerenciado = () => {
    if (!__checkbox.verificar()) return true
    if (__checkbox.verificar() && __inputQuantidade.value) return true

    __componenteAlerta.alertar('Preencha a quantidade do produto')
    return false
  }

  // Data

  methods.importarDados = (dados) => {
    methods.limparCampos()

    __inputNome.value = dados.nome || ''
    __inputCategoria.value = dados.categoria || ''
    __inputDuracao.value = dados.duracao || ''
    __inputPrecoCusto.value = FORMAT().money(dados.precoCusto) || ''
    __inputPrecoVenda.value = FORMAT().money(dados.precoVenda) || ''
  }

  methods.exportarDados = () => {
    const produto = {}

    // obrigatorio
    produto.nome = __inputNome.value
    produto.duracao = __inputDuracao.value
    produto.precoCusto = FORMAT().unformatMoney(__inputPrecoCusto.value)
    produto.precoVenda = FORMAT().unformatMoney(__inputPrecoVenda.value)

    // opcional
    if (__inputCategoria.value) produto.categoria = __inputCategoria.value

    if (!__checkbox.verificar()) {
      produto.estocado = false
      produto.quantidade = 0
      produto.quantidadeMinima = 0
    } else {
      produto.estocado = true
      produto.quantidade = __inputQuantidade.value
      if (__inputQuantidadeMinima.value) produto.quantidadeMinima = __inputQuantidadeMinima.value
    }

    return produto
  }

  return methods
}

module.exports = Module
