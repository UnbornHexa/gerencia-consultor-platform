/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')
const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __inputId = __modal.querySelector('input#id')
  const __inputNome = __modal.querySelector('input#nome')
  const __inputCategoria = __modal.querySelector('input#categoria')
  const __inputQuantidade = __modal.querySelector('input#quantidade')
  const __inputQuantidadeMinima = __modal.querySelector('input#quantidade_minima')
  const __inputDuracao = __modal.querySelector('input#duracao')
  const __inputPrecoCusto = __modal.querySelector('input#preco_custo')
  const __inputPrecoVenda = __modal.querySelector('input#preco_venda')
  const __checkbox = __modal.querySelector('app-checkbox')
  const __divEstoque = __modal.querySelector('.estoque_wrapper')
  const __buttonEditar = __modal.querySelector('button#editar')
  const __componenteAlerta = document.querySelector('app-alerta')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.mostrarParcialmente = () => {
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  methods.ocultarParcialmente = () => {
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
  }

  methods.mostrarEstoque = () => {
    __divEstoque.style.display = 'block'
  }

  methods.ocultarEstoque = () => {
    __divEstoque.style.display = 'none'
  }

  // Form

  methods.bloquearBotao = () => {
    __buttonEditar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonEditar.removeAttribute('disabled')
  }

  methods.limparCampos = () => {
    __inputId.value = ''
    __inputNome.value = ''
    __inputCategoria.value = ''
    __inputQuantidade.value = ''
    __inputQuantidadeMinima.value = ''
    __inputDuracao.value = ''
    __inputPrecoCusto.value = ''
    __inputPrecoVenda.value = ''
    __checkbox.limpar()

    methods.ocultarEstoque()
  }

  methods.verificarCamposObrigatorios = () => {
    if (__inputNome.value && __inputDuracao.value && __inputPrecoCusto.value && __inputPrecoVenda.value) return true

    __componenteAlerta.alertar('Preencha todos os campos obrigatórios')
    return false
  }

  methods.verificarEstoqueGerenciado = () => {
    if (!__checkbox.verificar()) return true
    if (__checkbox.verificar() && __inputQuantidade.value) return true

    __componenteAlerta.alertar('Preencha a quantidade do produto')
    return false
  }

  // Data

  methods.importarDados = (dados) => {
    __inputId.value = dados._id
    __inputNome.value = dados.nome || ''

    __inputDuracao.value = dados.duracao || ''
    __inputPrecoCusto.value = FORMAT().money(dados.precoCusto) || ''
    __inputPrecoVenda.value = FORMAT().money(dados.precoVenda) || ''

    if (dados.estocado) __checkbox.click()

    // opcional
    __inputCategoria.value = dados.categoria || ''
    __inputQuantidade.value = dados.quantidade || ''
    __inputQuantidadeMinima.value = dados.quantidadeMinima || ''
  }

  methods.exportarDados = () => {
    const produto = {}

    // obrigatorio
    produto.nome = __inputNome.value
    produto.duracao = __inputDuracao.value
    produto.precoCusto = FORMAT().unformatMoney(__inputPrecoCusto.value)
    produto.precoVenda = FORMAT().unformatMoney(__inputPrecoVenda.value)

    // opcional
    if (__inputCategoria.value) produto.categoria = __inputCategoria.value

    if (!__checkbox.verificar()) {
      produto.estocado = false
      produto.quantidade = 0
      produto.quantidadeMinima = 0
    } else {
      produto.estocado = true
      produto.quantidade = __inputQuantidade.value
      if (__inputQuantidadeMinima.value) produto.quantidadeMinima = __inputQuantidadeMinima.value
    }
    return produto
  }

  methods.exportarId = () => {
    return __inputId.value
  }

  return methods
}

module.exports = Module
