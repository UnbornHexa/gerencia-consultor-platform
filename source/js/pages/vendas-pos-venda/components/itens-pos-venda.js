/* Requires */

const REQUEST_POSVENDA = require('../../../global/requests/pos-venda')
const FORMAT = require('../../../global/helpers/format')
const INIT_LISTA = require('../inits/componente-lista')
const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

class ItensPosVenda extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      itens: []
    }

    this.habilitarCliqueStatus()
  }

  // Renderizacao

  _renderizarNovoItem (item) {
    const classeConfirmado = (item.abordado) ? 'confirmado' : ''
    const textoStatus = (item.abordado) ? 'Concluído' : 'Pendente'
    const valor = FORMAT().money(item.produto.valorSubtotal) || ''

    const dataSubtracao = new Date(item.data) - new Date()
    const numeroDiasSubtracao = Math.ceil(dataSubtracao / (1000 * 60 * 60 * 24))
    const diasFaltantes = (numeroDiasSubtracao > 0) ? numeroDiasSubtracao : 0

    const itemDiv = `
    <div class="item ${classeConfirmado}">
      <input id="id" type="hidden" value="${item._id}">
      <div class="informacao">
        <p class="titulo">${item.produto.quantidade}x ${item.produto.idProduto.nome}</p>
        <p class="valor">${valor} - Acaba em ${diasFaltantes} dias</p>
      </div>
      <div class="status">
        <i class="check icon-confirmar"></i>
        <p class="label">${textoStatus}</p>
      </div>
    </div>
    `
    this.innerHTML += itemDiv
  }

  // Utils

  limpar () {
    this.innerHTML = ''
    this.__state.itens = []
  }

  importarItens (itens) {
    this.limpar()
    this.__state.itens = itens

    if (itens.length === 0) return

    for (const item of itens) {
      this._renderizarNovoItem(item)
    }
  }

  // Eventos

  habilitarCliqueStatus () {
    this.addEventListener('click', (evento) => {
      const elemento = '.status i'
      if (!evento.target.matches(elemento)) return

      const id = evento.target.parentElement.parentElement.querySelector('input#id').value
      const status = evento.target.parentElement.querySelector('.label').innerText
      const valorStatus = (status === 'Pendente')

      const data = { abordado: valorStatus }

      REQUEST_POSVENDA().editar(data, id)
        .then(() => {
          MODAL_UNIVERSAL().ocultar()
          INIT_LISTA().atualizar()
        })
    })
  }
}

module.exports = ItensPosVenda
