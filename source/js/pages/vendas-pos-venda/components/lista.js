/* Requires */

const REQUEST_POSVENDA = require('../../../global/requests/pos-venda')
const COMPONENT_LISTA = require('../../../global/components/lista')
const ACCENT = require('../../../global/helpers/accent')
const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

class Lista extends COMPONENT_LISTA {
  constructor () {
    super()
    this.habilitarCliqueItem()
  }

  _renderizarNovoItem (item) {
    const itensUl = this.querySelector('ul.itens')
    const palavra = (item.numeroProdutos > 1) ? 'produtos' : 'produto'

    const li = `
    <li class="item">
      <div class="info">
        <input type="hidden" value="${item._id}">
        <div class="icone-tela posvenda"></div>
        <div>
          <p class="principal">Cliente <span class="cliente">${item.nomeCliente}</span></p>
          <p class="secundario"><span id="quantidade">${item.numeroProdutos}</span> ${palavra} acabando</p>
        </div>
      </div>
      <div class="opcoes">
        <div class="icone"></div>
      </div>
    </li>
    `
    itensUl.innerHTML += li
  }

  filtrar (texto) {
    this.__state.itensFiltrados = this.__state.itens.filter(item => {
      const termoPesquisado = ACCENT().remove(texto.toString().toUpperCase())
      const itemAVerificar = ACCENT().remove(item.nomeCliente.toUpperCase())
      return (itemAVerificar.includes(termoPesquisado))
    })

    this._exibirItensDaPagina(1)
  }

  habilitarCliqueItem () {
    this.addEventListener('click', (evento) => {
      if (!evento.target.matches('ul li')) return

      const __dateSelect = document.querySelector('app-date-select')
      const ano = __dateSelect.exportarDataSelecionada().ano
      const mes = __dateSelect.exportarDataSelecionada().mes

      MODAL_UNIVERSAL().mostrar()

      const idCliente = evento.target.querySelector('.info input').value
      REQUEST_POSVENDA().receberPorCliente(idCliente, ano, mes)
        .then((retorno) => {
          MODAL_UNIVERSAL().importarDados(retorno)
        })
    })
  }
}

module.exports = Lista
