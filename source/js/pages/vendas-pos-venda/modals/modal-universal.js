/* Requires */

const HELPER_MODAL_EFEITOS = require('../../../structures/modal/helper-efeitos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __inputIdCliente = __modal.querySelector('input#id')
  const __selectCliente = __modal.querySelector('app-select')
  const __itensPosVenda = __modal.querySelector('app-itens-pos-venda')
  const __buttonWhatsapp = __modal.querySelector('button#whatsapp')

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoAbrir()
    __modal.classList.add('mostrar-block')
    __modal.classList.remove('ocultar')
    methods.limparCampos()
  }

  methods.ocultar = () => {
    HELPER_MODAL_EFEITOS().aplicarEfeitoFechar()
    __modal.classList.remove('mostrar-block')
    __modal.classList.add('ocultar')
    methods.limparCampos()
  }

  // Form

  methods.limparCampos = () => {
    __inputIdCliente.value = ''
    __selectCliente.limparSelecionado()
    __selectCliente.desativar()
    __buttonWhatsapp.removeAttribute('data-numero')
    __itensPosVenda.limpar()
  }

  // Data

  methods.importarDados = (dados) => {
    if (dados.length === 0) return
    const idCliente = dados[0].idCliente._id
    const telefoneCliente = dados[0].idCliente.contato.telefone1.replace(/[^0-9]/g, '') || ''

    __inputIdCliente.value = idCliente
    __selectCliente.selecionarOpcao(idCliente)
    __itensPosVenda.importarItens(dados)
    __buttonWhatsapp.setAttribute('data-numero', telefoneCliente)
  }

  methods.exportarId = () => {
    return __inputIdCliente.value
  }

  return methods
}

module.exports = Module
