/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')
const COMPONENTE_LISTA = require('./componente-lista')
const COMPONENTE_DATE_SELECT = require('./componente-date-select')
const COMPONENTE_SELECT = require('./componente-select')
const COMPONENTE_ITENS_POS_VENDA = require('./componente-itens-pos-venda')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
    COMPONENTE_DATE_SELECT().iniciar()
    COMPONENTE_SELECT().iniciar()
    COMPONENTE_LISTA().iniciar()
    COMPONENTE_ITENS_POS_VENDA().iniciar()
  }

  return methods
}

module.exports = Module
