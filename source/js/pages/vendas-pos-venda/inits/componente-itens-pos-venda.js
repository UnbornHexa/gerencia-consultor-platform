/* Requires */

const COMPONENT_ITENS_POS_VENDA = require('../components/itens-pos-venda')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-itens-pos-venda', COMPONENT_ITENS_POS_VENDA)
  }

  return methods
}

module.exports = Module
