/* Requires */

const REQUEST_POSVENDA = require('../../../global/requests/pos-venda')
const COMPONENT_LISTA = require('../components/lista')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciar = () => {
    window.customElements.define('app-lista', COMPONENT_LISTA)
  }

  methods.atualizar = () => {
    const __componenteLista = document.querySelector('app-lista')
    const __dateSelect = document.querySelector('app-date-select')

    const ano = __dateSelect.exportarDataSelecionada().ano
    const mes = __dateSelect.exportarDataSelecionada().mes

    REQUEST_POSVENDA().receberTodos(ano, mes)
      .then(posVenda => {
        __componenteLista.importarItens(posVenda)
      })
  }

  return methods
}

module.exports = Module
