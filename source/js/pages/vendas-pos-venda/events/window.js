/* Requires */

const INIT_LISTA = require('../inits/componente-lista')
const INIT_SELECT = require('../inits/componente-select')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.povoarListaAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_LISTA().atualizar()
    })
  }

  methods.povoarSelectAoIniciar = () => {
    window.addEventListener('load', () => {
      INIT_SELECT().atualizarNomesClientes()
    })
  }

  return methods
}

module.exports = Module
