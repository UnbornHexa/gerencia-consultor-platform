/* Requires */

const EVENTS = require('../../../global/events/events')
const CHAT = require('../../../global/events/chat')
const TOGGLE = require('../../../structures/navbar/helper-toggle')
const WINDOW = require('./window')
const MODAL_UNIVERSAL = require('./modal-universal')
const PESQUISA = require('./pesquisa')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    TOGGLE().habilitarCliqueToggle()

    WINDOW().povoarListaAoIniciar()
    WINDOW().povoarSelectAoIniciar()

    MODAL_UNIVERSAL().habilitarCliqueBotaoFechar()
    MODAL_UNIVERSAL().habilitarCliqueBotaoOpcoesWhatsapp()

    PESQUISA().habilitarBarraPesquisa()
    CHAT().habilitarChat()
  }

  return methods
}

module.exports = Module
