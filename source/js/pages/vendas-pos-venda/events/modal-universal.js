/* Requires */

const MODAL_UNIVERSAL = require('../modals/modal-universal')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_universal')
  const __buttonFechar = __modal.querySelector('button#fechar')
  const __buttonWhatsapp = __modal.querySelector('.opcoes button#whatsapp')

  // Methods

  methods.habilitarCliqueBotaoFechar = () => {
    __buttonFechar.addEventListener('click', () => {
      MODAL_UNIVERSAL().ocultar()
    })
  }

  methods.habilitarCliqueBotaoOpcoesWhatsapp = () => {
    __buttonWhatsapp.addEventListener('click', () => {
      const telefoneCliente = __buttonWhatsapp.getAttribute('data-numero') || ''
      const pagina = `https://api.whatsapp.com/send?phone=55${telefoneCliente}`
      window.open(pagina, '_blank')
    })
  }

  return methods
}

module.exports = Module
