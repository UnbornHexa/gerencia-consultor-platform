const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.alterar .senhas')

  const __inputSenha = __section.querySelector('input#senha')
  const __inputConfirmarSenha = __section.querySelector('input#confirmar_senha')

  const __buttonAlterar = __section.querySelector('.botoes button#alterar')

  const __componenteAlerta = document.querySelector('app-alerta')

  // Methods

  methods.bloquearBotao = () => {
    __buttonAlterar.setAttribute('disabled', true)
  }

  methods.desbloquearBotao = () => {
    __buttonAlterar.removeAttribute('disabled')
  }

  /* Form */

  methods.verificarCamposObrigatorios = () => {
    if (__inputSenha.value && __inputConfirmarSenha.value) return true

    __componenteAlerta.alertar('Preencha todos os campos para criar sua conta.')
    return false
  }

  methods.verificarSenhaMinimo6Digitos = () => {
    if (__inputSenha.value.length >= 6) return true

    __componenteAlerta.alertar('A senha deve ter no mínimo 6 caractéres.')
    return false
  }

  methods.verificarSenhasIguais = () => {
    if (__inputSenha.value === __inputConfirmarSenha.value) return true

    __componenteAlerta.alertar('As senhas devem ser iguais.')
    return false
  }

  methods.limparCampos = () => {
    __inputSenha.value = ''
    __inputConfirmarSenha.value = ''
  }

  /* Data */

  methods.exportarDados = () => {
    const usuario = {}
    usuario.senha = __inputSenha.value
    return usuario
  }

  return methods
}

module.exports = Module
