const Module = () => {
  const methods = {}

  methods.exportarParametros = () => {
    const parametros = {}
    parametros.idUsuario = methods._idUsuario()
    parametros.tokenRecuperacao = methods._tokenRecuperacao()

    return parametros
  }

  methods._idUsuario = () => {
    return window.location.pathname.split('/')[2]
  }

  methods._tokenRecuperacao = () => {
    return window.location.pathname.split('/')[3]
  }

  return methods
}

module.exports = Module
