/* Requires */

const COMPONENTE_ALERTA = require('./componente-alerta')

/* Module */

const Module = () => {
  const methods = {}

  methods.iniciarTodosOsComponentes = () => {
    COMPONENTE_ALERTA().iniciar()
  }

  return methods
}

module.exports = Module
