/* Requires */

const REQUEST_AUTENTICACAO = require('../../../global/requests/autenticacao')
const SECTION_ALTERAR = require('../sections/section-alterar')
const HELPER_REDIRECIONAMENTO = require('../helpers/redirecionamento')
const HELPER_PARAMETROS = require('../helpers/parametros-url')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.alterar .senhas')
  const __buttonAlterar = __section.querySelector('button#alterar')
  const __inputSenha = __section.querySelector('input#senha')

  // Methods

  methods.habilitarCliqueButtonAlterar = () => {
    __buttonAlterar.addEventListener('click', () => {
      const dados = SECTION_ALTERAR().exportarDados()
      const parametros = HELPER_PARAMETROS().exportarParametros()

      const camposObrigatoriosOk = SECTION_ALTERAR().verificarCamposObrigatorios()
      if (!camposObrigatoriosOk) return

      const senhaSeguraOk = SECTION_ALTERAR().verificarSenhaMinimo6Digitos()
      if (!senhaSeguraOk) return

      const senhasIguais = SECTION_ALTERAR().verificarSenhasIguais()
      if (!senhasIguais) return

      SECTION_ALTERAR().bloquearBotao()
      REQUEST_AUTENTICACAO().redefinirSenha(parametros, dados)
        .then(() => {
          SECTION_ALTERAR().limparCampos()
          setTimeout(() => { HELPER_REDIRECIONAMENTO().entrar() }, 5000)
        })
        .finally(() => {
          SECTION_ALTERAR().desbloquearBotao()
        })
    })
  }

  methods.habilitarAlteracaoComTeclaEnter = () => {
    __inputSenha.addEventListener('keypress', (evento) => {
      const keyCode = evento.keyCode || evento.which
      if (keyCode !== 13) return false

      __buttonAlterar.click()
    })
  }

  return methods
}

module.exports = Module
