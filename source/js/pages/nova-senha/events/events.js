/* Requires */

const EVENTS = require('../../../global/events/events')
const SECTION_ALTERAR = require('./section-alterar')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()

    SECTION_ALTERAR().habilitarCliqueButtonAlterar()
    SECTION_ALTERAR().habilitarAlteracaoComTeclaEnter()
  }

  return methods
}

module.exports = Module
