/* Requires */

const EVENTOS = require('./events/events')
const INICIADORES = require('./inits/init')

/* Start */

INICIADORES().iniciarTodosOsComponentes()
EVENTOS().habilitarTodosOsEventos()
