const Module = () => {
  const methods = {}

  methods.acionarIcone = () => {
    const iconeNotificacoesMobile = document.querySelector('navbar .notificacoes a')
    iconeNotificacoesMobile.classList.add('nova')
  }

  return methods
}

module.exports = Module
