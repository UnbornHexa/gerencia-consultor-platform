/* Requires */

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.informacoes')
  const __divEncomendas = __section.querySelector('.encomendas')
  const __divPosVendas = __section.querySelector('.posvenda')
  const __divHorarios = __section.querySelector('.compromissos')
  const __divAnotacoes = __section.querySelector('.lembretes')

  // Methods

  methods.exibirQuantidadeEncomendas = (quantidade) => {
    const descricao = (quantidade === 1)
      ? `Você tem ${quantidade} encomenda para entregar nos próximos 7 dias`
      : `Você tem ${quantidade} encomendas para entregar nos próximos 7 dias`

    const pNumero = __divEncomendas.querySelector('p.numero')
    const pDescricao = __divEncomendas.querySelector('p.descricao')
    pNumero.innerText = quantidade
    pDescricao.innerText = descricao
  }

  methods.exibirQuantidadePosVendas = (quantidade) => {
    const descricao = (quantidade === 1)
      ? `Você tem ${quantidade} pós-venda para ser feito nos próximos 7 dias`
      : `Você tem ${quantidade} pós-vendas para serem feitos nos próximos 7 dias`

    const pNumero = __divPosVendas.querySelector('p.numero')
    const pDescricao = __divPosVendas.querySelector('p.descricao')
    pNumero.innerText = quantidade
    pDescricao.innerText = descricao
  }

  methods.exibirQuantidadeAnotacoes = (quantidade) => {
    const descricao = (quantidade === 1)
      ? `Você tem ${quantidade} anotação marcada para ver hoje`
      : `Você tem ${quantidade} anotações marcadas para ver hoje`

    const pNumero = __divAnotacoes.querySelector('p.numero')
    const pDescricao = __divAnotacoes.querySelector('p.descricao')
    pNumero.innerText = quantidade
    pDescricao.innerText = descricao
  }

  methods.exibirQuantidadeHorarios = (quantidade) => {
    const descricao = (quantidade === 1)
      ? `Você tem ${quantidade} compromisso na agenda para hoje`
      : `Você tem ${quantidade} compromissos na agenda para hoje`

    const pNumero = __divHorarios.querySelector('p.numero')
    const pDescricao = __divHorarios.querySelector('p.descricao')
    pNumero.innerText = quantidade
    pDescricao.innerText = descricao
  }

  return methods
}

module.exports = Module
