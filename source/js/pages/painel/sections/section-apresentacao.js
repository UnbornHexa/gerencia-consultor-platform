/* Requires */

const HELPER_PERFIL = require('../../../structures/header/helper-perfil')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __section = document.querySelector('section.apresentacao')
  const __divPerfil = __section.querySelector('.perfil .info')
  const __divNotificacoes = __section.querySelector('.notificacoes')

  // Methods

  methods.exibirQuantidadeNotificacoes = (quantidade) => {
    const texto = (quantidade === 1)
      ? `Você tem ${quantidade} notificação nova`
      : `Você tem ${quantidade} notificações novas`

    const p = __divNotificacoes.querySelector('p')
    p.innerText = texto
  }

  methods.exibirSaudacao = () => {
    const saudacao = methods._construirSaudacao()
    const nome = HELPER_PERFIL().receberNomeUsuario()
    const pNome = __divPerfil.querySelector('.nome')
    pNome.innerText = `${saudacao}, ${nome}!`
  }

  methods.exibirAssinatura = (assinatura) => {
    const dataSubtracao = new Date(assinatura.dataVencimento) - new Date()
    const numeroDiasSubtracao = Math.ceil(dataSubtracao / (1000 * 60 * 60 * 24))
    const diasFaltantes = (numeroDiasSubtracao > 0) ? numeroDiasSubtracao : 0

    let nomeAssinatura
    if (assinatura.tipo === 'gratis') nomeAssinatura = 'Grátis'
    else if (assinatura.tipo === 'black') nomeAssinatura = 'Black'
    else if (assinatura.tipo === 'especial') nomeAssinatura = 'Especial'

    const texto = (diasFaltantes === 1)
      ? `Sua assinatura ${nomeAssinatura} expira em ${diasFaltantes} dia.`
      : `Sua assinatura ${nomeAssinatura} expira em ${diasFaltantes} dias.`

    const pStatus = __divPerfil.querySelector('.status')
    pStatus.innerText = texto
  }

  methods._construirSaudacao = () => {
    const hora = new Date().getHours()
    if (Number(hora) >= 6 && Number(hora) < 12) return 'Bom dia'
    if (Number(hora) >= 12 && Number(hora) < 18) return 'Boa tarde'
    if (Number(hora) >= 18 && Number(hora) < 24) return 'Boa noite'
    if (Number(hora) < 6) return 'Boa madrugada'
  }

  return methods
}

module.exports = Module
