/* Requires */

const HELPER_LOGOUT = require('../../global/helpers/logout')
const HELPER_ASSINATURA = require('../../global/helpers/assinatura-expirada')
const HELPER_ASIDE = require('../../structures/aside/helper-selecionador')
const EVENTOS = require('./events/events')
const INICIADORES = require('./inits/init')

/* Start */

HELPER_LOGOUT().checkTokenOk()
HELPER_ASSINATURA().checkAssinaturaOk()
INICIADORES().iniciarTodosOsComponentes()
EVENTOS().habilitarTodosOsEventos()
HELPER_ASIDE().selecionarPagina('painel')
