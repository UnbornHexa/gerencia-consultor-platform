/* Requires */

const REQUEST_ASSINATURAS = require('../../../global/requests/assinaturas')
const REQUEST_NOTIFICACOES = require('../../../global/requests/notificacoes')
const REQUEST_POSVENDA = require('../../../global/requests/pos-venda')
const REQUEST_ENCOMENDAS = require('../../../global/requests/encomendas')
const REQUEST_HORARIOS = require('../../../global/requests/horarios')
const REQUEST_ANOTACOES = require('../../../global/requests/anotacoes')
const HELPER_NOTIFICACOES = require('../helpers/helper-notificacao')

const SECTION_APRESENTACAO = require('../sections/section-apresentacao')
const SECTION_INFORMACOES = require('../sections/section-informacoes')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.exibirQuantidadeNotificacoesNovas = () => {
    REQUEST_NOTIFICACOES().contar()
      .then(quantidade => {
        SECTION_APRESENTACAO().exibirQuantidadeNotificacoes(quantidade)
        if (quantidade > 0) HELPER_NOTIFICACOES().acionarIcone()
      })
  }

  methods.exibirSaudacao = () => {
    SECTION_APRESENTACAO().exibirSaudacao()
  }

  methods.exibirQuantidadeEncomendasDaSemana = () => {
    REQUEST_ENCOMENDAS().contar()
      .then(quantidade => {
        SECTION_INFORMACOES().exibirQuantidadeEncomendas(quantidade)
      })
  }

  methods.exibirQuantidadePosVendasDaSemana = () => {
    REQUEST_POSVENDA().contar()
      .then(quantidade => {
        SECTION_INFORMACOES().exibirQuantidadePosVendas(quantidade)
      })
  }

  methods.exibirQuantidadeAnotacoesHoje = () => {
    REQUEST_ANOTACOES().contar()
      .then(quantidade => {
        SECTION_INFORMACOES().exibirQuantidadeAnotacoes(quantidade)
      })
  }

  methods.exibirQuantidadeHorariosHoje = () => {
    REQUEST_HORARIOS().contar()
      .then(quantidade => {
        SECTION_INFORMACOES().exibirQuantidadeHorarios(quantidade)
      })
  }

  methods.exibirAssinatura = () => {
    REQUEST_ASSINATURAS().receberPorUsuario()
      .then(assinatura => {
        SECTION_APRESENTACAO().exibirAssinatura(assinatura)
      })
  }

  return methods
}

module.exports = Module
