/* Requires */

const CHAT = require('../../../global/helpers/chat')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __chatNavbar = document.querySelector('navbar a#chat_mobile_start')
  const __chatApresentacao = document.querySelector('section.apresentacao a#chat_desktop_1_start')
  const __chatAtalhos = document.querySelector('section.atalhos a#chat_desktop_2_start')

  // Methods

  methods.habilitarChat = () => {
    CHAT().construirChat()
    methods._habilitarCliqueNoMobile()
    methods._habilitarCliqueNoDesktop()
  }

  methods._habilitarCliqueNoMobile = () => {
    __chatNavbar.addEventListener('click', () => {
      CHAT().abrirChat()
    })
  }

  methods._habilitarCliqueNoDesktop = () => {
    __chatApresentacao.addEventListener('click', () => {
      CHAT().abrirChat()
    })
    __chatAtalhos.addEventListener('click', () => {
      CHAT().abrirChat()
    })
  }

  return methods
}

module.exports = Module
