/* Requires */

const EVENTS = require('../../../global/events/events')
const CHAT = require('./chat')
const TOGGLE = require('../../../structures/navbar/helper-toggle')
const WINDOW = require('./window')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    TOGGLE().habilitarCliqueToggle()

    WINDOW().exibirSaudacao()
    WINDOW().exibirAssinatura()
    WINDOW().exibirQuantidadeEncomendasDaSemana()
    WINDOW().exibirQuantidadePosVendasDaSemana()
    WINDOW().exibirQuantidadeNotificacoesNovas()
    WINDOW().exibirQuantidadeHorariosHoje()
    WINDOW().exibirQuantidadeAnotacoesHoje()

    CHAT().habilitarChat()
  }

  return methods
}

module.exports = Module
