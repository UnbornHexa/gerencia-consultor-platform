/* Requires */

const TOKEN = require('../../global/helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __pNomeUsuario = document.querySelector('header p#nome_usuario')

  // Methods

  methods.exibirNomeUsuario = () => {
    const nomeUsuario = methods.receberNomeUsuario()
    __pNomeUsuario.innerText = nomeUsuario
  }

  methods.receberNomeUsuario = () => {
    const nomeDoToken = window.localStorage.getItem('nome-usuario') || TOKEN().readUsuarioNome()
    const nomeDividido = nomeDoToken.split(' ')
    const nomeAbreviado = (nomeDividido.length > 1)
      ? nomeDividido[0] + ' ' + nomeDividido[1]
      : nomeDoToken
    return nomeAbreviado
  }

  return methods
}

module.exports = Module
