/* Requires */

const NOTIFICACOES_REQUEST = require('../../global/requests/notificacoes')

/* Module */

const Module = () => {
  const methods = {}

  methods.verificarNovasNotificacoes = () => {
    NOTIFICACOES_REQUEST().contar()
      .then(quantidade => {
        if (quantidade <= 0) return

        methods._acionarIcone()
      })
  }

  methods._acionarIcone = () => {
    const iconeNotificacoesDesktop = document.querySelector('header .opcoes a.notificacoes')
    const iconeNotificacoesMobile = document.querySelector('navbar .notificacoes a')

    iconeNotificacoesMobile.classList.add('nova')
    iconeNotificacoesDesktop.classList.add('nova')
  }

  return methods
}

module.exports = Module
