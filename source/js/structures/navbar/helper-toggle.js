const Module = () => {
  const methods = {}

  // Internal Variables

  const __toggle = document.querySelector('navbar .funcionalidades .funcionalidades__abrir')

  // Methods

  methods.habilitarCliqueToggle = () => {
    __toggle.addEventListener('click', () => {
      const aside = document.querySelector('aside')
      __toggle.classList.toggle('show')
      aside.classList.toggle('close')
    })
  }

  return methods
}

module.exports = Module
