/* Requires */

const HELPER_BODY_SCROLL = require('../body/helper-scroll')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section.modal')

  // Methods

  methods.aplicarEfeitoAbrir = () => {
    __modal.classList.remove('modal-fechado')
    __modal.classList.add('modal-aberto')
    HELPER_BODY_SCROLL().desativarScroll()
  }

  methods.aplicarEfeitoFechar = () => {
    __modal.classList.remove('modal-aberto')
    __modal.classList.add('modal-fechado')
    HELPER_BODY_SCROLL().ativarScroll()
  }

  return methods
}

module.exports = Module
