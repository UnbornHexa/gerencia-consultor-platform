const Module = () => {
  const methods = {}

  methods.selecionarPagina = (pagina) => {
    const a = document.querySelector(`aside .funcionalidades a[name="${pagina}"`)
    a.classList.add('ativo')
  }

  return methods
}

module.exports = Module
