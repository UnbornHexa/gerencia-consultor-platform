const Module = () => {
  const methods = {}

  methods.ativarScroll = () => {
    document.body.classList.remove('desativar-scroll')
  }

  methods.desativarScroll = () => {
    document.body.classList.add('desativar-scroll')
  }

  return methods
}

module.exports = Module
