/* Requires */

const HTTP = require('../helpers/http')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.receber = (cep) => {
    const url = `https://viacep.com.br/ws/${cep}/json/`

    return new Promise((resolve, reject) => {
      HTTP().get(url, null, (response) => {
        if (response.status === 200) {
          return resolve(response.body)
        }

        reject(new Error(response.body))
      })
    })
  }

  return methods
}

module.exports = Module
