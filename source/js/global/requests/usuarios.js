/* Requires */

const HOSTS = require('../data/hosts')
const HTTP = require('../helpers/http')
const TOKEN = require('../helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __url = `${HOSTS().url.apiPlataforma}/usuarios`
  const __token = TOKEN().getToken()
  const __usuarioId = TOKEN().readUsuarioId()
  const __headers = HTTP().setHeadersWithToken(__token)
  const __componenteAlerta = document.querySelector('app-alerta')

  // Methods

  methods.receberPorId = () => {
    const url = `${__url}/${__usuarioId}`

    return new Promise((resolve, reject) => {
      HTTP().get(url, __headers, (response) => {
        if (response.status === 200) {
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  methods.editar = (data) => {
    const url = `${__url}/${__usuarioId}`

    return new Promise((resolve, reject) => {
      HTTP().put(url, data, __headers, (response) => {
        if (response.status === 200) {
          __componenteAlerta.alertar(response.body, 'positivo')
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  methods.editarSenha = (data) => {
    const url = `${__url}/editar-senha/${__usuarioId}`

    return new Promise((resolve, reject) => {
      HTTP().patch(url, data, __headers, (response) => {
        if (response.status === 200) {
          __componenteAlerta.alertar(response.body, 'positivo')
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  return methods
}

module.exports = Module
