/* Requires */

const HOSTS = require('../data/hosts')
const HTTP = require('../helpers/http')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __url = `${HOSTS().url.apiPlataforma}/autenticacao`
  const __componenteAlerta = document.querySelector('app-alerta')

  // Methods

  methods.registrar = (data) => {
    const url = `${__url}/registrar`

    return new Promise((resolve, reject) => {
      HTTP().post(url, data, null, (response) => {
        if (response.status === 200) {
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  methods.entrar = (data) => {
    const url = `${__url}/entrar`

    return new Promise((resolve, reject) => {
      HTTP().post(url, data, null, (response) => {
        if (response.status === 200) {
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  methods.recuperarSenha = (email) => {
    const url = `${__url}/recuperar/senha/${email}`

    return new Promise((resolve, reject) => {
      HTTP().get(url, null, (response) => {
        if (response.status === 200) {
          __componenteAlerta.alertar(response.body, 'positivo')
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  methods.redefinirSenha = (parametros, dados) => {
    const idUsuario = parametros.idUsuario
    const tokenRecuperacao = parametros.tokenRecuperacao

    const url = `${__url}/senha/${idUsuario}/${tokenRecuperacao}`

    return new Promise((resolve, reject) => {
      HTTP().put(url, dados, null, (response) => {
        if (response.status === 200) {
          __componenteAlerta.alertar(response.body, 'positivo')
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  return methods
}

module.exports = Module
