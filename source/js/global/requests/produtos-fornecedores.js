/* Requires */

const HOSTS = require('../data/hosts')
const HTTP = require('../helpers/http')
const TOKEN = require('../helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __url = `${HOSTS().url.apiPlataforma}/produtos-fornecedores`
  const __token = TOKEN().getToken()
  const __headers = HTTP().setHeadersWithToken(__token)
  const __componenteAlerta = document.querySelector('app-alerta')

  // Methods

  methods.receberTodosHinode = () => {
    const url = `${__url}/hinode/`

    return new Promise((resolve, reject) => {
      HTTP().get(url, __headers, (response) => {
        if (response.status === 200) {
          return resolve(response.body)
        }

        __componenteAlerta.alertar(response.body, 'negativo')
        reject(new Error(response.body))
      })
    })
  }

  return methods
}

module.exports = Module
