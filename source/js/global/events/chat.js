/* Requires */

const CHAT = require('../../global/helpers/chat')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __chatMobile = document.querySelector('navbar a#chat_mobile_start')
  const __chatDesktop = document.querySelector('a#chat_desktop_start')

  // Methods

  methods.habilitarChat = () => {
    CHAT().construirChat()
    methods._habilitarCliqueNoMobile()
    methods._habilitarCliqueNoDesktop()
  }

  methods._habilitarCliqueNoMobile = () => {
    __chatMobile.addEventListener('click', () => {
      CHAT().abrirChat()
    })
  }

  methods._habilitarCliqueNoDesktop = () => {
    __chatDesktop.addEventListener('click', () => {
      CHAT().abrirChat()
    })
  }

  return methods
}

module.exports = Module
