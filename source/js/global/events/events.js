/* Requires */

const INPUT = require('./input')
const TEXTAREA = require('./textarea')
const WINDOW = require('./window')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventosGlobais = () => {
    INPUT().habilitarMascaraGenerica()
    INPUT().habilitarMascaraTelefone()
    INPUT().habilitarMascaraDinheiro()
    INPUT().habilitarBloqueioLetrasTipoTel()

    TEXTAREA().habilitarTamanhoAutomatico()
    WINDOW().habilitarBloqueioDragAndDrop()
  }

  return methods
}

module.exports = Module
