/* Requires */

const MASK = require('../helpers/mask')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarBloqueioLetrasTipoTel = () => {
    document.addEventListener('keypress', (evento) => { handler(evento) })
    document.addEventListener('keyup', (evento) => { handler(evento) })

    function handler (evento) {
      if (!evento.target.matches('input[type="tel"]')) return

      const caracteresValidos = evento.target.value.replace(/(?![R])[a-z|A-Z]/g, '')
      evento.target.value = caracteresValidos
    }
  }

  // Generic

  methods.habilitarMascaraGenerica = () => {
    document.addEventListener('keypress', (evento) => { handler(evento) })
    document.addEventListener('keyup', (evento) => { handler(evento) })

    function handler (evento) {
      if (!evento.target.matches('input')) return
      if (!evento.target.getAttribute('data-mask')) return

      const pattern = evento.target.getAttribute('data-mask')
      if (evento.target.value.length >= pattern.length) evento.preventDefault()
      evento.target.value = MASK().generic(evento.target.value, pattern)
    }
  }

  // Phone

  methods.habilitarMascaraTelefone = () => {
    document.addEventListener('keypress', (evento) => { handler(evento) })
    document.addEventListener('keyup', (evento) => { handler(evento) })

    function handler (evento) {
      if (!evento.target.matches('input')) return
      if (!evento.target.getAttribute('data-mask-phone')) return
      if (evento.target.value.length >= 16) evento.preventDefault()

      let pattern = '(##) ####-#####'
      const phone = evento.target.value.replace(/[^0-9]/g, '')
      if (phone.length > 10) pattern = '(##) # ####-####'

      evento.target.value = MASK().generic(evento.target.value, pattern)
    }
  }

  // Money

  methods.habilitarMascaraDinheiro = () => {
    document.addEventListener('keyup', (evento) => {
      if (!evento.target.matches('input')) return
      if (!evento.target.getAttribute('data-mask-money')) return

      evento.target.value = MASK().moneyMask(evento.target.value)
    })
  }

  return methods
}

module.exports = Module
