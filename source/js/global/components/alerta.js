class Lista extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      mensagem: '',
      tipo: '',
      cancelarDesaparecimento: null
    }

    this.habilitarCliqueParaDesaparecer()
  }

  alertar (mensagem, tipo = 'normal') {
    this.__state.mensagem = mensagem
    this.__state.tipo = tipo

    this._renderizar()
    this.classList.add('mostrar')
    this.habilitarTempoParaDesaparecer()
  }

  // Renderizacao
  _renderizar () {
    const mensagem = this.__state.mensagem
    const tipo = this.__state.tipo

    this.classList = ''
    this.classList.add(tipo)

    this.innerHTML = `
    <div class="icone"></div>
    <p>${mensagem}</p>
    `
  }

  // Eventos
  habilitarTempoParaDesaparecer () {
    clearTimeout(this.__state.cancelarDesaparecimento)
    this.__state.cancelarDesaparecimento = setTimeout(() => {
      this.classList.remove('mostrar')
    }, 5000)
  }

  habilitarCliqueParaDesaparecer () {
    this.addEventListener('click', () => {
      this.classList.remove('mostrar')
    })
  }
}

module.exports = Lista
