/* Requires */

const ACCENT = require('../helpers/accent')

/* Module */

class Select extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      opcoes: [],
      selecionado: {},
      cancelarPesquisa: null,
      limiteDeResultados: 30
    }

    this.renderizar()
    this.habilitarFoco()
    this.habilitarCliqueParaAbrirDropdown()
    this.habilitarCliqueOpcao()
    this.habilitarPesquisaInput()
    this.habilitarCliqueExterno()
  }

  // Build Component

  renderizar () {
    this.innerHTML = `
    <p class="texto"></p>
    <div class="dropdown">
      <div class="busca ocultar">
        <input spellcheck="false" autocomplete="off" type="text" placeholder="Pesquisar">
      </div>
      <div class="opcoes"></div>
    </div>
    `
  }

  // Dropdown

  _mostrarDropdown () {
    const dropdown = this.querySelector('.dropdown')
    dropdown.classList.add('mostrar')
  }

  _ocultarDropdown () {
    const dropdown = this.querySelector('.dropdown')
    dropdown.classList.remove('mostrar')
  }

  _alternarDropdown () {
    const dropdown = this.querySelector('.dropdown')
    if (dropdown.classList.contains('mostrar')) {
      this._ocultarDropdown()
      return
    }
    this._mostrarDropdown()
  }

  _verificarDisabled () {
    if (this.getAttribute('disabled')) return true
    return false
  }

  // Busca

  gerenciarBusca () {
    if (this.__state.opcoes.length > 0) {
      this.querySelector('.busca').classList.remove('ocultar')
      return
    }

    this.querySelector('.busca').classList.add('ocultar')
  }

  _limparBusca () {
    const busca = this.querySelector('.dropdown .busca input')
    busca.value = ''
  }

  // Opcoes

  importarOpcoes (opcoes) {
    this.__state.opcoes = opcoes
    this._exibirPrimeirosResultados()
  }

  _exibirPrimeirosResultados () {
    this._limparOpcoes()
    this.gerenciarBusca()
    const opcoesExibidas = this.__state.opcoes.slice(0, this.__state.limiteDeResultados)

    const opcaoDiv = this.querySelector('.dropdown .opcoes')
    const opcoesExibidasMap = opcoesExibidas
      .map(opcao => { return `<div data-value="${opcao.valor}">${opcao.texto}</div>` })
      .join('')

    opcaoDiv.innerHTML = opcoesExibidasMap
  }

  filtrar (texto) {
    const elementoOpcoes = this.querySelector('.dropdown .opcoes')
    this._limparOpcoes()

    const opcoesFiltradas = this.__state.opcoes
      .filter(opcao => {
        const termoPesquisado = ACCENT().remove(texto.toString().toUpperCase())
        const opcaoAVerificar = ACCENT().remove(opcao.texto.toUpperCase())
        return (opcaoAVerificar.includes(termoPesquisado))
      })
      .slice(0, this.__state.limiteDeResultados)
      .map(opcao => { return `<div data-value="${opcao.valor}">${opcao.texto}</div>` })
      .join('')

    elementoOpcoes.innerHTML = opcoesFiltradas
  }

  _limparOpcoes () {
    const opcoes = this.querySelector('.dropdown .opcoes')
    opcoes.innerHTML = ''
  }

  // Utilidades

  exportarValorSelecionado () {
    return this.__state.selecionado.valor
  }

  exportarTextoSelecionado () {
    return this.__state.selecionado.texto
  }

  vazio () {
    if (Object.entries(this.__state.selecionado).length === 0) return true
    return false
  }

  desativar () {
    this.setAttribute('disabled', true)
  }

  // Selecionado

  selecionarOpcao (valor) {
    const selecionadoP = this.querySelector('p.texto')

    for (const opcao of this.__state.opcoes) {
      if (opcao.valor === valor) {
        selecionadoP.innerText = opcao.texto
        this.__state.selecionado = {
          valor: opcao.valor,
          texto: opcao.texto
        }
        break
      }
    }
  }

  limparSelecionado () {
    this.__state.selecionado = {}
    this.querySelector('p.texto').innerText = ''
  }

  // Eventos

  habilitarFoco () {
    this.addEventListener('focus', (evento) => {
      if (this._verificarDisabled()) return

      this._mostrarDropdown()
    })
  }

  habilitarCliqueParaAbrirDropdown () {
    document.addEventListener('click', (evento) => {
      if (evento.target !== this) return
      if (this._verificarDisabled()) return

      this._mostrarDropdown()
    })
  }

  habilitarCliqueExterno () {
    document.addEventListener('click', (evento) => {
      if (this.contains(evento.target)) return

      this._ocultarDropdown()
    })
  }

  habilitarCliqueOpcao () {
    this.addEventListener('click', (evento) => {
      const elemento = '.dropdown .opcoes div'
      if (!evento.target.matches(elemento)) return

      const valor = evento.target.getAttribute('data-value')

      this.selecionarOpcao(valor)
      this._limparBusca()
      this._exibirPrimeirosResultados()
      this._ocultarDropdown()
    })
  }

  habilitarPesquisaInput () {
    this.addEventListener('keyup', (evento) => {
      const elemento = '.dropdown .busca input'
      if (!evento.target.matches(elemento)) return

      clearTimeout(this.__state.cancelarPesquisa)
      if (evento.target.value.length < 1) {
        this._exibirPrimeirosResultados()
        return
      }

      this.__state.cancelarPesquisa = setTimeout(() => {
        this.filtrar(evento.target.value)
      }, 500)
    })
  }

  // Opcoes PreConfiguradas

  importarEstados () {
    this.__state.opcoes = [
      { valor: 'AC', texto: 'Acre' },
      { valor: 'AL', texto: 'Alagoas' },
      { valor: 'AP', texto: 'Amapá' },
      { valor: 'AM', texto: 'Amazonas' },
      { valor: 'BA', texto: 'Bahia' },
      { valor: 'CE', texto: 'Ceará' },
      { valor: 'DF', texto: 'Distrito Federal' },
      { valor: 'ES', texto: 'Espírito Santo' },
      { valor: 'GO', texto: 'Goiás' },
      { valor: 'MA', texto: 'Maranhão' },
      { valor: 'MT', texto: 'Mato Grosso' },
      { valor: 'MS', texto: 'Mato Grosso do Sul' },
      { valor: 'MG', texto: 'Minas Gerais' },
      { valor: 'PA', texto: 'Pará' },
      { valor: 'PB', texto: 'Paraíba' },
      { valor: 'PR', texto: 'Paraná' },
      { valor: 'PE', texto: 'Pernambuco' },
      { valor: 'PI', texto: 'Piauí' },
      { valor: 'RJ', texto: 'Rio de Janeiro' },
      { valor: 'RN', texto: 'Rio Grande do Norte' },
      { valor: 'RS', texto: 'Rio Grande do Sul' },
      { valor: 'RO', texto: 'Rondônia' },
      { valor: 'RR', texto: 'Roraima' },
      { valor: 'SC', texto: 'Santa Catarina' },
      { valor: 'SP', texto: 'São Paulo' },
      { valor: 'SE', texto: 'Sergipe' },
      { valor: 'TO', texto: 'Tocantins' }
    ]

    this._exibirPrimeirosResultados()
  }

  importarMetodosDePagamento () {
    this.__state.opcoes = [
      { valor: 'dinheiro', texto: 'Dinheiro' },
      { valor: 'credito', texto: 'Crédito' },
      { valor: 'debito', texto: 'Débito' }
    ]

    this._exibirPrimeirosResultados()
  }
}

module.exports = Select
