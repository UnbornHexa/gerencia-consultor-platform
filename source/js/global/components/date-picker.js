/* Requires */

const DATE = require('../helpers/date')

/* Module */

class DatePicker extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      nomeDiasDaSemana: [
        'D',
        'S',
        'T',
        'Q',
        'Q',
        'S',
        'S'
      ],
      nomeMeses: [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
      ],
      dataSelecionada: {
        ano: 0,
        mes: 0
      }
    }

    this._renderizar()
    this.atualizar()
    this.selecionarData(DATE().todayLocal())

    this.habilitarFoco()
    this.habilitarCliqueInput()
    this.habilitarCliqueAvancar()
    this.habilitarCliqueVoltar()
    this.habilitarCliqueExterno()
    this.habilitarCliqueDia()
  }

  // Renderizar

  _renderizar () {
    this.innerHTML = `
    <input class="visor" type="text" readonly="true">
    <div class="calendario">
      <div class="controles">
        <div class="voltar"><i class="icon-seta-esquerda"></i></div>
        <div class="data">
          <span class="mes"></span>
          <span class="ano"></span>
        </div>
        <div class="avancar"><i class="icon-seta-direita"></i></div>
      </div>
      <div class="titulos"></div>
      <div class="dias"></div>
    </div>
    `
  }

  _renderizarDias () {
    const diasDiv = this.querySelector('.dias')
    diasDiv.innerHTML = ''

    const dataSelecionada = this.__state.dataSelecionada
    const indiceDoDiaDaSemana = DATE().getIndexOfWeekDay(dataSelecionada.ano, dataSelecionada.mes)
    const numeroDeDias = DATE().getHowManyDaysMonthHas(dataSelecionada.ano, dataSelecionada.mes)
    const listaDias = []

    // passo 1 - adiciona blocos vazios
    for (const indice of [...Array(indiceDoDiaDaSemana).keys()]) { // eslint-disable-line
      const span = '<span class="vazio"></span>'
      listaDias.push(span)
    }

    // passo 2 - adicionar blocos de dias
    for (const indice of [...Array(numeroDeDias).keys()]) { // eslint-disable-line
      const dia = indice + 1
      const hojeClasse = (this._verificarHoje(dia)) ? 'class="hoje"' : ''
      const span = `<span data-dia="${dia}" ${hojeClasse}>${dia}</span>`
      listaDias.push(span)
    }

    diasDiv.innerHTML = listaDias.join('')
  }

  _renderizarTitulos () {
    const titulosDiv = this.querySelector('.titulos')
    titulosDiv.innerHTML = ''
    const listaTitulos = []

    for (const titulo of this.__state.nomeDiasDaSemana) {
      const span = `<span>${titulo}</span>`
      listaTitulos.push(span)
    }

    titulosDiv.innerHTML += listaTitulos.join('')
  }

  _exibirData () {
    const ano = this.__state.dataSelecionada.ano
    const mes = this.__state.dataSelecionada.mes
    const nomeDoMes = this._receberNomeMes(mes)

    const mesSpan = this.querySelector('.controles .data .mes')
    const anoSpan = this.querySelector('.controles .data .ano')

    mesSpan.innerText = nomeDoMes
    anoSpan.innerText = ano
  }

  atualizar () {
    this._renderizarTitulos()
    this._renderizarDias()
    this._exibirData()
  }

  // Calendario

  _mostrarCalendario () {
    const calendarioDiv = this.querySelector('.calendario')
    calendarioDiv.classList.add('mostrar')
  }

  _ocultarCalendario () {
    const calendarioDiv = this.querySelector('.calendario')
    calendarioDiv.classList.remove('mostrar')
  }

  _verificarDisabled () {
    if (this.getAttribute('disabled') === 'true') return true
    return false
  }

  // Control

  vazio () {
    return (!this.querySelector('input').value)
  }

  desativar () {
    const inputVisor = this.querySelector('input.visor')
    this.setAttribute('disabled', true)
    inputVisor.setAttribute('disabled', true)
  }

  exportarData () {
    return this.querySelector('input').value
  }

  resetar () {
    this.limpar()
    this.selecionarData(DATE().todayLocal())
  }

  selecionarData (data) {
    if (!data) return
    const timestamp = DATE().convertBRToTimestamp(data)
    const dataObj = DATE().dateToObj(DATE().timestampToDate(timestamp))

    this.__state.dataSelecionada.mes = dataObj.month
    this.__state.dataSelecionada.ano = dataObj.year

    this.querySelector('input').value = data
    this.atualizar()
  }

  limpar () {
    this.querySelector('input').value = ''
    const hoje = this._receberDataAtual()
    this.__state.dataSelecionada.ano = hoje.year
    this.__state.dataSelecionada.mes = hoje.month
    this.atualizar()
  }

  _avancarMes () {
    if (this.__state.dataSelecionada.mes < 12) {
      this.__state.dataSelecionada.mes++
    } else {
      this.__state.dataSelecionada.mes = 1
      this.__state.dataSelecionada.ano++
    }

    this.atualizar()
  }

  _voltarMes () {
    if (this.__state.dataSelecionada.mes > 1) {
      this.__state.dataSelecionada.mes--
    } else {
      this.__state.dataSelecionada.mes = 12
      this.__state.dataSelecionada.ano--
    }

    this.atualizar()
  }

  // Funcoes Auxiliares

  _receberDataAtual () {
    const hoje = new Date()
    const objHoje = DATE().dateToObj(hoje)

    return objHoje
  }

  _receberNomeMes (mes) {
    if (!mes) return
    return this.__state.nomeMeses[mes - 1]
  }

  _verificarHoje (dia) {
    const hoje = this._receberDataAtual()

    if (Number(this.__state.dataSelecionada.ano) !== Number(hoje.year)) return false
    if (Number(this.__state.dataSelecionada.mes) !== Number(hoje.month)) return false
    if (Number(dia) !== Number(hoje.day)) return false

    return true
  }

  // Events

  habilitarCliqueInput () {
    this.addEventListener('click', (evento) => {
      const elemento = 'input'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      this._mostrarCalendario()
    })
  }

  habilitarFoco () {
    this.addEventListener('focus', (evento) => {
      if (this._verificarDisabled()) return

      this._mostrarCalendario()
    })
  }

  habilitarCliqueExterno () {
    document.addEventListener('click', (evento) => {
      if (this.contains(evento.target)) return

      this._ocultarCalendario()
    })
  }

  habilitarCliqueAvancar () {
    this.addEventListener('click', (evento) => {
      const elemento = '.calendario .controles .avancar'
      if (!evento.target.matches(elemento)) return

      this._avancarMes()
    })
  }

  habilitarCliqueVoltar () {
    this.addEventListener('click', (evento) => {
      const elemento = '.calendario .controles .voltar'
      if (!evento.target.matches(elemento)) return

      this._voltarMes()
    })
  }

  habilitarCliqueDia () {
    this.addEventListener('click', (evento) => {
      const elemento = '.calendario .dias span'
      if (!evento.target.matches(elemento)) return

      const diaSelecionado = evento.target.getAttribute('data-dia').toString().padStart(2, '0')
      const mesSelecionado = this.__state.dataSelecionada.mes.toString().padStart(2, '0')
      const anoSelecionado = this.__state.dataSelecionada.ano.toString().padStart(4, '0')

      const dataSelecionada = `${diaSelecionado}/${mesSelecionado}/${anoSelecionado}`
      this.selecionarData(dataSelecionada)
    })
  }
}

module.exports = DatePicker
