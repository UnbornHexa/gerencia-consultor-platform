class Checkbox extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      ativo: false
    }

    this._renderizar()
    this.habilitarClique()
  }

  // Renderizacao

  _renderizar () {
    this.innerHTML = `
    <div class="manipulador">
    <div class="bolinha"></div>
    </div>
    `
  }

  // Utils

  limpar () {
    this.desativar()
  }

  alternar () {
    (this.verificar()) ? this.desativar() : this.ativar()
  }

  desativar () {
    this.__state.ativo = false
    this.querySelector('.manipulador').classList.remove('ativo')
  }

  ativar () {
    this.__state.ativo = true
    this.querySelector('.manipulador').classList.add('ativo')
  }

  verificar () {
    return this.__state.ativo
  }

  // Eventos

  habilitarClique () {
    this.addEventListener('click', (evento) => {
      this.alternar()
    })
  }
}

module.exports = Checkbox
