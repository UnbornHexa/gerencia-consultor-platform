/* Requires */

const FORMAT = require('../helpers/format')
const DATE = require('../helpers/date')

/* Module */

class Pagamento extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      metodosPagamento: [
        { texto: 'Dinheiro', valor: 'dinheiro' },
        { texto: 'Débito', valor: 'debito' },
        { texto: 'Crédito', valor: 'credito' },
        { texto: 'Boleto', valor: 'boleto' }
      ],
      numeroParcelas: 0,
      valorTotal: 0,
      valorRestante: 0,
      dataParcelaAtual: ''
    }

    this._renderizar()
    this.limpar()
    this.habilitarFoco()
    this.habilitarCliqueSelecionandoInputValorTotal()
    this.habilitarKeyupInputValorTotal()
    this.habilitarCliqueSelecionandoInputsValor()
    this.habilitarKeyupInputsValor()
    this.habilitarCliqueAdicionarParcela()
    this.habilitarCliqueDeletarParcela()
    this.habilitarCliqueTrocandoMetodoPagamento()
    this.habilitarCliqueBotaoCalcular()
  }

  // Renderizar

  _renderizar () {
    this.innerHTML = `
    <!-- Valor Total -->
    <label>Valor Total <span class="required">*</span> </label>
    <input spellcheck="false" id="valor_total" autocomplete="off" type="text" placeholder="Qual o valor?" data-mask-money="true">
    <!-- Calcular Parcelas -->
    <div class="calcular">
      <button tabIndex="-1"></button>
    </div>
    <!-- Quantidade Parcelas -->
    <div class="quantidade">
      <button class="diminuir" tabIndex="-1">-</button>
        <p class="atual">1x</p>
      <button class="aumentar" tabIndex="-1">+</button>
    </div>
    <!-- Grupo de Parcelas -->
    <div class="parcelas"></div>
    <div class="aviso">
      <p></p>
    </div>
    `
  }

  _renderizarNovaParcela (indice, valor, data, metodoPagamento = 'Dinheiro', atual) {
    const disabled = (this._verificarDisabled()) ? 'disabled="true"' : ''

    const html = `
    <label>${indice}ª Parcela</label>
    <div class="content">
      <app-date-picker ${disabled}></app-date-picker>
      <div class="grupo1">
        <input ${disabled} spellcheck="false" class="valor" autocomplete="off" type="text" placeholder="R$ 0,00" value="${valor}" data-mask-money="true">
        <span class="metodo_pagamento">${metodoPagamento}</span>
      </div>
    </div>
    `

    const novaParcela = document.createElement('div')
    novaParcela.classList.add('parcela')
    if (atual) novaParcela.classList.add('atual')
    novaParcela.innerHTML = html

    if (data) novaParcela.querySelector('app-date-picker').selecionarData(data)

    const parcelasDiv = this.querySelector('.parcelas')
    parcelasDiv.appendChild(novaParcela)
  }

  // Exibir

  _exibirQuantidadeParcelas () {
    const p = this.querySelector('.quantidade p.atual')
    p.innerHTML = `${this.__state.numeroParcelas}x`
  }

  _exibirValorRestante () {
    const p = this.querySelector('.aviso p')
    if (this.__state.valorRestante >= 0) {
      const valor = FORMAT().money(this.__state.valorRestante) || ''
      p.innerText = `Está faltando: ${valor}`
    } else {
      const valor = FORMAT().money(-this.__state.valorRestante) || ''
      p.innerText = `Está sobrando: ${valor}`
    }
  }

  // Limpar

  _limparValorTotal () {
    this.querySelector('input#valor_total').value = ''
    this.__state.valorTotal = 0
  }

  _limparDataParcelaAtual () {
    this.__state.dataParcelaAtual = ''
  }

  _limparValorRestante () {
    this.querySelector('.aviso p').innerText = ''
    this.__state.valorRestante = 0
  }

  _limparParcelas () {
    this.querySelector('.parcelas').innerHTML = ''
  }

  _limparQuantidadeParcelas () {
    this.__state.numeroParcelas = 0
    this._exibirQuantidadeParcelas()
  }

  // Parcelas

  _adicionarParcela () {
    const totalParcelas = this.__state.numeroParcelas
    this._renderizarNovaParcela(totalParcelas, '', '')
  }

  _deletarParcela () {
    const nodes = this.querySelectorAll('.parcela')
    const ultimaParcela = nodes[nodes.length - 1]
    ultimaParcela.parentElement.removeChild(ultimaParcela)
  }

  _calcularParcelas () {
    this._limparParcelas()
    const totalParcelas = this.__state.numeroParcelas
    const dataHoje = DATE().BRtoDate(DATE().todayLocal())

    const valorParcela = Number(this._calcularValorParcelamento())
    if (valorParcela === 0) return

    for (const indice of [...Array(totalParcelas).keys()]) {
      const dataParcela = DATE().addMonthsIntoDate(dataHoje, indice)
      const dataParcelaBR = DATE().dateToBR(dataParcela)
      const valorParcelaFormatada = FORMAT().money(valorParcela)

      const atual = (this.__state.dataParcelaAtual === dataParcelaBR)
      this._renderizarNovaParcela(indice + 1, valorParcelaFormatada, dataParcelaBR, 'Dinheiro', atual)
    }
  }

  _calcularValorParcelamento () {
    const valorParcela = (this.__state.valorTotal / this.__state.numeroParcelas)
    const parcelaArredondada = (Math.round(valorParcela * 100) / 100).toFixed(2)
    return parcelaArredondada
  }

  // Metodo Pagamento

  _trocarMetodoPagamento (span) {
    const metodoAtual = span.innerText
    const listaMetodos = this.__state.metodosPagamento

    const metodoSelecionado = listaMetodos.filter((metodo) => { return (metodo.texto === metodoAtual) })
    const indexSelecionado = listaMetodos.indexOf(metodoSelecionado[0])

    const proximoIndex = (indexSelecionado === listaMetodos.length - 1) ? 0 : indexSelecionado + 1
    const proximoMetodo = listaMetodos[proximoIndex].texto

    span.innerText = proximoMetodo
  }

  _filtrarMetodoPagamento (conteudo) {
    return this.__state.metodosPagamento.filter(metodo => {
      return (metodo.texto === conteudo || metodo.valor === conteudo)
    })
  }

  // Valor Restante

  verificarSeExisteValorRestante () {
    if (Number(this.__state.valorRestante) !== 0) {
      this._exibirValorRestante()
      return true
    }

    this._limparValorRestante()
    return false
  }

  _calcularValorRestante () {
    const parcelasDiv = this.querySelectorAll('.parcela')
    let valorTotal = Number(this.__state.valorTotal)

    for (const indice in parcelasDiv) {
      if (isNaN(indice)) continue

      const inputValor = parcelasDiv[indice].querySelector('.grupo1 input.valor').value
      const valor = FORMAT().unformatMoney(inputValor)

      valorTotal -= Number(valor).toFixed(2)
    }

    this.__state.valorRestante = Number(valorTotal).toFixed(2)
  }

  _verificarDisabled () {
    if (this.getAttribute('disabled') === 'true') return true
    return false
  }

  // Utils

  limpar () {
    this._limparParcelas()
    this._limparValorTotal()
    this._limparValorRestante()
    this._limparQuantidadeParcelas()
    this._limparDataParcelaAtual()
  }

  vazio () {
    return (!this.__state.valorTotal && this.__state.valorTotal !== 0)
  }

  desativar () {
    const inputValorTotal = this.querySelector('input#valor_total')
    this.setAttribute('disabled', true)
    inputValorTotal.setAttribute('disabled', true)
  }

  exportarValorTotal () {
    return this.__state.valorTotal
  }

  exportarParcelas () {
    const parcelasDiv = this.querySelectorAll('.parcela')
    const listaParcelas = []

    for (const indice in parcelasDiv) {
      if (isNaN(indice)) continue

      const valor = parcelasDiv[indice].querySelector('.grupo1 input.valor').value
      const metodo = parcelasDiv[indice].querySelector('.grupo1 span.metodo_pagamento').innerText
      const data = parcelasDiv[indice].querySelector('app-date-picker').exportarData()

      listaParcelas.push({
        valor: FORMAT().unformatMoney(valor),
        metodoPagamento: this._filtrarMetodoPagamento(metodo)[0].valor,
        dataPagamento: DATE().convertBRToTimestamp(data)
      })
    }

    return listaParcelas
  }

  importarValorTotal (valor) {
    this.__state.valorTotal = valor
    this.querySelector('input#valor_total').value = FORMAT().money(valor)
    this._calcularValorRestante()
  }

  importarDataParcelaAtual (dataParcelaAtual) {
    this.__state.dataParcelaAtual = dataParcelaAtual
  }

  importarParcelas (parcelas) {
    this.__state.numeroParcelas = parcelas.length
    this._exibirQuantidadeParcelas()

    for (const indice in parcelas) {
      const parcela = parcelas[indice]
      const numeroParcela = Number(indice) + 1

      const data = DATE().convertMongoDateToBR(parcela.dataPagamento)
      const valor = FORMAT().money(parcela.valor)
      const metodo = this._filtrarMetodoPagamento(parcela.metodoPagamento)[0].texto

      const atual = (this.__state.dataParcelaAtual === data)
      this._renderizarNovaParcela(numeroParcela, valor, data, metodo, atual)
    }
  }

  // Eventos

  habilitarFoco () {
    this.addEventListener('focus', (evento) => {
      if (this._verificarDisabled()) return

      const inputTotal = this.querySelector('input#valor_total')
      inputTotal.focus()
    })
  }

  habilitarCliqueSelecionandoInputValorTotal () {
    this.addEventListener('click', (evento) => {
      const elemento = 'input#valor_total'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      evento.target.select()
    })
  }

  habilitarKeyupInputValorTotal () {
    this.addEventListener('keyup', (evento) => {
      const elemento = 'input#valor_total'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      this.__state.valorTotal = FORMAT().unformatMoney(evento.target.value)

      this._calcularValorRestante()
      this.verificarSeExisteValorRestante()
    })
  }

  habilitarKeyupInputsValor () {
    this.addEventListener('keyup', (evento) => {
      if (evento.target.nodeName !== 'INPUT') return
      if (evento.target.getAttribute('data-mask-money') !== 'true') return

      this._calcularValorRestante()
      this.verificarSeExisteValorRestante()
    })
  }

  habilitarCliqueSelecionandoInputsValor () {
    this.addEventListener('click', (evento) => {
      if (evento.target.nodeName !== 'INPUT') return
      if (evento.target.getAttribute('data-mask-money') !== 'true') return

      evento.target.select()
    })
  }

  habilitarCliqueAdicionarParcela () {
    this.addEventListener('click', (evento) => {
      const elemento = '.quantidade button.aumentar'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return
      if (this.__state.numeroParcelas === 12) return

      this.__state.numeroParcelas++
      this._exibirQuantidadeParcelas()
      this._adicionarParcela()

      this._calcularValorRestante()
      this.verificarSeExisteValorRestante()
    })
  }

  habilitarCliqueDeletarParcela () {
    this.addEventListener('click', (evento) => {
      const elemento = '.quantidade button.diminuir'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return
      if (this.__state.numeroParcelas <= 1) return

      this.__state.numeroParcelas--
      this._exibirQuantidadeParcelas()
      this._deletarParcela()

      this._calcularValorRestante()
      this.verificarSeExisteValorRestante()
    })
  }

  habilitarCliqueBotaoCalcular () {
    this.addEventListener('click', (evento) => {
      const elemento = '.calcular button'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return
      if (!this.__state.valorTotal) return

      this._calcularParcelas()
      this._calcularValorRestante()
      this.verificarSeExisteValorRestante()
    })
  }

  habilitarCliqueTrocandoMetodoPagamento () {
    this.addEventListener('click', (evento) => {
      const elemento = '.parcelas .parcela span.metodo_pagamento'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      this._trocarMetodoPagamento(evento.target)
    })
  }
}

module.exports = Pagamento
