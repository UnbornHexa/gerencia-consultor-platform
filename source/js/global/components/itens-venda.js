/* Requires */

const FORMAT = require('../helpers/format')

/* Module */

class ProdutosVendas extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      todosOsProdutos: []
    }

    this._renderizar()
    this.habilitarCliqueBotaoAdicionarProduto()
    this.habilitarCliqueBotaoAumentarQuantidade()
    this.habilitarCliqueBotaoDiminuirQuantidade()
    this.habilitarCliqueSelecionarInputs()
    this.habilitarKeyupInputPrecoVenda()
  }

  // Renderizacao

  _renderizar () {
    this.innerHTML = `
    <div class="grupo-1">
      <!-- Produto -->
      <label>Produto <span class="required">*</span> </label>
      <app-select></app-select>
      <button class="outline" id="adicionar_produto">Adicionar Produto</button>
    </div>
    <div class="itens"></div>
    `
  }

  _renderizarNovoProduto (produto) {
    const precoCusto = FORMAT().money(produto.precoCusto)
    const precoVenda = FORMAT().money(produto.precoVenda)
    const subtotal = FORMAT().money((produto.precoVenda * produto.quantidade))

    const disabled = (this._verificarDisabled()) ? 'disabled="true"' : ''

    const itensDiv = this.querySelector('.itens')
    const html = `
    <input type="hidden" id="id" value="${produto._id}">
    <p class="titulo">${produto.nome}</p>
    <div class="info">
      <div class="input_wrapper">
      <label>Quantidade</label>
        <div class="quantidade">
          <button class="diminuir" tabIndex="-1">-</button>
          <p class="atual">${produto.quantidade}</p>
          <button class="aumentar" tabIndex="-1">+</button>
        </div>
      </div>
      <div class="input_wrapper">
        <label>Preço de Custo</label>
        <input spellcheck="false" id="preco_custo" autocomplete="off" type="text" value="${precoCusto}" placeholder="R$ 0,00" data-mask-money="true" ${disabled}>
      </div>
      <div class="input_wrapper">
        <label>Preço de Venda</label>
        <input spellcheck="false" id="preco_venda" autocomplete="off" type="text" value="${precoVenda}" placeholder="R$ 0,00" data-mask-money="true" ${disabled}>
      </div>
      <div class="input_wrapper">
        <label>Subtotal</label>
        <input spellcheck="false" id="subtotal" autocomplete="off" type="text" value="${subtotal}" placeholder="R$ 0,00" data-mask-money="true" ${disabled}>
      </div>
    </div>
    `

    const novoItem = document.createElement('div')
    novoItem.classList.add('item')
    novoItem.innerHTML = html

    itensDiv.appendChild(novoItem)
  }

  // Inicializacao

  povoarSelect (produtos) {
    const todosOsProdutos = produtos.map(produto => {
      return {
        _id: produto._id,
        nome: produto.nome,
        precoCusto: produto.precoCusto,
        precoVenda: produto.precoVenda,
        quantidadeMaxima: produto.quantidade,
        estocado: produto.estocado
      }
    })

    const produtosDisponiveis = todosOsProdutos.filter((produto) => {
      if (produto.estocado === false) return produto
      if (produto.estocado === true && produto.quantidadeMaxima > 0) return produto
    })

    this.__state.todosOsProdutos = produtosDisponiveis
    const opcoes = produtosDisponiveis.map(produto => {
      return {
        texto: produto.nome,
        valor: produto._id
      }
    })
    this.querySelector('app-select').importarOpcoes(opcoes)
  }

  // Validacoes e Calculos

  _filtrarProduto (id) {
    const filtro = (produto) => { return (produto._id === id) }
    return this.__state.todosOsProdutos.filter(filtro)
  }

  _verificarProdutoJaAdicionado (produto) {
    const itensDiv = this.querySelectorAll(`.itens .item input#id[value="${produto._id}"]`)
    return (itensDiv.length !== 0)
  }

  _verificarQuantidadeMaximaEmEstoque (produto) {
    if (produto.estocado === false) return true

    const itensDiv = this.querySelectorAll(`.itens .item input#id[value="${produto._id}"]`)
    const pAtual = itensDiv[0].parentElement.querySelector('.info .quantidade p.atual')

    const quantidadeAtual = Number(pAtual.innerText)
    const quantidadeMaxima = Number(produto.quantidadeMaxima)

    if (quantidadeMaxima > quantidadeAtual) return true

    document.querySelector('app-alerta').alertar('Não há mais produtos no estoque disponíveis')
    return false
  }

  _verificarSelectVazio () {
    if (!this.querySelector('app-select').vazio()) return false

    document.querySelector('app-alerta').alertar('Selecione o produto')
    return true
  }

  _verificarQuantidade0 (produto) {
    const itensDiv = this.querySelectorAll(`.itens .item input#id[value="${produto._id}"]`)
    const pAtual = itensDiv[0].parentElement.querySelector('.info .quantidade p.atual')

    const quantidade = Number(pAtual.innerText) - 1

    if (quantidade === 0) return true
    return false
  }

  _incrementarQuantidadeProduto (produto) {
    const itensDiv = this.querySelectorAll(`.itens .item input#id[value="${produto._id}"]`)
    const pAtual = itensDiv[0].parentElement.querySelector('.info .quantidade p.atual')
    const inputPrecoVenda = itensDiv[0].parentElement.querySelector('input#preco_venda')
    const inputSubtotal = itensDiv[0].parentElement.querySelector('input#subtotal')

    const quantidade = Number(pAtual.innerText) + 1
    const subtotal = quantidade * (Number(FORMAT().unformatMoney(inputPrecoVenda.value)) || produto.precoVenda)

    pAtual.innerText = quantidade
    inputSubtotal.value = FORMAT().money(subtotal)
  }

  _decrementarQuantidadeProduto (produto) {
    const itensDiv = this.querySelectorAll(`.itens .item input#id[value="${produto._id}"]`)
    const pAtual = itensDiv[0].parentElement.querySelector('.info .quantidade p.atual')
    const inputPrecoVenda = itensDiv[0].parentElement.querySelector('input#preco_venda')
    const inputSubtotal = itensDiv[0].parentElement.querySelector('input#subtotal')

    const quantidade = Number(pAtual.innerText) - 1
    const subtotal = quantidade * (Number(FORMAT().unformatMoney(inputPrecoVenda.value)) || produto.precoVenda)

    pAtual.innerText = quantidade
    inputSubtotal.value = FORMAT().money(subtotal)
  }

  _deletarProduto (produto) {
    const itensDiv = this.querySelectorAll(`.itens .item input#id[value="${produto._id}"]`)
    const item = itensDiv[0].parentElement

    item.parentElement.removeChild(item)
  }

  _verificarDisabled () {
    if (this.getAttribute('disabled') === 'true') return true
    return false
  }

  // Utils

  limpar () {
    this.querySelector('.itens').innerHTML = ''
  }

  desativar () {
    const selectProdutos = this.querySelector('app-select')
    const botaoAdicionarProdutos = this.querySelector('button#adicionar_produto')
    this.setAttribute('disabled', true)
    botaoAdicionarProdutos.setAttribute('disabled', true)
    selectProdutos.setAttribute('disabled', true)
  }

  limparSelect () {
    this.querySelector('app-select').limparSelecionado()
  }

  importarProdutos (produtos) {
    for (const produto of produtos) {
      const produtoObj = {
        _id: produto.idProduto._id,
        nome: produto.idProduto.nome,
        quantidade: produto.quantidade,
        precoCusto: produto.precoCusto,
        precoVenda: produto.precoVenda
      }
      this._renderizarNovoProduto(produtoObj)
    }
  }

  exportarProdutos () {
    const itensDiv = this.querySelectorAll('.itens .item')
    const listaProdutos = []

    for (const item of itensDiv) {
      const idProduto = item.querySelector('input#id').value
      const quantidade = item.querySelector('.info .quantidade p.atual').innerText
      const precoCusto = FORMAT().unformatMoney(item.querySelector('.info input#preco_custo').value)
      const precoVenda = FORMAT().unformatMoney(item.querySelector('.info input#preco_venda').value)
      const valorSubtotal = FORMAT().unformatMoney(item.querySelector('.info input#subtotal').value)
      const estocado = this._filtrarProduto(idProduto)[0].estocado

      const produtoObj = { idProduto, quantidade, precoCusto, precoVenda, valorSubtotal, estocado }
      listaProdutos.push(produtoObj)
    }

    return listaProdutos
  }

  exportarValorTotal () {
    const produtos = this.exportarProdutos()

    const reducer = (acumulador, produto) => { return acumulador + produto.valorSubtotal }
    return produtos.reduce(reducer, 0)
  }

  vazio () {
    const itens = this.querySelector('.itens').innerHTML
    return (!itens)
  }

  // Eventos

  habilitarCliqueBotaoAdicionarProduto () {
    this.addEventListener('click', (evento) => {
      const elemento = 'button#adicionar_produto'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return
      if (this._verificarSelectVazio()) return

      const idProduto = this.querySelector('app-select').exportarValorSelecionado()
      const produtoSelecionado = this._filtrarProduto(idProduto)[0]
      produtoSelecionado.quantidade = 1
      this.limparSelect()

      // produto nao adicionado
      const produtoJaAdicionado = this._verificarProdutoJaAdicionado(produtoSelecionado)
      if (!produtoJaAdicionado) {
        this._renderizarNovoProduto(produtoSelecionado)
        return
      }

      // produto ja adicionado
      const quantidadeSuficiente = this._verificarQuantidadeMaximaEmEstoque(produtoSelecionado)
      if (!quantidadeSuficiente) return

      this._incrementarQuantidadeProduto(produtoSelecionado)
    })
  }

  habilitarCliqueBotaoAumentarQuantidade () {
    this.addEventListener('click', (evento) => {
      const elemento = 'app-itens-venda .itens .item .info button.aumentar'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      const idProduto = evento.target.parentElement.parentElement.parentElement
        .parentElement.querySelector('input#id').value
      const produtoSelecionado = this._filtrarProduto(idProduto)[0]

      const quantidadeSuficiente = this._verificarQuantidadeMaximaEmEstoque(produtoSelecionado)
      if (!quantidadeSuficiente) return

      this._incrementarQuantidadeProduto(produtoSelecionado)
    })
  }

  habilitarCliqueBotaoDiminuirQuantidade () {
    this.addEventListener('click', (evento) => {
      const elemento = 'app-itens-venda .itens .item .info button.diminuir'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      const idProduto = evento.target.parentElement.parentElement.parentElement
        .parentElement.querySelector('input#id').value
      const produtoSelecionado = this._filtrarProduto(idProduto)[0]

      const quantidade0 = this._verificarQuantidade0(produtoSelecionado)
      if (quantidade0) {
        this._deletarProduto(produtoSelecionado)
        return
      }

      this._decrementarQuantidadeProduto(produtoSelecionado)
    })
  }

  habilitarCliqueSelecionarInputs () {
    this.addEventListener('click', (evento) => {
      const elemento = 'app-itens-venda .itens .item input[data-mask-money="true"]'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      evento.target.select()
    })
  }

  habilitarKeyupInputPrecoVenda () {
    this.addEventListener('input', (evento) => {
      const elemento = 'app-itens-venda .itens .item input#preco_venda'
      if (!evento.target.matches(elemento)) return
      if (this._verificarDisabled()) return

      const precoVenda = FORMAT().unformatMoney(evento.target.value)
      const itemDiv = evento.target.parentElement.parentElement
      const pQuantidade = itemDiv.querySelector('p.atual')
      const inputSubTotal = itemDiv.querySelector('input#subtotal')

      const subtotal = Number(pQuantidade.innerText) * Number(precoVenda)
      inputSubTotal.value = FORMAT().money(subtotal)
    })
  }
}

module.exports = ProdutosVendas
