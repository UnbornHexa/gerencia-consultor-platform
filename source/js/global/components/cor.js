class Cor extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      selecionado: ''
    }

    this._renderizar()
    this.habilitarClique()
  }

  // Renderizacao

  _renderizar () {
    this.innerHTML = `
    <button class="cinza" tabIndex="-1"></button>
    <button class="azul" tabIndex="-1"></button>
    <button class="verde" tabIndex="-1"></button>
    <button class="amarelo" tabIndex="-1"></button>
    <button class="vermelho" tabIndex="-1"></button>
    <button class="rosa" tabIndex="-1"></button>
    `
  }

  // Utils

  limpar () {
    this.__state.selecionado = ''

    const divSelecionada = this.querySelector('.selecionado')
    if (!document.contains(divSelecionada)) return

    divSelecionada.classList.remove('selecionado')
  }

  selecionarCor (cor) {
    this.limpar()
    const divSelecionada = this.querySelector(`button.${cor}`)
    if (!document.contains(divSelecionada)) return

    divSelecionada.classList.add('selecionado')
    this.__state.selecionado = cor
  }

  exportarCor () {
    return this.__state.selecionado
  }

  // Eventos

  habilitarClique () {
    this.addEventListener('click', (evento) => {
      const elemento = 'button'
      if (!evento.target.matches(elemento)) return

      const cor = evento.target.classList[0]
      this.selecionarCor(cor)
    })
  }
}

module.exports = Cor
