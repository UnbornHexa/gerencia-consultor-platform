/* Requires */

const DATE = require('../helpers/date')

/* Module */

class DateSelect extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      data: {
        selecionada: {
          ano: 0,
          mes: 0
        },
        hoje: {
          ano: 0,
          mes: 0
        }
      },
      nomeMeses: [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
      ]
    }

    this._renderizar()
    this._iniciarValores()
    this._exibirData()

    this.habilitarCliqueBotaoAvancar()
    this.habilitarCliqueBotaoVoltar()
  }

  // Renderizar

  _renderizar () {
    this.innerHTML = `
    <button class="voltar" tabIndex="-1"><i class="icon-seta-esquerda"></i></button>
    <p class="animation">&nbsp;<span>&nbsp;</span></p>
    <button class="avancar" tabIndex="-1"><i class="icon-seta-direita"></i></button>
    `
  }

  _iniciarValores () {
    const hoje = new Date()
    const objHoje = DATE().dateToObj(hoje)

    this.__state.data.hoje.ano = objHoje.year
    this.__state.data.selecionada.ano = objHoje.year

    this.__state.data.hoje.mes = objHoje.month
    this.__state.data.selecionada.mes = objHoje.month
  }

  _exibirData () {
    const ano = this.__state.data.selecionada.ano
    const mes = this.__state.data.selecionada.mes
    const nomeMes = this.__state.nomeMeses[Number(mes) - 1]

    this.querySelector('p').innerHTML = `
    ${nomeMes}
    <span>${ano}</span
    `
  }

  exportarDataSelecionada () {
    return {
      ano: this.__state.data.selecionada.ano,
      mes: this.__state.data.selecionada.mes
    }
  }

  _avancarMes () {
    if (this.__state.data.selecionada.mes < 12) {
      this.__state.data.selecionada.mes++
    } else {
      this.__state.data.selecionada.mes = 1
      this.__state.data.selecionada.ano++
    }

    this._exibirData()
  }

  _retrocederMes () {
    if (this.__state.data.selecionada.mes > 1) {
      this.__state.data.selecionada.mes--
    } else {
      this.__state.data.selecionada.mes = 12
      this.__state.data.selecionada.ano--
    }

    this._exibirData()
  }

  // Eventos

  habilitarCliqueBotaoAvancar () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('button.avancar')
      if (evento.target !== elemento) return

      this._avancarMes()
    })
  }

  habilitarCliqueBotaoVoltar () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('button.voltar')
      if (evento.target !== elemento) return

      this._retrocederMes()
    })
  }
}

module.exports = DateSelect
