/*  Requires */

const ACCENT = require('../helpers/accent')

/* Module */

class Lista extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      itens: [],
      itensFiltrados: [],
      itensPorPagina: 20,
      indice: {
        ultimaPagina: 0,
        primeiraPagina: 1,
        paginaAtual: 1
      }
    }

    this._renderizar()
    this.habilitarCliqueProximaPagina()
    this.habilitarCliqueAnteriorPagina()
    this.habilitarCliqueUltimaPagina()
    this.habilitarCliquePrimeiraPagina()
  }

  // Itens

  limparItens () {
    const itensUl = this.querySelector('ul.itens')
    itensUl.innerHTML = ''
  }

  importarItens (itens) {
    this.limparItens()
    this.__state.itens = itens
    this.__state.itensFiltrados = itens
    this._exibirItensDaPagina(1)
    this._irPrimeiraPagina()
  }

  _exibirItensDaPagina (pagina) {
    this.limparItens()
    const indicePrimeiroItemAExibir = (pagina - 1) * this.__state.itensPorPagina
    const indiceUltimoItemAExibir = (pagina * this.__state.itensPorPagina)
    const itensAExibir = this.__state.itensFiltrados.slice(indicePrimeiroItemAExibir, indiceUltimoItemAExibir)

    for (const item of itensAExibir) {
      this._renderizarNovoItem(item)
    }

    this._calcularPaginacao()
  }

  filtrar (texto) {
    this.__state.itensFiltrados = this.__state.itens.filter(item => {
      const termoPesquisado = ACCENT().remove(texto.toString().toUpperCase())
      const itemAVerificar = ACCENT().remove(item.nome.toUpperCase())
      return (itemAVerificar.includes(termoPesquisado))
    })

    this._exibirItensDaPagina(1)
  }

  // Renderizacao

  _renderizar () {
    this.innerHTML = `
      <ul class="itens"></ul>
      <div class="paginacao"></div>
    `

    this._calcularPaginacao()
  }

  _renderizarNovoItem (_item) {
    const itensUl = this.querySelector('ul.itens')

    const li = '<li class="item"></li>'
    itensUl.innerHTML += li
  }

  _renderizarPaginacao () {
    const paginacaoDiv = this.querySelector('.paginacao')
    paginacaoDiv.innerHTML = ''

    if ((this.__state.indice.paginaAtual - 2) >= this.__state.indice.primeiraPagina) {
      paginacaoDiv.innerHTML += `<span class="primeira">${this.__state.indice.primeiraPagina}</span>`
      paginacaoDiv.innerHTML += '<span class="divisor">...</span>'
    }

    if ((this.__state.indice.paginaAtual - 1) >= this.__state.indice.primeiraPagina) {
      paginacaoDiv.innerHTML += `<span class="anterior">${this.__state.indice.paginaAtual - 1}</span>`
    }

    paginacaoDiv.innerHTML += `<span class="atual">${this.__state.indice.paginaAtual}</span>`

    if ((this.__state.indice.paginaAtual + 1) <= this.__state.indice.ultimaPagina) {
      paginacaoDiv.innerHTML += `<span class="proxima">${this.__state.indice.paginaAtual + 1}</span>`
    }

    if ((this.__state.indice.paginaAtual + 2) <= this.__state.indice.ultimaPagina) {
      paginacaoDiv.innerHTML += '<span class="divisor">...</span>'
      paginacaoDiv.innerHTML += `<span class="ultima">${this.__state.indice.ultimaPagina}</span>`
    }
  }

  // Paginacao

  _calcularPaginacao () {
    const numeroPaginas = Math.ceil(this.__state.itensFiltrados.length / this.__state.itensPorPagina)
    this.__state.indice.ultimaPagina = numeroPaginas
    this._renderizarPaginacao()
  }

  _irProximaPagina () {
    if (this.__state.indice.paginaAtual >= this.__state.indice.ultimaPagina) return
    this.__state.indice.paginaAtual++
    this.limparItens()
    this._exibirItensDaPagina(this.__state.indice.paginaAtual)
  }

  _irPaginaAnterior () {
    if (this.__state.indice.paginaAtual <= this.__state.indice.primeiraPagina) return
    this.__state.indice.paginaAtual--
    this.limparItens()
    this._exibirItensDaPagina(this.__state.indice.paginaAtual)
  }

  _irUltimaPagina () {
    this.__state.indice.paginaAtual = this.__state.indice.ultimaPagina
    this.limparItens()
    this._exibirItensDaPagina(this.__state.indice.paginaAtual)
  }

  _irPrimeiraPagina () {
    this.__state.indice.paginaAtual = this.__state.indice.primeiraPagina
    this.limparItens()
    this._exibirItensDaPagina(this.__state.indice.paginaAtual)
  }

  // Eventos

  habilitarCliqueProximaPagina () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('.paginacao .proxima')
      if (evento.target !== elemento) return
      this._irProximaPagina()
    })
  }

  habilitarCliqueAnteriorPagina () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('.paginacao .anterior')
      if (evento.target !== elemento) return
      this._irPaginaAnterior()
    })
  }

  habilitarCliqueUltimaPagina () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('.paginacao .ultima')
      if (evento.target !== elemento) return
      this._irUltimaPagina()
    })
  }

  habilitarCliquePrimeiraPagina () {
    this.addEventListener('click', (evento) => {
      const elemento = this.querySelector('.paginacao .primeira')
      if (evento.target !== elemento) return
      this._irPrimeiraPagina()
    })
  }
}

module.exports = Lista
