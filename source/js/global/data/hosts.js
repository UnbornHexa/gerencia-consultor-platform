const Module = () => {
  const methods = {}

  // Internal Variables

  const __protocol = window.location.protocol
  const __host = window.location.host

  methods.url = {
    appPlataforma: `${__protocol}//${__host}`,
    apiPlataforma: 'https://api-plataforma.gerenciaconsultor.com'
    // apiPlataforma: 'http://localhost:53100'
  }

  return methods
}

module.exports = Module
