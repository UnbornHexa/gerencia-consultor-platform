const Module = () => {
  const methods = {}

  methods.money = (valor) => {
    if (!valor && valor !== 0) return 'R$ 00,00'
    valor = Number(valor)

    const pais = 'pt-BR'
    const parametros = {
      style: 'currency',
      currency: 'BRL'
    }

    return valor.toLocaleString(pais, parametros)
  }

  methods.unformatMoney = (valor) => {
    valor = valor.replace(/\u00A0/, ' ')
    valor = valor.replace('R$ ', '')
    valor = valor.replace(/\./g, '')
    valor = valor.replace(',', '.')
    valor = Number(valor)
    return valor
  }

  methods.number = (numero) => {
    return numero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }

  methods.onlyNumber = (numero) => {
    return numero.toString().replace(/[^0-9]/g, '')
  }

  return methods
}

module.exports = Module
