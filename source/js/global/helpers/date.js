const Module = () => {
  const methods = {}

  /* VALIDACAO */

  /*
  * Verifica se string esta no formato ####-##-##
  */
  methods.validateUS = (dateUS) => {
    if (!dateUS) return false

    const regex = new RegExp(/[0-9]{4}-[0-9]{2}-[0-9]{2}$/)
    if (!regex.test(dateUS)) return false

    return true
  }

  /*
  * Verifica se string esta no formato ##/##/####
  */
  methods.validateBR = (dateBR) => {
    if (!dateBR) return false

    const regex = new RegExp(/[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)
    if (!regex.test(dateBR)) return false

    return true
  }

  /* CONVERSAO */

  /*
  * Converte string no formato ##/##/#### em string no formato ####-##-##
  */
  methods.BRtoUS = (dateBR) => {
    if (!dateBR) return
    if (!methods.validateBR(dateBR)) return

    const dateUS = dateBR.split('/')
    return `${dateUS[2]}-${dateUS[1]}-${dateUS[0]}`
  }

  /*
  * Converte string no formato ####-##-## em string no formato ##/##/####
  */
  methods.UStoBR = (dateUS) => {
    if (!dateUS) return
    if (!methods.validateUS(dateUS)) return

    const dateBR = dateUS.split('-')
    return `${dateBR[0]}/${dateBR[1]}/${dateBR[2]}`
  }

  /*
  * Converte string no formato ##/##/#### em data
  */
  methods.BRtoDate = (dateBR) => {
    if (!dateBR) return
    if (!methods.validateBR(dateBR)) return

    const dateUS = methods.BRtoUS(dateBR)
    return methods.UStoDate(dateUS)
  }

  /*
  * Converte string no formato ####-##-## em data
  */
  methods.UStoDate = (dateUS) => {
    if (!dateUS) return
    if (!methods.validateUS(dateUS)) return

    return new Date(dateUS)
  }

  /*
  * Converte data em objeto
  */
  methods.dateToObj = (date) => {
    if (!date) return

    const day = (date.getDate()).toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = (date.getFullYear()).toString().padStart(4, '0')

    return { day, month, year }
  }

  /*
  * Converte data em string no formato ##/##/####
  */
  methods.dateToBR = (date) => {
    if (!date) return

    const obj = methods.dateToObj(date)
    return `${obj.day}/${obj.month}/${obj.year}`
  }

  /*
  * Converte data em string no formato ####-##-##
  */
  methods.dateToUS = (date) => {
    if (!date) return

    const obj = methods.dateToObj(date)
    return `${obj.year}-${obj.month}-${obj.day}`
  }

  /* TIMESTAMP */

  /*
  * Transforma data em timestamp
  */
  methods.dateToTimestamp = (date) => {
    if (!date) return

    return date.getTime()
  }

  /*
  * Adiciona utc horario local ao timestamp
  */
  methods.fixUTC = (timestamp) => {
    if (!timestamp) return

    const utc = new Date().getTimezoneOffset() * 1000 * 60
    return timestamp + utc
  }

  /*
  * Transforma timestamp em data
  */
  methods.timestampToDate = (timestamp) => {
    if (!timestamp) return

    return new Date(timestamp)
  }

  /* MONGODB */

  /*
  * Transforma data do banco de dados em timestamp
  */
  methods.mongoDateToTimestamp = (mongoDate) => {
    if (!mongoDate) return

    return new Date(mongoDate).getTime()
  }

  /* EXTENSIONS */

  methods.getHowManyDaysMonthHas = (year, month) => {
    if (!year || !month) return

    return new Date(year, month + 1, 0).getDate()
  }

  methods.getIndexOfWeekDay = (year, month, day = 1) => {
    if (!year || !month) return

    const newDate = new Date(year, (month - 1), day)
    return newDate.getDay()
  }

  methods.addMonthsIntoDate = (date, number) => {
    if (!date) return

    const obj = methods.dateToObj(date)
    const futureMonth = (Number(obj.month) - 1) + Number(number)
    const limitDays = methods.getHowManyDaysMonthHas(obj.year, futureMonth)

    const generatedDate = new Date(date)
    generatedDate.setDate(1)
    generatedDate.setMonth(futureMonth)
    generatedDate.setDate(Math.min(limitDays, obj.day))

    const timestamp = methods.dateToTimestamp(generatedDate)
    const fixedTimestamp = methods.fixUTC(timestamp)
    const newDate = methods.timestampToDate(fixedTimestamp)

    return newDate
  }

  /* CUSTOM */

  /*
  * Recebe string da data atual no formato ##/##/####
  */
  methods.today = () => {
    const date = new Date()
    const timestamp = methods.dateToTimestamp(date)
    const fixedTimestamp = methods.fixUTC(timestamp)
    const newDate = methods.timestampToDate(fixedTimestamp)
    return methods.dateToBR(newDate)
  }

  /*
  * Recebe string da data local do dispositivo no formato ##/##/####
  */
  methods.todayLocal = () => {
    const date = new Date()
    return methods.dateToBR(date)
  }

  /*
  * Transforma texto no formato ##/##/#### em timestamp
  */
  methods.convertBRToTimestamp = (dateBR) => {
    if (!dateBR) return

    const date = methods.BRtoDate(dateBR)
    const timestamp = methods.dateToTimestamp(date)
    const fixedTimestamp = methods.fixUTC(timestamp)
    const newDate = methods.timestampToDate(fixedTimestamp)
    return methods.dateToTimestamp(newDate)
  }

  /*
  * Transforma MongoDB Data em string no formato ##/##/####
  */

  methods.convertMongoDateToBR = (mongoDate) => {
    if (!mongoDate) return

    const timestamp = methods.mongoDateToTimestamp(mongoDate)
    const newDate = methods.timestampToDate(timestamp)
    return methods.dateToBR(newDate)
  }

  return methods
}

module.exports = Module
