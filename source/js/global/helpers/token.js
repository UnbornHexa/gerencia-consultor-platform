const Module = () => {
  const methods = {}

  // Internal Variables

  const __tokenName = 'token-usuario'

  // Methods

  methods.getToken = () => {
    return window.localStorage.getItem(__tokenName) || false
  }

  methods.setToken = (data) => {
    window.localStorage.setItem(__tokenName, data)
  }

  methods.delToken = () => {
    window.localStorage.removeItem(__tokenName)
  }

  // Get Info From Token

  methods.readTimestamp = () => {
    return readToken(methods.getToken()).exp || 0
  }

  methods.readUsuarioId = () => {
    return readToken(methods.getToken()).data.id || 0
  }

  methods.readUsuarioNome = () => {
    return readToken(methods.getToken()).data.nome || ''
  }

  methods.readUsuarioEmail = () => {
    return readToken(methods.getToken()).data.email || ''
  }

  methods.readUsuarioAssinatura = () => {
    return readToken(methods.getToken()).data.assinatura || ''
  }

  // Aux Funtions

  function readToken (token) {
    const payload = token.match(/\.([^.]+)\./)[1]
    const tokenDecrypted = b64DecodeUnicode(payload)
    return JSON.parse(tokenDecrypted)
  }

  function b64DecodeUnicode (str) {
    return decodeURIComponent(window.atob(str).split('').map(c => {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
  }

  return methods
}

module.exports = Module
