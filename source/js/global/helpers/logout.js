/* Requires */

const HOSTS = require('../data/hosts')
const TOKEN = require('./token')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.checkTokenOk = () => {
    if (!TOKEN().getToken()) { methods.logout() }
    if (methods.tokenExpired()) { methods.logout() }
  }

  methods.logout = () => {
    TOKEN().delToken()
    window.localStorage.removeItem('nome-usuario')
    window.location.assign(`${HOSTS().url.appPlataforma}`)
  }

  // Aux Functions

  methods.tokenExpired = () => {
    const expiracaoTimestamp = TOKEN().readTimestamp()
    const atualTimestamp = Number(new Date().getTime() / 1000).toFixed(0)
    const diferencaTimeStamp = expiracaoTimestamp - atualTimestamp

    return (diferencaTimeStamp < 0)
  }

  return methods
}

module.exports = Module
