const Module = () => {
  const methods = {}

  methods.load = () => {
    if (!window.localStorage.getItem('dark-theme')) return
    methods.habilitarModoNoturno()
  }

  methods.habilitarModoNoturno = () => {
    window.localStorage.setItem('dark-theme', true)
    document.querySelector('html').setAttribute('data-theme', 'dark')
  }

  methods.desabilitarModoNoturno = () => {
    window.localStorage.removeItem('dark-theme')
    document.querySelector('html').removeAttribute('data-theme')
  }

  return methods
}

module.exports = Module
