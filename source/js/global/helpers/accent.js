const Module = () => {
  const methods = {}

  methods.remove = (content) => {
    return content
      .replace(/[À|Á|Â|Ä|Ã]/g, 'A')
      .replace(/[È|É|Ê|Ë|Ẽ]/g, 'E')
      .replace(/[Ì|Í|Î|Ï|Ĩ]/g, 'I')
      .replace(/[Ò|Ó|Ô|Ö|Õ]/g, 'O')
      .replace(/[Ù|Ú|Û|Ü|Ũ]/g, 'U')
      .replace(/[Ç]/g, 'C')
      .replace(/[Ñ]/g, 'N')
      .replace(/[\n]/g, ' ')
  }

  return methods
}

module.exports = Module
