/* Requires */

const HOSTS = require('../data/hosts')
const TOKEN = require('./token')

/* Module */

const Module = () => {
  const methods = {}

  // Methods

  methods.checkAssinaturaOk = () => {
    const assinaturaExpirada = TOKEN().readUsuarioAssinatura().expirado
    if (!assinaturaExpirada) return

    methods._redirecionarAssinatura()
  }

  methods._redirecionarAssinatura = () => {
    window.location.assign(`${HOSTS().url.appPlataforma}/assinatura`)
  }

  return methods
}

module.exports = Module
