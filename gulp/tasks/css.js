/* Requires */

const gulp = require('gulp')
const uglifyCss = require('gulp-uglifycss')
const cssImport = require('gulp-cssimport')
const autoprefixer = require('gulp-autoprefixer')

const es = require('event-stream')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

const mode = require('gulp-mode')()

/* Settings */

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/css'),
  build: path.join(__dirname, '../../build/css')
}

const files = [
  route.source + '/pages/agenda/agenda.css',
  route.source + '/pages/anotacoes/anotacoes.css',
  route.source + '/pages/assinatura/assinatura.css',
  route.source + '/pages/assinatura-aprovado/assinatura-aprovado.css',
  route.source + '/pages/assinatura-pendente/assinatura-pendente.css',
  route.source + '/pages/autenticacao/autenticacao.css',
  route.source + '/pages/clientes/clientes.css',
  route.source + '/pages/erro/erro.css',
  route.source + '/pages/financeiro/financeiro.css',
  route.source + '/pages/financeiro-despesas/financeiro-despesas.css',
  route.source + '/pages/financeiro-receitas/financeiro-receitas.css',
  route.source + '/pages/notificacoes/notificacoes.css',
  route.source + '/pages/nova-senha/nova-senha.css',
  route.source + '/pages/painel/painel.css',
  route.source + '/pages/perfil/perfil.css',
  route.source + '/pages/produtos/produtos.css',
  route.source + '/pages/vendas/vendas.css',
  route.source + '/pages/vendas-encomendas/vendas-encomendas.css',
  route.source + '/pages/vendas-pos-venda/vendas-pos-venda.css'
]

/* Tasks */

gulp.task('bundle-pages', function (done) {
  const tasks = files.map(entry => {
    return gulp.src(entry)
      .pipe(plumber({ errorHandler: error => { notify.onError(errorMessage)(error) } }))
      .pipe(cssImport())
      .pipe(autoprefixer())
      .pipe(mode.production(uglifyCss()))
      .pipe(gulp.dest(route.build))
  })
  es.merge.apply(null, tasks)
  es.merge(tasks).on('end', done)
})

gulp.task('app.css', gulp.parallel([
  'bundle-pages'
]))
