/* Requires */

const gulp = require('gulp')
const clean = require('gulp-clean')
const fs = require('fs')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

/* Settings */

const errorMessage = 'Error: <%= error.message %>'
const route = {
  build: path.join(__dirname, '../../build/')
}

/* Tasks */

gulp.task('mkdir', function () {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(route.build)) fs.mkdirSync(route.build)
    resolve()
  })
})

gulp.task('clean-build', function () {
  return gulp.src(route.build)
    .pipe(plumber({ errorHandler: error => { notify.onError(errorMessage)(error) } }))
    .pipe(clean({ force: true }))
})

gulp.task('app.clean', gulp.parallel([
  'mkdir',
  'clean-build'
]))
