/* Requires */

const express = require('express')
const router = express.Router()

/* Controllers */

const AUTENTICACAO_CONTROLLER = require('../controllers/autenticacao')

/* Methods */

router.get('/', AUTENTICACAO_CONTROLLER.renderizar)
router.get('/nova-senha/:idUsuario/:token', AUTENTICACAO_CONTROLLER.renderizarNovaSenha)

module.exports = router
