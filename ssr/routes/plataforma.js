/* Requires */

const express = require('express')
const router = express.Router()

/* Controllers */

const AGENDA_CONTROLLER = require('../controllers/agenda')
const ANOTACOES_CONTROLLER = require('../controllers/anotacoes')
const ASSINATURA_CONTROLLER = require('../controllers/assinatura')
const CLIENTES_CONTROLLER = require('../controllers/clientes')
const FINANCEIRO_CONTROLLER = require('../controllers/financeiro')
const NOTIFICACOES_CONTROLLER = require('../controllers/notificacoes')
const PAINEL_CONTROLLER = require('../controllers/painel')
const PERFIL_CONTROLLER = require('../controllers/perfil')
const PRODUTOS_CONTROLLER = require('../controllers/produtos')
const VENDAS_CONTROLLER = require('../controllers/vendas')

/* Methods */

router.get('/agenda', AGENDA_CONTROLLER.renderizar)
router.get('/anotacoes', ANOTACOES_CONTROLLER.renderizar)
router.get('/assinatura', ASSINATURA_CONTROLLER.renderizar)
router.get('/assinatura/aprovado', ASSINATURA_CONTROLLER.renderizarAprovado)
router.get('/assinatura/pendente', ASSINATURA_CONTROLLER.renderizarPendente)
router.get('/clientes', CLIENTES_CONTROLLER.renderizar)
router.get('/financeiro', FINANCEIRO_CONTROLLER.renderizarResumo)
router.get('/financeiro/despesas', FINANCEIRO_CONTROLLER.renderizarDespesas)
router.get('/financeiro/receitas', FINANCEIRO_CONTROLLER.renderizarReceitas)
router.get('/notificacoes', NOTIFICACOES_CONTROLLER.renderizar)
router.get('/painel', PAINEL_CONTROLLER.renderizar)
router.get('/perfil', PERFIL_CONTROLLER.renderizar)
router.get('/produtos', PRODUTOS_CONTROLLER.renderizar)
router.get('/vendas', VENDAS_CONTROLLER.renderizar)
router.get('/vendas/pos-venda', VENDAS_CONTROLLER.renderizarPosVenda)
router.get('/vendas/encomendas', VENDAS_CONTROLLER.renderizarEncomendas)

module.exports = router
