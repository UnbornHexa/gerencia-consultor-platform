/* Requires */

const file = require('../helpers/read-file-async')
const path = require('path')

/* Internal Variables */

const PATH_HTML = path.join(__dirname, '../../build/html/pages')

/* Methods */

exports.renderizarResumo = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/financeiro.html`)
  res.status(200).send(html)
}

exports.renderizarReceitas = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/financeiro-receitas.html`)
  res.status(200).send(html)
}

exports.renderizarDespesas = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/financeiro-despesas.html`)
  res.status(200).send(html)
}
