/* Requires */

const file = require('../helpers/read-file-async')
const path = require('path')

/* Internal Variables */

const PATH_HTML = path.join(__dirname, '../../build/html/pages')

/* Methods */

exports.renderizar = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/assinatura.html`)
  res.status(200).send(html)
}

exports.renderizarAprovado = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/assinatura-aprovado.html`)
  res.status(200).send(html)
}

exports.renderizarPendente = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/assinatura-pendente.html`)
  res.status(200).send(html)
}
