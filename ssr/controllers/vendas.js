/* Requires */

const file = require('../helpers/read-file-async')
const path = require('path')

/* Internal Variables */

const PATH_HTML = path.join(__dirname, '../../build/html/pages')

/* Methods */

exports.renderizar = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/vendas.html`)
  res.status(200).send(html)
}

exports.renderizarPosVenda = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/vendas-pos-venda.html`)
  res.status(200).send(html)
}

exports.renderizarEncomendas = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/vendas-encomendas.html`)
  res.status(200).send(html)
}
