/* Requires */

const fs = require('fs')

/* Methods */

exports.readFileAsync = async (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (error, data) => {
      if (error) throw new Error(error)
      resolve(data)
    })
  })
}
