/* Requires */

const express = require('express')
const path = require('path')
const morgan = require('morgan')
const serveStatic = require('serve-static')
const compression = require('compression')
const helmet = require('helmet')

const app = express()

const helmetConfig = require('./config/helmet.json')

/* Middlewares */

app.use('/static', serveStatic(path.join(__dirname, '../build')))
app.use('/', serveStatic(path.join(__dirname, '../build/docs')))

app.use(compression())
app.use(morgan('common'))

app.use(helmet.hidePoweredBy())
app.use(helmet.contentSecurityPolicy(helmetConfig.contentSecurityPolicy))

/* Routes */

const AUTENTICACAO_ROUTE = require('./routes/autenticacao')
const PLATAFORMA_ROUTE = require('./routes/plataforma')
const ERRO_ROUTE = require('./routes/erro')

app.use('/', AUTENTICACAO_ROUTE)
app.use('/', PLATAFORMA_ROUTE)
app.use('/', ERRO_ROUTE)

module.exports = app
