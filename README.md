1. Baixando o Projeto
```
git clone https://gitlab.com/gerencia-consultor/frontend/plataforma.git
```

2. Instalando as Dependências
```
cd plataforma
yarn install
```

3. Executando a Aplicação
```
yarn run dev
```
